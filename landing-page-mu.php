<?php 
	$tel = '0913349945';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Ohnew.vn - Đồng phục</title>
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta name="keywords" content="">
    <meta name="description" content="Tottee.com">
    <script id='script_viewport' type='text/javascript'>
        window.ladi_viewport = function() {
            var width = window.outerWidth > 0 ? window.outerWidth : window.screen.width;
            var widthDevice = width;
            var is_desktop = width >= 768;
            var content = "";
            if (typeof window.ladi_is_desktop == "undefined" || window.ladi_is_desktop == undefined) {
                window.ladi_is_desktop = is_desktop;
            }
            if (!is_desktop) {
                widthDevice = 420;
            } else {
                widthDevice = 960;
            }
            content = "width=" + widthDevice + ", user-scalable=no";
            var scale = 1;
            if (!is_desktop && widthDevice != window.screen.width && window.screen.width > 0) {
                scale = window.screen.width / widthDevice;
            }
            if (scale != 1) {
                content += ", initial-scale=" + scale + ", minimum-scale=" + scale + ", maximum-scale=" + scale;
            }
            var docViewport = document.getElementById("viewport");
            if (!docViewport) {
                docViewport = document.createElement("meta");
                docViewport.setAttribute("id", "viewport");
                docViewport.setAttribute("name", "viewport");
                document.head.appendChild(docViewport);
            }
            docViewport.setAttribute("content", content);
        };
        window.ladi_viewport();
    </script>
    <link rel="canonical" href="http://ladituivaiohnew.ladi.me" />
    <meta property="og:url" content="http://ladituivaiohnew.ladi.me" />
    <meta property="og:title" content="Đặt tiêu đề trang" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://static.ladipage.net//57b167c9ca57d39c18a1c57c/untitled-1-957168.jpg">
    <meta property="og:description" content="Tottee.com" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="dns-prefetch">
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <link rel="preconnect" href="https://w.ladicdn.com/" crossorigin>
    <link rel="preconnect" href="https://api.forms.ladipage.com/" crossorigin>
    <link rel="preconnect" href="https://la.ladipage.com/" crossorigin>
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Open Sans:bold,regular|Montserrat:bold,regular|Roboto:bold,regular&display=swap" as="style" onload="this.onload = null;this.rel = 'stylesheet';">
    <link rel="preload" href="/style/script.js" as="script">
    <link href="/style/css3.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>

<body>
	<nav id="menuheaderfix" class="topmainmenu">
		<a href="/index.php" class="logo pull-left"><img alt="Công ty may thời trang, đồng phục bảo hộ lao động Ohnew" src="media/upload/slide/47941069.png" height="60px"></a>
		<div class="middle-menu"> 
		  <ul class="menu-ul">
			<li><a href="/index.php">Trang chủ</a></li>
			<li class="menu-intro">Giới thiệu</li>
			<li class="menu-product">Sản phẩm</li>
			<li class="menu-customer">Khách hàng</li>
			<li class="menu-promo">Khuyến mãi</li>
			<li class="menu-contact">Liên hệ</li>
		  </ul>
		</div>
		<a href="tel:<?php echo $tel;?>" class="btn-get-price">Lấy báo giá</a>
	</nav>
    <div class="ladi-wraper">
        <div id="SECTION2" class="ladi-section">
            <div class='ladi-section-background' style="background-image: url('/media/upload/slide/landingpage-slide.jpg')"></div>
            <div class="ladi-overlay"></div>
            <div class="ladi-container">
                <div id="HEADLINE5" class="ladi-element">
                    <h1 class='ladi-headline'></h1></div>
                <div id="BUTTON7" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT7" class="ladi-element">
                            <p class='ladi-headline' id="top-submit" style="cursor:pointer">ĐĂNG KÝ NGAY!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SECTION898" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-overlay"></div>
            <div class="ladi-container">
                <div id="GROUP899" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="GROUP916" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX900" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE901" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 1664 1896.0833" fill="rgba(255, 174, 16, 1.0)">
                                            <path d="M557 1280h595V685zm-45-45l595-595H512v595zm1152 77v192q0 14-9 23t-23 9h-224v224q0 14-9 23t-23 9h-192q-14 0-23-9t-9-23v-224H288q-14 0-23-9t-9-23V640H32q-14 0-23-9t-9-23V416q0-14 9-23t23-9h224V160q0-14 9-23t23-9h192q14 0 23 9t9 23v224h851l246-247q10-9 23-9t23 9q9 10 9 23t-9 23l-247 246v851h224q14 0 23 9t9 23z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE902" class="ladi-element">
                            <h4 class='ladi-headline'>MIỄN PHÍ THIẾT KÉ</h4></div>
                        <div id="HEADLINE903" class="ladi-element">
                            <p class='ladi-headline'>Miễn phí thiết kế, MIỄN PHÍ TEST MẪU. Ohnew luôn luôn hỗ trợ để quý vị có được mẫu sản phẩm ưng ý nhất.</p>
                        </div>
                    </div>
                </div>
                <div id="GROUP904" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="GROUP917" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX905" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE906" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 1920 1896.0833" fill="rgba(255, 174, 17, 1.0)">
                                            <path d="M1920 960q-1 32-288 96l-352 32-224 64h-64l-293 352h69q26 0 45 4.5t19 11.5-19 11.5-45 4.5H448v-32h64v-416H352l-192 224H64l-32-32v-192h32v-32h128v-8L0 1024V896l192-24v-8H64v-32H32V640l32-32h96l192 224h160V416h-64v-32h320q26 0 45 4.5t19 11.5-19 11.5-45 4.5h-69l293 352h64l224 64 352 32q261 58 287 93z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE907" class="ladi-element">
                            <h4 class='ladi-headline'>TỐC ĐỘ NHANH CHÓNG</h4></div>
                        <div id="HEADLINE908" class="ladi-element">
                            <p class='ladi-headline'>Thời gian sản xuất và giao hàng nhanh nhất thị trường nhờ quy mô lớn và đội ngũ tay nghề cao.</p>
                        </div>
                    </div>
                </div>
                <div id="GROUP909" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE910" class="ladi-element">
                            <p class='ladi-headline'>Ohnew cung cấp đầy đủ mọi giấy tờ liên quan. Đáp ứng mọi nhu cầu của đơn hàng chuẩn trên thị trường.</p>
                        </div>
                        <div id="HEADLINE911" class="ladi-element">
                            <h4 class='ladi-headline'>HÓA ĐƠN VAT</h4></div>
                        <div id="GROUP918" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX912" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE913" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                            <path d="M10,17L6,13L7.41,11.59L10,14.17L16.59,7.58L18,9M12,1L3,5V11C3,16.55 6.84,21.74 12,23C17.16,21.74 21,16.55 21,11V5L12,1Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="HEADLINE914" class="ladi-element">
                    <h6 class='ladi-headline'>Với kinh nghiệm &gt;12 năm trong ngành may mặc, chúng tôi đã hân hạnh được phục vụ hơn 1.000.000 quý khách hàng là các công ty, tổ chức lớn nhỏ. Ohnew tự tin cung cấp đến quý khách hàng những sản phẩm tốt nhất.<br><span style="font-weight: bold; color: rgb(214, 29, 35);">Sự hài lòng của quý khách chính là niềm vui của Ohnew!</span></h6></div>
                <div id="HEADLINE915" class="ladi-element">
					<h1 class="ladi-headline" style="font-size:50px">OHNEW</h1><br/>
                    <h2 class='ladi-headline' style="margin-top:10px">UY TÍN LÀM NÊN THƯƠNG HIỆU</h2>
				</div>
                <div id="GROUP919" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="GROUP920" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX921" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE922" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 1128.1586 1896.0833" fill="rgba(255, 174, 18, 1.0)">
                                            <path d="M978 1185q0 153-99.5 263.5T620 1585v175q0 14-9 23t-23 9H453q-13 0-22.5-9.5T421 1760v-175q-66-9-127.5-31T192 1509.5t-74-48-46.5-37.5-17.5-18q-17-21-2-41l103-135q7-10 23-12 15-2 24 9l2 2q113 99 243 125 37 8 74 8 81 0 142.5-43t61.5-122q0-28-15-53t-33.5-42-58.5-37.5-66-32-80-32.5q-39-16-61.5-25T349 948.5t-62.5-31T230 882t-53.5-42.5-43.5-49-35.5-58-21-66.5-8.5-78q0-138 98-242t255-134V32q0-13 9.5-22.5T453 0h135q14 0 23 9t9 23v176q57 6 110.5 23t87 33.5T881 302t39 29 15 14q17 18 5 38l-81 146q-8 15-23 16-14 3-27-7-3-3-14.5-12t-39-26.5-58.5-32-74.5-26T537 430q-95 0-155 43t-60 111q0 26 8.5 48t29.5 41.5 39.5 33 56 31 60.5 27 70 27.5q53 20 81 31.5t76 35 75.5 42.5 62 50 53 63.5T965 1091t13 94z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE923" class="ladi-element">
                            <h4 class='ladi-headline'>GIÁ RẺ NHẤT</h4></div>
                        <div id="HEADLINE924" class="ladi-element">
                            <p class='ladi-headline'>Với lợi thế bao trọn mọi khâu trong sản xuất. Ohnew luôn luôn tối ưu tốt nhất cho quý vị.</p>
                        </div>
                    </div>
                </div>
                <div id="GROUP925" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="GROUP926" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX927" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE928" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 1792 1896.0833" fill="rgba(255, 174, 18, 1.0)">
                                            <path d="M640 1408q0-52-38-90t-90-38-90 38-38 90 38 90 90 38 90-38 38-90zM256 896h384V640H482q-13 0-22 9L265 844q-9 9-9 22v30zm1280 512q0-52-38-90t-90-38-90 38-38 90 38 90 90 38 90-38 38-90zm256-1088v1024q0 15-4 26.5t-13.5 18.5-16.5 11.5-23.5 6-22.5 2-25.5 0-22.5-.5q0 106-75 181t-181 75-181-75-75-181H768q0 106-75 181t-181 75-181-75-75-181h-64q-3 0-22.5.5t-25.5 0-22.5-2-23.5-6-16.5-11.5-13.5-18.5-4-26.5q0-26 19-45t45-19V960q0-8-.5-35t0-38 2.5-34.5 6.5-37 14-30.5 22.5-30l198-198q19-19 50.5-32t58.5-13h160V320q0-26 19-45t45-19h1024q26 0 45 19t19 45z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE929" class="ladi-element">
                            <h4 class='ladi-headline'>MIỄN PHÍ GIAO HÀNG</h4></div>
                        <div id="HEADLINE930" class="ladi-element">
                            <p class='ladi-headline'>Áp dụng trên mọi đơn hàng có giá trị từ 5.000.000 đồng</p>
                        </div>
                    </div>
                </div>
                <div id="GROUP931" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="GROUP932" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX933" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="SHAPE934" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                            <path d="M3,1H19A1,1 0 0,1 20,2V6A1,1 0 0,1 19,7H3A1,1 0 0,1 2,6V2A1,1 0 0,1 3,1M3,9H19A1,1 0 0,1 20,10V10.67L17.5,9.56L11,12.44V15H3A1,1 0 0,1 2,14V10A1,1 0 0,1 3,9M3,17H11C11.06,19.25 12,21.4 13.46,23H3A1,1 0 0,1 2,22V18A1,1 0 0,1 3,17M8,5H9V3H8V5M8,13H9V11H8V13M8,21H9V19H8V21M4,3V5H6V3H4M4,11V13H6V11H4M4,19V21H6V19H4M17.5,12L22,14V17C22,19.78 20.08,22.37 17.5,23C14.92,22.37 13,19.78 13,17V14L17.5,12M17.5,13.94L15,15.06V17.72C15,19.26 16.07,20.7 17.5,21.06V13.94Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE935" class="ladi-element">
                            <h4 class='ladi-headline'>CAM KẾT CHẤT LƯỢNG</h4></div>
                        <div id="HEADLINE936" class="ladi-element">
                            <p class='ladi-headline'>Chúng tôi cam kết trên từng đường kim mũi chỉ và bảo hành từng chi tiết in thêu với mỗi sản phẩm đến tay quý khách.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SECTION12" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="BOX13" class="ladi-element">
                    <div class='ladi-box' style="background-image:url('https://w.ladicdn.com/s650x750/5dd3b80e2faf646a59273c48/lo-20191219094233.jpg')"></div>
                    <div class="ladi-overlay"></div>
                </div>
                <div id="BOX16" class="ladi-element">
                    <div class='ladi-box' style="background-image:url('https://w.ladicdn.com/s1000x750/5dd3b80e2faf646a59273c48/mlt-20191219115336.jpg')"></div>
                    <div class="ladi-overlay"></div>
                </div>
                <div id="BOX28" class="ladi-element">
                    <div class='ladi-box' style="background-image:url('https://w.ladicdn.com/s650x650/5dd3b80e2faf646a59273c48/hhhv_nha-trang-center_05_grande-20191219114612.jpg')"></div>
                    <div class="ladi-overlay"></div>
                </div>
                <div id="BOX531" class="ladi-element">
                    <div class='ladi-box' style="background-image:url('https://w.ladicdn.com/s650x650/5dd3b80e2faf646a59273c48/ao-thun-dong-phuc-su-kien-outing1-20191219114448.jpg')"></div>
                    <div class="ladi-overlay"></div>
                </div>
                <div id="PARAGRAPH567" class="ladi-element">
                    <p class='ladi-paragraph'><span style="font-weight: bold; color: rgb(232, 59, 48);">XƯỞNG MŨ LƯỠI TRAI OHNEW</span>
                        <br>Nhận làm mũ với số lượng từ 30 chiếc đến 1.000.0000 + chiếc
                        <br><span style="font-weight: bold;">- Giá rẻ nhất<br>- May nhanh nhất<br>- Tư vẫn hỗ trợ 24/7/365<br>- Giao hàng miễn phí tận nơi</span>
                        <br>
                        <br>Mũ lưỡi trai, mũ đồng phục, mũ sự kiện, mũ lưỡi trai Hà Nội
                        <br>
                    </p>
                </div>
            </div>
        </div>
        <div id="SECTION34" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="HEADLINE35" class="ladi-element">
                    <h3 class='ladi-headline'>XU HƯỚNG SẢN PHẨM HOT NHẤT</h3></div>
					
                <div id="LINE53" class="ladi-element">
                    <div class='ladi-line'>
                        <div class="ladi-line-container"></div>
                    </div>
                </div>
                <div id="GROUP486" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX37" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE38" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-moi-truong-viet-nam-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE39" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 01</h6></div>
                        <div id="GROUP138" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP487" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX378" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE379" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url('https://w.ladicdn.com/s700x700/5dd3b80e2faf646a59273c48/mu-thoi-trang-ford-2-20191219113305.png')"></div>
                            </div>
                        </div>
                        <div id="HEADLINE380" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 02</h6></div>
                        <div id="GROUP381" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP489" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX396" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE397" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url('https://w.ladicdn.com/s650x650/5dd3b80e2faf646a59273c48/mu-du-lich-duy-hung-3-20191219113305.png')"></div>
                            </div>
                        </div>
                        <div id="HEADLINE398" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 03</h6></div>
                        <div id="GROUP399" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
				<div id="GROUP488" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX387" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE388" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url('https://w.ladicdn.com/s650x650/5dd3b80e2faf646a59273c48/mu-castrol-2-20191219113305.png')"></div>
                            </div>
                        </div>
                        <div id="HEADLINE389" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 04</h6></div>
                        <div id="GROUP390" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <a href="tel:<?php echo $tel;?>" id="BUTTON541" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT541" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON542" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT542" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON544" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT544" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON546" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT546" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
		<div id="SECTION34" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="GROUP486" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX37" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE38" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-unfpa-2-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE39" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 05</h6></div>
                        <div id="GROUP138" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP487" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX378" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE379" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image: url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-doi-bong-dep-3-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE380" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 06</h6></div>
                        <div id="GROUP381" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP489" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX396" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE397" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image: url(https://w.ladicdn.com/s650x650/5dd3b80e2faf646a59273c48/mu-ct-2-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE398" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 07</h6></div>
                        <div id="GROUP399" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP488" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX387" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE388" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image: url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-thoi-trang-pc-2-20191219112808.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE389" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 08</h6></div>
                        <div id="GROUP390" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <a href="tel:<?php echo $tel;?>" id="BUTTON541" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT541" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON542" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT542" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON544" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT544" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON546" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT546" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
		<div style="height:100px;"></div>
        <div id="SECTION405" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="HEADLINE406" class="ladi-element">
                    <h3 class='ladi-headline'>SẢN PHẨM</h3>
				</div>
                <div id="LINE416" class="ladi-element">
                    <div class='ladi-line'>
                        <div class="ladi-line-container"></div>
                    </div>
                </div>
                <div id="GROUP492" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX407" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE408" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mau2-2-20191219112808.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE409" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 9</h6></div>
                        <div id="GROUP410" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP499" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX462" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE463" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-studio-den-2-20191219112808.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE464" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 10</h6></div>
                        <div id="GROUP465" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP493" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX417" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE418" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-rstore-2-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE419" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 11</h6></div>
                        <div id="GROUP420" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP497" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX453" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE454" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s600x600/5dd3b80e2faf646a59273c48/mu-du-hoc-tre-2-20191219113305.png)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE455" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 12</h6></div>
                        <div id="GROUP456" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <a href="tel:<?php echo $tel;?>" id="BUTTON548" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT548" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON550" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT550" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON552" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT552" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON554" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT554" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p> 
                        </div>
                    </div>
                </a>
            </div>
        </div>
		<div id="SECTION405" class="ladi-section" style="margin-top: -54px;">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="GROUP492" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX407" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE408" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s550x600/5dd3b80e2faf646a59273c48/home_5_cong_viec_noi_bat_img_5-20191219120035.jpg)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE409" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 13</h6></div>
                        <div id="GROUP410" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
				
                <div id="GROUP499" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX462" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE463" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s750x600/5dd3b80e2faf646a59273c48/ao-thun-su-kien-cao-cap-20191219120034.jpg)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE464" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 14</h6></div>
                        <div id="GROUP465" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP493" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX417" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE418" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s750x600/5dd3b80e2faf646a59273c48/dong-phuc-su-kien-1-20191219120035.jpg)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE419" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 15</h6></div>
                        <div id="GROUP420" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <div id="GROUP497" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX453" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="IMAGE454" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background" style="background-image:url(https://w.ladicdn.com/s900x700/5dd3b80e2faf646a59273c48/hhhv_nha-trang-center_05_grande-20191219114612.jpg)"></div>
                            </div>
                        </div>
                        <div id="HEADLINE455" class="ladi-element">
                            <h6 class='ladi-headline'>MŨ 16</h6></div>
                        <div id="GROUP456" class="ladi-element">
                            
                        </div>
                    </div>
                </div>
                <a href="tel:<?php echo $tel;?>" id="BUTTON548" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT548" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON550" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT550" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON552" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT552" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p>
                        </div>
                    </div>
                </a>
                <a href="tel:<?php echo $tel;?>" id="BUTTON554" class="ladi-element">
                    <div class='ladi-button'>
                        <div class="ladi-button-background"></div>
                        <div id="BUTTON_TEXT554" class="ladi-element">
                            <p class='ladi-headline'>MUA NGAY</p> 
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="web-container">
            <h2 class="video-title">VIDEO FEEBBACK KHÁCH HÀNG</h2>
            <div class="video-content ">
				<img src="https://w.ladicdn.com/s1300x600/5dd3b80e2faf646a59273c48/tgtvv-20191217105728.png" width="100%">
				<iframe class="video-youtube" src="https://www.youtube.com/embed/4CeAstUZyqQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<img src="https://w.ladicdn.com/s1350x650/5dd3b80e2faf646a59273c48/tgtvv-20191217105728.png" width="100%">
			</div>
		</div>
		<div id="SECTION696" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="PARAGRAPH697" class="ladi-element">
                    <p class='ladi-paragraph'>Sự hài lòng của quý vị chính là niềm vui lớn nhất của đội ngũ Ohnew!</p>
                </div>
                <div id="HEADLINE698" class="ladi-element">
                    <h2 class='ladi-headline'>Ý kiến khách hàng&nbsp;</h2></div>
                <div id="GROUP699" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX700" class="ladi-element" >
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay" style="background-image: url('https://blogtraitim.info/wp-content/uploads/2019/07/hinh-dai-dien-fb-dep-nhat.jpg');"></div>
                        </div>
                        <div id="GROUP701" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="SHAPE702" class="ladi-element" >
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE703" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE704" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE705" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE706" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE707" class="ladi-element">
                            <p class='ladi-headline'>MŨ vải rất đẹp, đường may khéo và chuẩn thiết kế. Cảm ơn Ohnew nhé!</p>
                        </div>
                        <div id="GROUP708" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="HEADLINE709" class="ladi-element">
                                    <h6 class='ladi-headline'>Phan Thu Thủy</h6></div>
                                <div id="HEADLINE710" class="ladi-element">
                                    <h6 class='ladi-headline'>Đã xác thực</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GROUP711" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX712" class="ladi-element">
                            <div class='ladi-box' style="background-image: url(http://hoahongmakeup.com/wp-content/uploads/2018/06/trang-diem-chup-anh-chan-dung-1.jpg);"></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="GROUP713" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="SHAPE714" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE715" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE716" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE717" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE718" class="ladi-element">
                            <p class='ladi-headline'>Nói chung là ưng! Giấy tờ chuẩn chỉ, đội quản lý chuyên nghiệp nên mình rất nhàn, gửi ý tưởng xong là yên tâm đợi hàng về.</p>
                        </div>
                        <div id="GROUP719" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="HEADLINE720" class="ladi-element">
                                    <h6 class='ladi-headline'>Bùi Hoài Thu</h6></div>
                                <div id="HEADLINE721" class="ladi-element">
                                    <h6 class='ladi-headline'>Đã xác thực</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GROUP722" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX723" class="ladi-element">
                            <div class='ladi-box' style="background-image: url(https://img.giaoduc.net.vn/w1050/uploaded/2019/bpcgtqvp/2019_09_21/gs_nguyen_thoi_trung.jpg);"></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="GROUP724" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="SHAPE725" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE726" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE727" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE728" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="SHAPE729" class="ladi-element">
                                    <div class='ladi-shape'>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255,174,15,1)">
                                            <path d="M12,15.89V6.59L13.71,10.63L18.09,11L14.77,13.88L15.76,18.16M22,9.74L14.81,9.13L12,2.5L9.19,9.13L2,9.74L7.45,14.47L5.82,21.5L12,17.77L18.18,21.5L16.54,14.47L22,9.74Z"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE730" class="ladi-element">
                            <p class='ladi-headline'>Áo bên mình lên dáng đẹp. Đội ngũ tư vấn, may đo rất nhiệt tình. Sẽ giới thiệu anh em đối tác về ohnew</p>
                        </div>
                        <div id="GROUP731" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="HEADLINE732" class="ladi-element">
                                    <h6 class='ladi-headline'>Nguyễn Văn Hoàng</h6></div>
                                <div id="HEADLINE733" class="ladi-element">
                                    <h6 class='ladi-headline'>Đã xác thực</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SECTION656" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="IMAGE657" class="ladi-element">
                    <div class='ladi-image'>
                        <div class="ladi-image-background" style="background-image: url('/media/upload/slide/sale_50.jpg');"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SECTION359" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-overlay"></div>
            <div class="ladi-container">
                <div id="PARAGRAPH360" class="ladi-element">
                    <p class='ladi-paragraph'>Giảm ngay 50% tổng giá trị đơn hàng.
                        <br>ƯU ĐÃI CÓ HẠN - chỉ áp dụng với 50 KH đăng ký nhanh nhất hôm nay!</p>
                </div>
                <div id="FORM361" data-config-id="" class="ladi-element">
                    <form autocomplete="off" method="post" id="landing-submit" class='ladi-form'>
                        <div id="FORM_ITEM362" class="ladi-element">
                            <div class="ladi-form-item-container">
                                <div class="ladi-form-item-background"></div>
                                <div class='ladi-form-item'>
                                    <input autocomplete="off" tabindex="1" name="name" required class="ladi-form-control" id="submit-name" type="text" placeholder="Họ và tên" value="" />
                                </div>
                            </div>
                        </div>
                        <div id="BUTTON365" class="ladi-element">
                            <div class='ladi-button'>
                                <div class="ladi-button-background"></div>
                                <div id="BUTTON_TEXT365" class="ladi-element" onClick="jQuery('#submit-form').click();">
                                    <p class='ladi-headline'>ĐĂNG KÝ NGAY !</p>
                                </div>
                            </div>
                        </div>
                        <div id="FORM_ITEM364" class="ladi-element">
                            <div class="ladi-form-item-container">
                                <div class="ladi-form-item-background"></div>
                                <div class='ladi-form-item'>
                                    <input autocomplete="off" tabindex="1" name="phone" required class="ladi-form-control" id="submit-phone" type="tel" placeholder="Nhập Số điện thoại" pattern="[0-9]{9,12}" value="" />
                                </div>
                            </div>
                        </div>
                        <div id="FORM_ITEM529" class="ladi-element">
                            <div class="ladi-form-item-container">
                                <div class="ladi-form-item-background"></div>
                                <div class='ladi-form-item'>
                                    <textarea autocomplete="off" tabindex="3" name="message" id="submit-message" class="ladi-form-control" placeholder="Để lại lời nhắn cho chúng tôi"></textarea>
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="submit-form" class="ladi-hidden"></button>
                    </form>
                </div>
                <div id="HEADLINE375" class="ladi-element">
                    <h2 class='ladi-headline'>NHẬN NGAY ƯU ĐÃI<br></h2></div>
                <div id="IMAGE573" class="ladi-element">
                    <div class='ladi-image'>
                        <div class="ladi-image-background" style="background-image:url('https://w.ladicdn.com/s800x800/5dd3b80e2faf646a59273c48/liixii-20191219073037.jpg')"></div>
                    </div>
                </div>
                <div id="GROUP585" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="BOX586" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="BOX587" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="BOX588" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="BOX589" class="ladi-element">
                            <div class='ladi-box'></div>
                            <div class="ladi-overlay"></div>
                        </div>
                        <div id="COUNTDOWN590" class="ladi-element">
                            <div class='ladi-countdown'>
                                <div id="COUNTDOWN_ITEM591" class="ladi-element">
                                    <div class="ladi-countdown-background"></div>
                                    <div class='ladi-countdown-text'><span id="days">00</span></div>
                                </div>
                                <div id="COUNTDOWN_ITEM592" class="ladi-element">
                                    <div class="ladi-countdown-background"></div>
                                    <div class='ladi-countdown-text'><span id="hours">00</span></div>
                                </div>
                                <div id="COUNTDOWN_ITEM593" class="ladi-element">
                                    <div class="ladi-countdown-background"></div>
                                    <div class='ladi-countdown-text'><span id="minutes">00</span></div>
                                </div>
                                <div id="COUNTDOWN_ITEM594" class="ladi-element">
                                    <div class="ladi-countdown-background"></div>
                                    <div class='ladi-countdown-text'><span id="seconds">00</span></div>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE595" class="ladi-element">
                            <h6 class='ladi-headline'>NGÀY</h6></div>
                        <div id="HEADLINE596" class="ladi-element">
                            <h6 class='ladi-headline'>GIỜ</h6></div>
                        <div id="HEADLINE597" class="ladi-element">
                            <h6 class='ladi-headline'>GIÂY</h6></div>
                        <div id="HEADLINE598" class="ladi-element">
                            <h6 class='ladi-headline'>PHÚT</h6></div>
                    </div>
                </div>
                <div id="HEADLINE937" class="ladi-element">
                    <h3 class='ladi-headline'>MIỄN PHÍ</h3></div>
            </div>
        </div>
        <div id="SECTION324" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="GROUP325" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE326" class="ladi-element">
                            <p class='ladi-headline'>Bảo hành từng sản phẩm</p>
                        </div>
                        <div id="HEADLINE327" class="ladi-element">
                            <h6 class='ladi-headline'>CAM KẾT CHẤT LƯỢNG</h6></div>
                        <div id="SHAPE328" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M12,12H19C18.47,16.11 15.72,19.78 12,20.92V12H5V6.3L12,3.19M12,1L3,5V11C3,16.55 6.84,21.73 12,23C17.16,21.73 21,16.55 21,11V5L12,1Z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GROUP329" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE330" class="ladi-element">
                            <p class='ladi-headline'>Hỗ trợ mọi lúc, mọi nơi</p>
                        </div>
                        <div id="HEADLINE331" class="ladi-element">
                            <h6 class='ladi-headline'>TƯ VẤN 24/7</h6></div>
                        <div id="SHAPE332" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M15,12H17A5,5 0 0,0 12,7V9A3,3 0 0,1 15,12M19,12H21C21,7 16.97,3 12,3V5C15.86,5 19,8.13 19,12M20,15.5C18.75,15.5 17.55,15.3 16.43,14.93C16.08,14.82 15.69,14.9 15.41,15.18L13.21,17.38C10.38,15.94 8.06,13.62 6.62,10.79L8.82,8.59C9.1,8.31 9.18,7.92 9.07,7.57C8.7,6.45 8.5,5.25 8.5,4A1,1 0 0,0 7.5,3H4A1,1 0 0,0 3,4A17,17 0 0,0 20,21A1,1 0 0,0 21,20V16.5A1,1 0 0,0 20,15.5Z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GROUP333" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE334" class="ladi-element">
                            <p class='ladi-headline'>Đảm bảo tiến độ</p>
                        </div>
                        <div id="HEADLINE335" class="ladi-element">
                            <h6 class='ladi-headline'>TỐC ĐỘ CỰC NHANH</h6></div>
                        <div id="SHAPE336" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 1536 1896.0833" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M896 544v448q0 14-9 23t-23 9H544q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h224V544q0-14 9-23t23-9h64q14 0 23 9t9 23zm416 352q0-148-73-273t-198-198-273-73-273 73-198 198-73 273 73 273 198 198 273 73 273-73 198-198 73-273zm224 0q0 209-103 385.5T1153.5 1561 768 1664t-385.5-103T103 1281.5 0 896t103-385.5T382.5 231 768 128t385.5 103T1433 510.5 1536 896z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GROUP337" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE338" class="ladi-element">
                            <p class='ladi-headline'>Đơn hàng trên 5.000.000đ</p>
                        </div>
                        <div id="HEADLINE339" class="ladi-element">
                            <h6 class='ladi-headline'>GIAO HÀNG MIỄN PHÍ</h6></div>
                        <div id="SHAPE340" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M16.36,4.27H18.55V2.13H16.36V1.07H18.22C17.89,0.43 17.13,0 16.36,0C15.16,0 14.18,0.96 14.18,2.13C14.18,3.31 15.16,4.27 16.36,4.27M10.04,9.39L13,6.93L17.45,9.6H10.25M19.53,12.05L21.05,10.56C21.93,9.71 21.93,8.43 21.05,7.57L19.2,9.39L13.96,4.27C13.64,3.73 13,3.41 12.33,3.41C11.78,3.41 11.35,3.63 11,3.95L7,7.89C6.65,8.21 6.44,8.64 6.44,9.17V9.71H5.13C4.04,9.71 3.16,10.67 3.16,11.84V12.27C3.5,12.16 3.93,12.16 4.25,12.16C7.09,12.16 9.5,14.4 9.5,17.28C9.5,17.6 9.5,18.03 9.38,18.35H14.5C14.4,18.03 14.4,17.6 14.4,17.28C14.4,14.29 16.69,12.05 19.53,12.05M4.36,19.73C2.84,19.73 1.64,18.56 1.64,17.07C1.64,15.57 2.84,14.4 4.36,14.4C5.89,14.4 7.09,15.57 7.09,17.07C7.09,18.56 5.89,19.73 4.36,19.73M4.36,12.8C1.96,12.8 0,14.72 0,17.07C0,19.41 1.96,21.33 4.36,21.33C6.76,21.33 8.73,19.41 8.73,17.07C8.73,14.72 6.76,12.8 4.36,12.8M19.64,19.73C18.11,19.73 16.91,18.56 16.91,17.07C16.91,15.57 18.11,14.4 19.64,14.4C21.16,14.4 22.36,15.57 22.36,17.07C22.36,18.56 21.16,19.73 19.64,19.73M19.64,12.8C17.24,12.8 15.27,14.72 15.27,17.07C15.27,19.41 17.24,21.33 19.64,21.33C22.04,21.33 24,19.41 24,17.07C24,14.72 22.04,12.8 19.64,12.8Z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="SECTION341" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-overlay"></div>
            <div class="ladi-container">
                <div id="LINE342" class="ladi-element">
                    <div class='ladi-line'>
                        <div class="ladi-line-container"></div>
                    </div>
                </div>
                <div id="GROUP349" class="ladi-element">
                    <div class='ladi-group'>
                        <div id="HEADLINE350" class="ladi-element">
                            <h4 class='ladi-headline'>LIÊN HỆ</h4></div>
                        <div id="HEADLINE351" class="ladi-element">
                            <p class='ladi-headline'>Địa chỉ: 183 đường K2, Cầu Diễn, Nam Từ Liêm. Hà Nội
                                <br>
                            </p>
                        </div>
                        <div id="SHAPE352" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 16, 1.0)">
                                    <path d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z"></path>
                                </svg>
                            </div>
                        </div>
                        <div id="SHAPE353" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M6.62,10.79C8.06,13.62 10.38,15.94 13.21,17.38L15.41,15.18C15.69,14.9 16.08,14.82 16.43,14.93C17.55,15.3 18.75,15.5 20,15.5A1,1 0 0,1 21,16.5V20A1,1 0 0,1 20,21A17,17 0 0,1 3,4A1,1 0 0,1 4,3H7.5A1,1 0 0,1 8.5,4C8.5,5.25 8.7,6.45 9.07,7.57C9.18,7.92 9.1,8.31 8.82,8.59L6.62,10.79Z"></path>
                                </svg>
                            </div>
                        </div>
                        <div id="HEADLINE354" class="ladi-element">
                            <p class='ladi-headline'>Hotline: <?php echo $tel; ?></p>
                        </div>
                        <div id="HEADLINE355" class="ladi-element">
                            <p class='ladi-headline'>Email: dongphucohnew@gmail.vn</p>
                        </div>
                        <div id="SHAPE356" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M20,4H4A2,2 0 0,0 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V6A2,2 0 0,0 20,4M20,18H4V8L12,13L20,8V18M20,6L12,11L4,6V6H20V6Z"></path>
                                </svg>
                            </div>
                        </div>
                        <div id="SHAPE357" class="ladi-element">
                            <div class='ladi-shape'>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(255, 174, 18, 1.0)">
                                    <path d="M16.36,14C16.44,13.34 16.5,12.68 16.5,12C16.5,11.32 16.44,10.66 16.36,10H19.74C19.9,10.64 20,11.31 20,12C20,12.69 19.9,13.36 19.74,14M14.59,19.56C15.19,18.45 15.65,17.25 15.97,16H18.92C17.96,17.65 16.43,18.93 14.59,19.56M14.34,14H9.66C9.56,13.34 9.5,12.68 9.5,12C9.5,11.32 9.56,10.65 9.66,10H14.34C14.43,10.65 14.5,11.32 14.5,12C14.5,12.68 14.43,13.34 14.34,14M12,19.96C11.17,18.76 10.5,17.43 10.09,16H13.91C13.5,17.43 12.83,18.76 12,19.96M8,8H5.08C6.03,6.34 7.57,5.06 9.4,4.44C8.8,5.55 8.35,6.75 8,8M5.08,16H8C8.35,17.25 8.8,18.45 9.4,19.56C7.57,18.93 6.03,17.65 5.08,16M4.26,14C4.1,13.36 4,12.69 4,12C4,11.31 4.1,10.64 4.26,10H7.64C7.56,10.66 7.5,11.32 7.5,12C7.5,12.68 7.56,13.34 7.64,14M12,4.03C12.83,5.23 13.5,6.57 13.91,8H10.09C10.5,6.57 11.17,5.23 12,4.03M18.92,8H15.97C15.65,6.75 15.19,5.55 14.59,4.44C16.43,5.07 17.96,6.34 18.92,8M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z"></path>
                                </svg>
                            </div>
                        </div>
                        <div id="HEADLINE358" class="ladi-element">
                            <p class='ladi-headline'>Website: ohnew.vn</p>
                        </div>
                    </div>
                </div>
                <div id="PARAGRAPH348" class="ladi-element">
                    <p class='ladi-paragraph'>MŨ vải, MŨ vải bố, MŨ cavas, MŨ vải quà tặng, MŨ vải hà nội, MŨ vải không dệt, MŨ vải tote. MŨ tote</p>
                </div>
            </div>
        </div>
        <div id="SECTION_POPUP" class="ladi-section">
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="POPUP512" class="ladi-element">
                    <div class='ladi-popup'>
                        <div class="ladi-overlay"></div>
                        <div id="IMAGE513" class="ladi-element">
                            <div class='ladi-image'>
                                <div class="ladi-image-background"></div>
                            </div>
                        </div>
                        <div id="HEADLINE514" class="ladi-element">
                            <h4 class='ladi-headline'>Nhận ngay mã giảm giá cho toàn bộ sản phẩm</h4></div>
                        <div id="GROUP522" class="ladi-element">
                            <div class='ladi-group'>
                                <div id="BOX516" class="ladi-element">
                                    <div class='ladi-box'></div>
                                    <div class="ladi-overlay"></div>
                                </div>
                                <div id="HEADLINE517" class="ladi-element">
                                    <h2 class='ladi-headline'>sALES 50%</h2></div>
                            </div>
                        </div>
                        <div id="FORM518" data-config-id="" class="ladi-element">
                            <form autocomplete="off" method="post" class='ladi-form'>
                                <div id="FORM_ITEM524" class="ladi-element">
                                    <div class="ladi-form-item-container">
                                        <div class="ladi-form-item-background"></div>
                                        <div class='ladi-form-item'>
                                            <input autocomplete="off" tabindex="2" name="name" required class="ladi-form-control" type="text" placeholder="Họ và tên" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div id="FORM_ITEM525" class="ladi-element">
                                    <div class="ladi-form-item-container">
                                        <div class="ladi-form-item-background"></div>
                                        <div class='ladi-form-item'>
                                            <input autocomplete="off" tabindex="2" name="phone" required class="ladi-form-control" type="tel" placeholder="Số điện thoại" pattern="(\+84|0){1}(9|8|7|5|3){1}[0-9]{8}" value="" />
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="ladi-hidden"></button>
                            </form>
                        </div>
                        <div id="BUTTON520" class="ladi-element">
                            <div class='ladi-button'>
                                <div class="ladi-button-background"></div>
                                <div id="BUTTON_TEXT520" class="ladi-element">
                                    <p class='ladi-headline'>NHẬN MÃ GIẢM GIÁ</p>
                                </div>
                            </div>
                        </div>
                        <div id="HEADLINE942" class="ladi-element">
                            <h3 class='ladi-headline'>Hãy là 30 KH nhanh nhất hôm nay!</h3></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="backdrop-popup" class="backdrop-popup"></div>
    <div id="lightbox-screen" class="lightbox-screen"></div>
    <div>
        <div id="ex1" class="modal" style="line-height:20px">
            <p><a id="more" href="#more">Đăng ký thành công</a></p>
            <br/>
            <p>Cảm ơn anh/chị đã để lại thông tin! Ohnew sẽ liên hệ để hỗ trợ anh/chị ngay ạ!</p>
        </div>

        <!-- Link to open the modal -->
        <p><a href="#ex1" style="display:none" id="open-modal" rel="modal:open">Open Modal</a></p>
    </div>
	<a href="tel:<?php echo $tel; ?>" class="call-phone">
		<?php echo ' '.$tel; ?>
	</a>
	<a href="tel:<?php echo $tel; ?>" class="call-phone-mobile">
		<img src="/images/phone2.gif" width="120px" />
	</a>
    <script id="script_lazyload" type="text/javascript">
        (function() {
            var list_element_lazyload = document.querySelectorAll('.ladi-section-background, .ladi-image-background, .ladi-button-background, .ladi-headline, .ladi-video-background, .ladi-countdown-background, .ladi-box, .ladi-frame, .ladi-form-item-background, .ladi-gallery-view-item, .ladi-gallery-control-item, .ladi-spin-lucky-screen, .ladi-spin-lucky-start, .ladi-list-paragraph ul li');
            var style_lazyload = document.getElementById('style_lazyload');
            for (var i = 0; i < list_element_lazyload.length; i++) {
                var rect = list_element_lazyload[i].getBoundingClientRect();
                if (rect.x == "undefined" || rect.x == undefined || rect.y == "undefined" || rect.y == undefined) {
                    rect.x = rect.left;
                    rect.y = rect.top;
                }
                var offset_top = rect.y + window.scrollY;
                if (offset_top >= window.scrollY + window.innerHeight || window.scrollY >= offset_top + list_element_lazyload[i].offsetHeight) {
                    list_element_lazyload[i].classList.add('ladi-lazyload');
                }
            }
            style_lazyload.parentElement.removeChild(style_lazyload);
            var currentScrollY = window.scrollY;
            var stopLazyload = function(event) {
                if (event.type == "scroll" && window.scrollY == currentScrollY) {
                    currentScrollY = -1;
                    return;
                }
                window.removeEventListener('scroll', stopLazyload);
                list_element_lazyload = document.getElementsByClassName('ladi-lazyload');
                while (list_element_lazyload.length > 0) {
                    list_element_lazyload[0].classList.remove('ladi-lazyload');
                }
            };
            window.addEventListener('scroll', stopLazyload);
        })();
    </script>
    <!--[if lt IE 9]><script src="https://w.ladicdn.com/v2/source/html5shiv.min.js?v=1576117580504"></script><script src="https://w.ladicdn.com/v2/source/respond.min.js?v=1576117580504"></script><![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Open Sans:bold,regular|Montserrat:bold,regular|Roboto:bold,regular&display=swap" rel="stylesheet" type="text/css">
    <link href="/style/style.css" rel="stylesheet" type="text/css">
    <script src="/style/script.js" type="text/javascript"></script>
    <script id="script_event_data" type="text/javascript">
        (function() {
            var run = function() {
                if (typeof window.LadiPageScript == "undefined" || window.LadiPageScript == undefined || typeof window.ladi == "undefined" || window.ladi == undefined) {
                    setTimeout(run, 100);
                    return;
                }
                // window.LadiPageApp = window.LadiPageApp || new window.LadiPageAppV2();
                // window.LadiPageScript.runtime.ladipage_id = '5df0912499a35f4bec1a35ae';
                // window.LadiPageScript.runtime.isMobileOnly = false;
                // window.LadiPageScript.runtime.DOMAIN_SET_COOKIE = ["ladi.me"];
                // window.LadiPageScript.runtime.DOMAIN_FREE = ["pagedemo.me", "demopage.me", "ladi.me"];
                // window.LadiPageScript.runtime.bodyFontSize = 16;
                // window.LadiPageScript.runtime.time_zone = 7;
                // window.LadiPageScript.runtime.eventData = "%7B%22FORM361%22%3A%7B%22type%22%3A%22form%22%2C%22option.form_send_ladipage%22%3Atrue%2C%22option.thankyou_type%22%3A%22default%22%2C%22option.thankyou_value%22%3A%22C%C3%A1m%20%C6%A1n%20b%E1%BA%A1n%20%C4%91%C3%A3%20quan%20t%C3%A2m%22%2C%22option.form_auto_funnel%22%3Atrue%2C%22option.form_auto_complete%22%3Atrue%7D%2C%22FORM_ITEM362%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22text%22%7D%2C%22FORM_ITEM364%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22tel%22%7D%2C%22POPUP512%22%3A%7B%22type%22%3A%22popup%22%2C%22option.popup_welcome_page_scroll_to%22%3A%22SECTION405%22%2C%22desktop.option.popup_position%22%3A%22default%22%2C%22desktop.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%2C%22mobile.option.popup_position%22%3A%22default%22%2C%22mobile.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%7D%2C%22HEADLINE514%22%3A%7B%22type%22%3A%22headline%22%2C%22desktop.style.animation-name%22%3A%22shake%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22shake%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%2C%22FORM518%22%3A%7B%22type%22%3A%22form%22%2C%22option.form_send_ladipage%22%3Atrue%2C%22option.thankyou_type%22%3A%22default%22%2C%22option.thankyou_value%22%3A%22C%C3%A1m%20%C6%A1n%20b%E1%BA%A1n%20%C4%91%C3%A3%20quan%20t%C3%A2m%22%2C%22option.form_auto_funnel%22%3Atrue%2C%22option.form_auto_complete%22%3Atrue%7D%2C%22FORM_ITEM524%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22text%22%7D%2C%22FORM_ITEM525%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22tel%22%7D%2C%22FORM_ITEM529%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22textarea%22%7D%2C%22BUTTON541%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22phone%22%2C%22action%22%3A%220986394897%22%7D%7D%2C%22BUTTON546%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22phone%22%2C%22action%22%3A%220986394897%22%7D%7D%2C%22BUTTON544%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22phone%22%2C%22action%22%3A%220986394897%22%7D%7D%2C%22BUTTON542%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22popup%22%2C%22action%22%3A%22POPUP512%22%7D%7D%2C%22COUNTDOWN_ITEM591%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22day%22%7D%2C%22COUNTDOWN_ITEM592%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22hour%22%7D%2C%22COUNTDOWN_ITEM593%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22minute%22%7D%2C%22COUNTDOWN_ITEM594%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22seconds%22%7D%2C%22COUNTDOWN590%22%3A%7B%22type%22%3A%22countdown%22%2C%22option.countdown_type%22%3A%22countdown%22%2C%22option.countdown_minute%22%3A1435%7D%2C%22GROUP585%22%3A%7B%22type%22%3A%22group%22%2C%22desktop.option.sticky%22%3Atrue%2C%22desktop.option.sticky_position%22%3A%22default%22%2C%22desktop.option.sticky_position_top%22%3A%220px%22%2C%22desktop.option.sticky_position_left%22%3A%220px%22%2C%22desktop.option.sticky_position_bottom%22%3A%220px%22%2C%22desktop.option.sticky_position_right%22%3A%220px%22%7D%2C%22HEADLINE937%22%3A%7B%22type%22%3A%22headline%22%2C%22desktop.style.animation-name%22%3A%22shake%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22shake%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%7D";
                // window.LadiPageScript.run(true);
                // window.LadiPageScript.runEventScroll();
            };
            run();
        })();
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('#landing-submit').submit(function(e) {
                e.preventDefault();
                var txtFullname = jQuery('#submit-name');
                var txtPhone = jQuery('#submit-phone');
                var txtMessage = jQuery('#submit-message');
                var url = window.location.href ;
                var ajax_loading = jQuery('#ajax_loading');

                if (txtFullname.val() == "") {
                    alert("Bạn chưa nhập tên");
                    txtFullname.focus();
                } else if (txtPhone.val() == "") {
                    alert("Bạn chưa nhập số điện thoại");
                    txtPhone.focus();
                } else {

                    var UrlToPass = 'action=gui_baogia_new&name=' + txtFullname.val() + '&phone=' + txtPhone.val() + '&message=' + txtMessage.val()+'&url='+url;
                    //console.log(UrlToPass);
                    ajax_loading.removeClass('hidden');
                    jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
                        type: 'POST',
                        data: UrlToPass,
                        url: 'lienhe_xuly.php',
                        success: function(responseText) { // Get the result and asign to each cases
                            ajax_loading.addClass('hidden');
                            if (responseText == 1) {
                                $('#open-modal').click();
                            } else if (responseText == 4) {
                                alert("có lỗi trong quá trình liên hệ báo giá, hãy thử lại");
                            }
                        }
                    });

                }

            });
        });
        $("#top-submit").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#SECTION359").offset().top
            }, 2000);
        });
		$(".menu-intro").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#HEADLINE915").offset().top
            }, 2000);
        });
		$(".menu-product").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#SECTION34").offset().top
            }, 2000);
        });
		$(".menu-customer").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#SECTION696").offset().top
            }, 2000);
        });
		$(".menu-promo").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#SECTION359").offset().top
            }, 2000);
        });
		$(".menu-contact").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#SECTION324").offset().top
            }, 2000);
        });
    </script>
	<script type="text/javascript"> // count down
		// Set the date we're counting down to
		var distance = 10000000; // 10 nghìn giây
		// Update the count down every 1 second
		var x = setInterval(function() {
			
		  // Count down
		  distance = distance - 1000;
			
		  // Time calculations for days, hours, minutes and seconds
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
		  // If the count down is over, write some text 
			jQuery("#days").html(days);
			jQuery("#hours").html(hours);
			jQuery("#minutes").html(minutes);
			jQuery("#seconds").html(seconds);
		}, 1000);
	</script>
</body>

</html>
<?php include('ajax_loading.php'); ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/5d9ea1c0f82523213dc6889d/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();
</script>
<!--End of Tawk.to Script-->
<!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>
	window.fbAsyncInit = function() {
	  FB.init({
		xfbml            : true,
		version          : 'v5.0'
	  });
	};

	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
	fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your customer chat code -->
  <div class="fb-customerchat"
	attribution=setup_tool
	page_id="440961062614280"
theme_color="#fa3c4c"
logged_in_greeting="Chào bạn! chúng tôi có thể giúp gì được bạn ạ."
logged_out_greeting="Chào bạn! chúng tôi có thể giúp gì được bạn ạ.">
  </div>