﻿<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<style>
    .form-group .control-label span{
        color: red;
    }
</style>

<h3>Thông tin hợp đồng</h3>

<?php $flash->display(); ?>

<form name="frm" method="post" action="<?=url(null, 'edit')?>" enctype="multipart/form-data" class="nhaplieu form-horizontal">

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Tên <span>*</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" value="<?=@$item['name']?>" placeholder="Tên hợp đồng">
        </div>
    </div>

    <div class="contract-img">
        <?php if (isset($item['images'])) foreach ($item['images'] as $index => $img) { ?>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <div class="_preview">
                        <img src="<?=$img?>" alt="Vui lòng upload lại ảnh này">
                    </div>
                    <input type="file"">
                </div>
                <div class="col-sm-3">
                    <button type="button" data-index="<?=$index?>"><img src="media/images/delete.png" border="0" /></button>
                </div>
            </div>
        <?php } ?>

        <?php if (empty($_GET['id'])) { ?>
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-7">
                    <div class="_preview"></div>
                    <input type="file"">
                </div>
                <div class="col-sm-3">
                    <button type="button"><img src="media/images/delete.png" border="0" /></button>
                </div>
            </div>
        <?php } ?>
    </div>

    <input type="hidden" name="id" id="id" value="<?= @$item['id'] ?>" />
    <input type="hidden" name="images_del" id="images_del" />
    <input type="submit" value="Lưu" class="btn" />
    <input type="button" value="Thêm ảnh" class="btn" onclick="addContractPage()"/>
    <input type="button" value="Thoát" onclick="javascript:window.location = '<?=router('admin', ['com' => 'contract'])?>'" class="btn" />
</form>

<script>
    setTimeout(reIndexName, 0);

    function addContractPage() {
        var html = '<div class="form-group">' +
            '            <label class="col-sm-2 control-label"></label>' +
            '            <div class="col-sm-7">' +
            '                <div class="_preview"></div>' +
            '                <input type="file">' +
            '            </div>' +
            '            <div class="col-sm-3">' +
            '                <button type="button"><img src="media/images/delete.png" border="0" /></button>' +
            '            </div>' +
            '        </div>';
        $('.contract-img').append(html);

        reIndexName();
    }

    function reIndexName() {
        $('.contract-img .form-group').each(function (i, el) {
            var $el = $(el);
            var previewId = 'preview' + i;

            $el.find('.control-label').html('Trang ' + (i + 1) + '<span>*</span>');
            $el.find('._preview').attr('id', previewId);

            var $input = $el.find('input[type="file"]');
            $input.attr('name', 'page_' + i);
            $input[0].onchange = function () {
                preview(this, ('#' + previewId));
            };

            $el.find('button')[0].onclick = function () {
                console.log(this);
                if (this.dataset.index !== undefined) {
                    var $images_del = $('#images_del');
                    var cur_val = $images_del.val();
                    var new_val = cur_val + ',' + this.dataset.index;

                    console.log(this.dataset.index);
                    console.log(cur_val);
                    console.log(new_val);

                    $images_del.val(new_val);
                }

                $(this).parents('.form-group').remove();
                reIndexName();
            };
        });
    }

    function preview(el, target) {
        var files = el.files;
        // console.log(el);
        if (files.length) {
            var fileName = files[0].name;
            // $(this).next('.custom-file-label').html(fileName);

            var reader = new FileReader();
            reader.onload = function (e) {
                $(target).html('<img src="' + e.target.result + '">');

                var img = new Image();
                img.onload = function () {// image is loaded; sizes are available
                    if (img.width > img.height) {
                        // previewLandscape();
                    } else {
                        // previewPortrait();
                    }
                };
                img.src = reader.result;
            };
            reader.readAsDataURL(files[0]);
        }
    }
</script>