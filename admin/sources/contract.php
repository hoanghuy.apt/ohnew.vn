<?php

if (!defined('_source')) die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "edit":
        if (isPost()) {
            return save();
        }

        show();

        $template = "contract/edit";
        break;
    case "del":
        delete();
        break;
    default:
        listItems();
        $template = "contract/index";
}

function listItems()
{
    global $d, $items;

    $sql = "select * from #_contract order by id desc";
    $d->query($sql);

    $items = $d->result_array();
}

function save()
{
    global $d, $flash;
    $d->setTable('contract');

    $id = isset($_POST['id']) ? themdau($_POST['id']) : null;
    $name = $_POST['name'];

    $data = [];

    if (empty($name)) {
        $flash->error('Vui lòng nhập tên hợp đồng');
        return redirectTo(url());
    }

    $data['name'] = $name;

    if ($id) {
        $d->setWhere('id', $id);
        $d->select();
        $row = $d->fetch_array();
        $images = json_decode($row['images'], true);

        if (!empty($_POST['images_del'])) {
            $del = explode(',', $_POST['images_del']);
            foreach ($del as $index) {
                if ($index === '' or $index === null) continue;
                unset($images[$index]);
            }
        }

        $images = array_values($images);

        foreach ($_FILES as $key => $file) {
            $k = $key{5};
            if (empty($file['name'])) {
                $images[$k] = isset($images[$k]) ? $images[$k] : '';
                continue;
            }
            $file_name = genFileName($file['name']);
            $photo = upload_image($key, 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_contract, $file_name);
            $images[$k] = _upload_contract . $photo;
        }

        $images = array_values($images);

        $data['images'] = json_encode($images);

        if ($d->update($data)) {
            $flash->success('Cập nhật thông tin hợp đồng thành công');
            return redirectTo(router('admin', ['com' => 'contract']));
        }

        return transfer("Cập nhật dữ liệu bị lỗi", url());
    }

    $images = [];

    foreach ($_FILES as $key => $file) {
        if (empty($file['name'])) {
            $images[$key{5}] = '';
            continue;
        }
        $file_name = genFileName($file['name']);
        $photo = upload_image($key, 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_contract, $file_name);
        $images[$key{5}] = _upload_contract . $photo;
    }

    $data['images'] = json_encode($images);

    if ($d->insert($data)) {
        $flash->success('Thêm mới hợp đồng thành công');
        return redirectTo(router('admin', ['com' => 'contract']));
    }

    $flash->error('Lưu dữ liệu bị lỗi');
    return redirectTo(url());
}

function show()
{
    global $d, $item;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $sql = "select * from #_contract where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() == 0) {
            return transfer("Dữ liệu không có thực", url());
        }

        $item = $d->fetch_array();
        $item['images'] = json_decode($item['images'], true);
    }
}

function delete()
{
    global $d, $flash;

    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $d->reset();
        $sql = "select * from #_contract where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() > 0) {
            $sql = "delete from #_contract where id='" . $id . "'";
            $d->query($sql);
        }

        $flash->success('Xóa đối tác thành công');

        return redirectTo(router('admin', ['com' => 'contract']));
    }

    return transfer("Không nhận được dữ liệu", router('admin', ['com' => 'contract']));
}

?>


