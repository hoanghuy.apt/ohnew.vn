<?php

function baseUrl()
{
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
    $domain = $_SERVER['SERVER_NAME'];

    if ($_SERVER['SERVER_PORT'] != 80) $domain .= ':' . $_SERVER['SERVER_PORT'];

    return $protocol . '://' . $domain;
}

function url($com = null, $act = null, $params = [])
{
    $url = baseUrl() . $_SERVER['REQUEST_URI'];
    $query_str = $_SERVER['QUERY_STRING'];

    $route = [];

    if (!empty($com)) $route['com'] = $com;
    if (!empty($act)) $route['act'] = $act;

    $params = $route + $params;

    return str_replace($query_str, '', $url) . _createQueryString($url, $params);
}

function router($uri = '', $params = [])
{
    return baseUrl() . '/' . (empty($uri) ? '' : $uri) . (empty($params) ? '' : ('?' . http_build_query($params)));
}

function extractQueryString($url)
{
    $query_str = parse_url($url, PHP_URL_QUERY);
    $parts = explode('&', $query_str);
    $return = array();
    foreach ($parts as $part) {
        $param = explode('=', $part);
        $return[str_replace('amp;', "", $param[0])] = isset($param[1]) ? $param[1] : '';
    }
    return $return;
}

function _createQueryString($url, $params = [])
{
    $query = extractQueryString($url);
    $query = array_merge($query, $params);
    return http_build_query($query);
}

function requestMethod()
{
    return $_SERVER['REQUEST_METHOD'];
}

function isPost()
{
    return isMethod('POST');
}

function isMethod($method = null)
{
    return requestMethod() === $method;
}

function redirectTo($url, $statusCode = 303)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}

function genFileName($name)
{
    return explode('.', $name)[0] . randNumber(0, 9, 10);
}

function randNumber($min, $max, $num)
{
    $result = '';
    for ($i = 0; $i < $num; $i++) {
        $result .= rand($min, $max);
    }
    return $result;
}

function gioihankitu($str, $limit)
{
    if (strlen($str) > $limit) {
        $re = substr($str, 0, $limit);
        $re = substr($re, 0, strrpos($re, " "));
        $re .= "...";
        return $re;
    } else {
        return $str;
    }
}

function dump($data, $die = false)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';

    if ($die) exit(0);
}

function dd($data)
{
    return dump($data, true);
}