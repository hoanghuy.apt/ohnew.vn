<?php

if (!defined('_source')) die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "edit":
        if (isPost()) {
            return save();
        }

        show();

        $template = "gallery/edit";
        break;
    case "del":
        delete();
        break;
    default:
        listItems();
        $template = "gallery/index";
}

function listItems()
{
    global $d, $items;

    $sql = "select * from #_gallery order by id desc";
    $d->query($sql);

    $items = $d->result_array();
}

function save()
{
    global $d, $flash;
    $d->setTable('gallery');

    $id = isset($_POST['id']) ? themdau($_POST['id']) : null;
    $type = $_POST['type'];
    $video = isset($_POST['video']) ? $_POST['video'] : null;
    $is_show = (isset($_POST['is_show']) and $_POST['is_show'] == 'on') ? 1 : 0;

    $data = [];
    if ($id) {
        //    dump($_FILES);
        //    dd($is_show);
        $d->setWhere('id', $id);
        $d->select();
        $data = $d->fetch_array();
        $data['type'] = $type;
        $data['is_show'] = $is_show;

				
        if ($type == 'video') {
            if (empty($video)) {
                $flash->error('Vui setup mã nhúng của youtube');
                return redirectTo(url());
            }
            $data['resource'] = htmlspecialchars($video);
        } else {
            if (!empty($_FILES['photo']['name'])) {
                $file_name = genFileName($_FILES['photo']['name']);
                $photo = upload_image('photo', 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_gallery, $file_name);
                $data['resource'] = _upload_gallery . $photo;
            }
        }

        if ($d->update($data)) {
            $flash->success('Cập nhật thông tin thành công');
            return redirectTo(router('admin', ['com' => 'gallery']));
        }

        return transfer("Cập nhật dữ liệu bị lỗi", url());
    }

    $data['type'] = $type;
    $data['is_show'] = $is_show;

    if ($type == 'video') {
        if (empty($video)) {
            $flash->error('Vui setup mã nhúng của youtube');
            return redirectTo(url());
        }
        $data['resource'] = htmlspecialchars($video);
    } else {
        if (empty($_FILES['photo']['name'])) {
            $flash->error('Vui lòng chọn ảnh');
            return redirectTo(url());
        }
        $file_name = genFileName($_FILES['photo']['name']);
        $photo = upload_image('photo', 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_gallery, $file_name);
        $data['resource'] = _upload_gallery . $photo;
    }

    if ($d->insert($data)) {
        $flash->success('Thêm dữ liệu mới vào bộ sưu tập thành công');
        return redirectTo(router('admin', ['com' => 'gallery']));
    }

    $flash->error('Lưu dữ liệu bị lỗi');
    return redirectTo(url());
}

function show()
{
    global $d, $item;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $sql = "select * from #_gallery where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() == 0) {
            return transfer("Dữ liệu không có thực", url());
        }

        $item = $d->fetch_array();
    }
}

function delete()
{
    global $d, $flash;

    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $d->reset();
        $sql = "select * from #_gallery where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() > 0) {
            $sql = "delete from #_gallery where id='" . $id . "'";
            $d->query($sql);
        }

        $flash->success('Xóa thành công');

        return redirectTo(router('admin', ['com' => 'gallery']));
    }

    return transfer("Không nhận được dữ liệu", router('admin', ['com' => 'gallery']));
}
