<?php
    $sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and giuaduoi=1 order by uutien limit 0,1 ");
    $news = [];

    while ($cate = mysql_fetch_object($sql_1)) {
        $row = $cate;
        $sql = mysql_query('select * from table_news where id_cat=' . $cate->id . ' and noibat=1 order by uutien');
        while ($new = mysql_fetch_object($sql)) {
            $row->news[] = $new;
        }
        $news[] = $row;
    }

    //Đối tác
    $sql_partner = mysql_query("select * from table_partner order by id");
    $partners = [];

    if (mysql_num_rows($sql_partner) > 0) {
        while ($partner = mysql_fetch_object($sql_partner)) {
            $partners[] = $partner;
        }
    }

    //Hợp đồng
    $sql_contract = mysql_query("select * from table_contract order by id");
    $contracts = [];

    if (mysql_num_rows($sql_contract) > 0) {
        while ($contract = mysql_fetch_object($sql_contract)) {
            $contract->images = json_decode($contract->images, true);
            $contracts[] = $contract;
        }
    }

    //Bộ sưu tập
    $sql_gallery_video = mysql_query("select * from table_gallery where type = 'video' and is_show = 1 order by id desc limit 0, 1");
    $galleries = [
        'video' => (object)[
            'resource' => ''
        ],
        'photos' => []
    ];

    $lg_photos = [
        './media/system/images/img5.jpg',
        './media/system/images/img5.jpg',
    ];
    $sm_photos = [
        './media/system/images/img5.jpg',
        './media/system/images/img5.jpg',
        './media/system/images/img5.jpg',
    ];

    if (mysql_num_rows($sql_gallery_video) > 0) {
        $galleries['video'] = mysql_fetch_object($sql_gallery_video);
    }

    $sql_gallery_photo = mysql_query("select * from table_gallery where type = 'photo' and is_show = 1 order by id desc limit 0, 5");

    if (mysql_num_rows($sql_gallery_photo) > 0) {
        $photos = [];
        while ($p = mysql_fetch_object($sql_gallery_photo)) {
            $galleries['photos'][] = $p;
            array_push($photos,$p);
        }
			// var_dump($photos[0]);
        if (isset($photos[0])) $lg_photos[0] = $photos[0];
        if (isset($photos[1])) $lg_photos[1] = $photos[1];

        if (isset($photos[2])) $sm_photos[0] = $photos[2];
        if (isset($photos[3])) $sm_photos[1] = $photos[3];
        if (isset($photos[4])) $sm_photos[2] = $photos[4];

    }

    //Tin tức ohnew
    $ohnew_news = [];

    $sql = mysql_query("select * from table_news order by id desc limit 0, 3");

    while ($new = mysql_fetch_object($sql)) {
        $ohnew_news[] = $new;
    }

//    dump($ohnew_news);
?>

<main>
    <section class="intro">
        <div class="container">
            <div class="intro_content row">
                <div class="col-lg-8">
                    <div class="video">
                        <?=$timkiem_lienhe?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <article class="content rounded shadow pr">
                        <!--<h4>CÔNG TY CỔ PHẦN OHNEW</h4>
                        <p>Là công ty chuyên sản xuất và xuất khẩu sản phẩm thời trang
                            trong và ngoài nước</p>
                        <p>
                            Ra đời từ năm 2013 với tiền thân là thương hiệu thời trang Freestyle(www.shopaophong.vn) - một thương hiệu của người việt đã khẳng định được tên tuổi ở thị trường trong nước từ năm 2007 đến này.
                            Sau một thời gian dài phát triển mạnh trong nghành may mặc, chung tôi tự tin nhận thấy đã làm chủ được tay nghề và chất lượng sản phẩm quyết định bước một bước tiền lơn hơn.
                            Đồng phục Cú Vọ ra đời từ đó, với những mong muốn đáp ứng tất cả các yêu cầu về đồng phục cho các bạn trẻ theo cách chuyên nghiệp nhất.
                        </p>
                        <a href="" class="btn">TÌM HIỂU VỀ CHÚNG TÔI</a>-->
                        <?=$gioithieu_lienhe?>
                        <a href="/lien-he.html" class="btn">TÌM HIỂU VỀ CHÚNG TÔI</a>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <div class="row">
                <?php foreach ($news as $key => $val): ?>
                    <?php foreach ($val->news as $k => $v): ?>
                        <div class="col-lg-4 col-md-6 mb-4">
                            <article class="post rounded shadow">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#fabrics_popup_<?=$v->url?>" class="thumb border">
                                    <!--<img src="media/upload/news/<?=$v->photo?>" alt="<?=$v->ten?>">-->
									<div class="bg-img2" style="background-image:url(media/upload/news/<?=$v->photo?>)"></div>
                                </a>
                                <h3>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#fabrics_popup_<?=$v->url?>"><?=$v->ten?></a>
                                </h3>
                                <p><?=gioihankitu($v->tomtat,150)?></p>
                            </article>
                        </div>

                        <div class="modal fade fabrics_popup" id="fabrics_popup_<?php echo $v->url; ?>" tabindex="-1"
                             role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="z-index: 1;">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    <div class="modal-body">
                                        <div class="pl-3 pr-3 row">
                                            <div class="col-12 text-center">
                                                <h2 class="mt-0 mr-2 mb-3 ml-2"><?php echo $v->ten; ?></h2>
                                            </div>
                                            <div class="col-sm-5 col-xs-12">
                                                <img class="border rounded" src="media/upload/news/<?php echo $v->photo; ?>" alt="<?php echo $v->ten; ?>">
                                            </div>
                                            <div class="col-sm-7 col-xs-12">
                                                <?php echo $v->trangchu; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="category" style="margin-top:0px">
        <h2 class="title"><span>DANH MỤC SẢN PHẨM</span></h2>
        <div class="container">
            <ul class="category_list">
                <?php foreach ($product_cate as $key => $val): ?>
                    <li>
                        <a href="product/<?= $val->url?>">
                            <img src="media/upload/sanpham/<?=$val->image1?>" alt=""><span><?=$val->loaitin?></span>
                        </a>
                    </li>
                    <?php if ($key == 5) break; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>

    <section class="product">
        <h2 class="title"><a href="/product.html"><span>SẢN PHẨM CHÍNH</span></a></h2>
        <div class="container">
            <div class="row">
                <?php foreach ($product_cate as $key => $val): ?>
                    <div class="col-lg-3 col-md-4">
                        <div class="item border border-light rounded shadow" data-toggle="modal"
                             data-target="#popup-product-<?=$val->id?>" onclick="setBaoGiaLogo('media/upload/sanpham/<?=$val->image1?>','<?=$val->loaitin?>')">
                            <div style="background-image:url('/media/upload/sanpham/<?=$val->image1 ?>')" class="pro_img"></div>
                            <h3><a href="/product/<?=$val->url ?>" title="Có lỗi xảy ra"><?=$val->loaitin?></a></h3>
                            <a href="javascript:;" class="btn btn-view-more">Nhận báo giá</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <?php foreach ($product_cate as $key => $val): ?>
            <div class="modal fade popup-product" id="popup-product-<?=$val->id?>">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="modal-header">
                            <a href="" class="logo"><img src="./media/system/images/logo.png" alt=""></a>
                            <a href="javascript:;" title="LẤY BÁO GIÁ NGAY" class="btn btn_baogia"
                               data-toggle="modal" data-target="#baogia_popup" onclick="$('#popup-product-<?=$val->id?>').modal('hide')">LẤY BÁO GIÁ NGAY</a>
                        </div>
                        <div class="modal-body">
                            <ul class="product_img">
                                <?php foreach ($val->sub as $k=>$v): ?>
                                    <li><img src="/media/upload/sanpham/<?=$v->image1?>" alt="Có lỗi xảy ra"></li>
                                <?php endforeach; ?>
                            </ul>
                            <a class="btn link_baogia" data-toggle="modal" data-target="#baogia_popup" onclick="$('#popup-product-<?=$val->id?>').modal('hide')">LẤY BÁO GIÁ NGAY</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </section>

    <section class="process">
        <h2 class="title">QUY TRÌNH SẢN XUẤT</h2>
        <ul class="process_list flex container">
            <li>
                <div class="icon">
                    <i class="icon_step1"></i>
                </div>
                <h5>Chốt đơn hàng</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step2"></i>
                </div>
                <h5>Lấy size áo, đo cắt áo</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step3"></i>
                </div>
                <h5>In, thêu</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step4"></i>
                </div>
                <h5>May áo</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step5"></i>
                </div>
                <h5>Kiểm tra chất lượng SP</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step6"></i>
                </div>
                <h5>Hoàn thiện</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step7"></i>
                </div>
                <h5>Giao hàng</h5>
            </li>
            <li>
                <div class="icon">
                    <i class="icon_step8"></i>
                </div>
                <h5>Nhận phản hồi</h5>
            </li>
        </ul>
        <div class="process_banner">
            <img src="./media/system/images/banner2.jpg" alt="">
        </div>
        <div class="process_statistical">
            Hơn 9.000 khách hàng,  gần  95.000 sản phẩm đã sản xuất trong 3 năm qua
        </div>
    </section>

    <section class="partner">
        <div class="container">
            <ul class="partner_list flex">
                <?php foreach ($partners as $val): ?>
                    <li>
                        <a href="<?=$val->target_link?>" target="_blank"><img src="media/upload/partner/<?=$val->logo?>" alt=""></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
    <section class="contract">
        <div class="container">
            <ul class="contract_list flex">
                <?php foreach ($contracts as $val): ?>
                    <li data-toggle="modal" data-target="#popup-contract-<?=$val->id?>">
                        <img src="<?=$val->images[0]?>" alt="Có lỗi xảy ra trong quá trình tải ảnh">
                        <h5><?=$val->name?></h5>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php foreach ($contracts as $val): ?>
            <div class="modal fade popup-contract" id="popup-contract-<?=$val->id?>">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="nav nav_contract flex">
                            <?php foreach ($val->images as $k => $v): ?>
                                <li>
                                    <a href="javascript:;" class="<?php if ($k == 0) echo 'active'; ?>" data-contract="<?=$v?>">Trang <?=$k+1?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="contract_content">
                            <img src="<?=$val->images[0]?>" alt="Có lỗi xảy ra trong quá trình tải ảnh">
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </section>

    <section class="partner_img">
        <div class="container">
            <div class="list_item row">
                <div class="col-lg-5">
                    <div class="item video">
                        <a rel="video" href="<?=htmlspecialchars_decode($galleries['video']->resource)?>">
                            <iframe width="100%" height="100%" src="<?=htmlspecialchars_decode($galleries['video']->resource)?>">
						</iframe>
                        </a>
						
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="item">
                        <div class="flex img-large">
                            <?php foreach ($lg_photos as $val): ?>
                                <a rel="img" href="<?=$val->resource?>"><img src="<?=$val->resource?>" alt="Có lỗi xảy ra"></a>
                            <?php endforeach; ?>
                        </div>
                        <div class="flex img-small">
                            <?php foreach ($sm_photos as $val): ?>
                                <a rel="img" href="<?=$val->resource?>"><img src="<?=$val->resource?>" alt="Có lỗi xảy ra"></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="news_ohnew">
        <div class="container">
            <div class="header flex">
                <h2 class="title">bản tin ohnew</h2>
                <a href="/tin-tuc.html" title="Xem thêm" class="btn-view-more">Xem  thêm</a>
            </div>
            <div class="row">
                <?php foreach ($ohnew_news as $val): ?>
                    <div class="col-lg-4 col-md-6">
                        <article class="post">
                            <a href="<?= $val->url?>.html" class="thumb"><img src="media/upload/news/<?=$val->photo?>" alt="Có lỗi xảy ra"></a>
                            <h3><a href="<?= $val->url?>.html"><?= $val->ten?></a></h3>
                            <div class="meta flex">
                                <time><i class="icon icon-time"></i> <?= $val->ngay?></time>
<!--                                <span class="comment"><i class="icon icon-comment"></i> 0 Bình luận</span>-->
                            </div>
                            <p><?= $val->trangchu?></p>
                        </article>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="ads">
        <div class="container">
            <div class="ads_list flex">
                <a href="<?=$ads->middle_ad_link?>" target="_blank"><img src="./media/upload/ad/<?=$ads->middle_ad_image?>" alt=""></a>
				<a href="<?=$ads->middle_ad_link2?>" target="_blank"><img src="./media/upload/ad/<?=$ads->middle_ad_image2?>" alt=""></a>
            </div>
        </div>
    </section>

    <section class="order">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="order_form">
                        <h4>HÃY LIÊN HỆ NGAY VỚI OHNEW !</h4>
                        <form autocomplete="off" method="post" id="landing-submit" class='ladi-form'>
                            <div class="form-group">
                                <input type="text" name="fullname" id="fullname" placeholder="Họ và tên:" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="email" placeholder="Email:">
                            </div>
                            <div class="form-group">
                                <input type="number" name="phone" id="phone" placeholder="Điện thoại:" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" placeholder="Yêu cầu:"></textarea>
                            </div>
                            <button type="submit" class="btn btn_submit">Gửi</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wrap_img">
                        <img src="./media/system/images/img3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<style>
.pro_img{
	height: 220px;
    width: 100%;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
}
.product .thumb{
	padding-bottom:0;
}
</style>