
<footer id="footer" role="contentinfo" class="">
          <div class="footer-container">
            <section id="footercenter" class="footer-center">
              <div class="container">
                <div class="footer-center-wrap">
                  <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 wow animated fadeInLeft" data-wow-delay="100ms">
                      <div class="block_aboutshop">
                        <?php echo $tomtat_lienhe; ?>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 wow animated fadeInUp" data-wow-delay="200ms">
                      <div class="footer_quicklink">
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="fb-like-box small--hide" data-href="<?php echo $face_lienhe; ?>" data-height="300"  style="width:100% !important;" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section id="footernav" class="footer-nav">
              <div class="container">
                <div id="social_block" class="social_block block">
                  <h4 class="title_block">Kết Nối Với Chúng Tôi</h4>
                  <div class="block_content">
                    <ul class="list-unstyled clearfix">
                      <li class="facebook"> <a target="_blank" href="<?php echo $face_lienhe; ?>" title="Shop áo thun on Facebook" class="btn-tooltip" data-original-title="Facebook"> <i class="fa fa-facebook"></i> <span>Facebook</span> </a> </li>
                    </ul>
                  </div>
                </div>
                <div id="powered" class="powered wow animated fadeInUp">
                  <p class="text-center">&copy; Copyright 2019 <strong><?php echo $tieude_lienhe; ?>.</strong></p>
                </div>
              </div>
            </section>
          </div>
        </footer>	