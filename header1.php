<?php include('ajax_loading.php'); ?>

<?php
    $sql_cate = mysql_query('select * from table_nhommuc where parentid=0 order by uutien desc');
    $product_cate = [];
    while ($cate = mysql_fetch_object($sql_cate)) {
        $row = $cate;
        $sql = mysql_query('select * from table_nhommuc where parentid = ' . $cate->id . ' and publish = 1 order by uutien');

        $row->sub = [];

        while ($sub = mysql_fetch_object($sql)) {
			
			$sql2 = mysql_query('select * from table_sanpham where (id_cap_mot=' . $sub->id . ' or id_cap_hai=' . $sub->id . ' or id_cap_ba=' . $sub->id . ') and spnoibat=1 and publish = 1 order by id desc limit 0,3');
			
			$sub->sub = [];
			while ($sp_sub = mysql_fetch_object($sql2)) {
				$sub->sub[] = $sp_sub; 
			}
            $row->sub[] = $sub;
        }
        $product_cate[] = $row;
    }

    $sql_slide = mysql_query('select * from table_slide order by id desc');
    $slide = [];

    while ($result = mysql_fetch_object($sql_slide)) {
        $slide[] = $result;
    }

    $sql_ads = mysql_query('select * from table_ad where id = 1');
    $ads = mysql_fetch_object($sql_ads);
	
	// landing page
	$sql_lp = mysql_query('select * from table_landing_setting where	is_active=1 order by id');
    $landing = [];

    while ($result = mysql_fetch_object($sql_lp)) {
        $landing[] = $result;
    }
?>

<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="navbar-top flex">
            <a class="logo" href="/"><img src="./media/system/images/logo.png" alt=""></a>
            <div class="navbar-banner">
                <a href="<?=$ads->top_ad_link?>" target="_blank">
                    <img src="./media/upload/ad/<?=$ads->top_ad_image?>" alt="Có lỗi xảy ra trong quá trình tải quảng cáo">
                </a>
            </div>
            <a href="#baogia_popup" data-toggle="modal" data-target="#baogia_popup" class="btn btn_baogia">Báo giá</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav container">
                <li><a href="/" title="Trang chủ">Trang chủ</a></li>
                <li>
                    <a href="/lien-he.html" title="Giới thiệu">Giới thiệu</a>
                    <ul class="sub-menu">
                        <li><a href="/lien-he.html" title="">Giới thiệu Ohnew</a></li>
                        <li><a href="/khach-hang-than-thiet/" title="">Khách hàng thân thiết</a></li>
                        <li><a href="/thong-tin/" title="">Thông tin</a></li>
                        <li><a href="/khuyen-mai-khung/" title="">Khuyến mãi khủng</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/product.html" title="Sản phẩm">Sản phẩm</a>
                    <div class="menu_product flex">
                        <div class="wrap_img">
                            <img id="product-img" src="media/upload/sanpham/<?=$product_cate[0]->sub[0]->image1?>" alt="Có lỗi xảy ra trong quá trình tải ảnh">
                        </div>
                        <div class="wrap_menu flex">
                            <?php $count = 0; foreach ($product_cate as $key => $val): ?>
                                <?php if ($count == 0): ?>
                                    <div class="col-menu">
                                <?php endif; ?>
                                <?php $count++; ?>
                                    <div class="box-menu">
                                        <a href="product/<?= $val->url?>">
                                            <h4><?= $val->loaitin?></h4>
                                        </a>

                                        <ul class="menu">
                                            <?php if(isset($val->sub)){ foreach ($val->sub as $k => $v): ?>
                                                <li>
                                                    <a data-toggle="tooltip" href="product/<?=$v->url?>"><?=$v->loaitin?></a>
                                                    <img src="media/upload/sanpham/<?=$v->image1?>" class="d-none">
                                                </li>
											<?php if(isset($v->sub)){ foreach ($v->sub as $kk => $vv): ?>
												<li style="padding-left:15px">
													<a data-toggle="tooltip" href="product/<?=$vv->url?>.html"><?=$vv->tensp?></a>
													<img src="media/upload/sanpham/<?=$vv->anh?>" class="d-none">
												</li>
											<?php endforeach;} ?>
                                            <?php endforeach;} ?>
                                        </ul>
                                    </div>
                                <?php if ($count == 2): ?>
                                    <?php $count = 0; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="/tin-tuc.html" title="Tin tức">Tin tức</a>
                    <ul class="sub-menu">
                        <li><a href="/tin-tuc.html" title="">Bản tin ohnew</a></li>
                        <li><a href="/thong-tin/" title="">Tư vấn kinh nghiệm</a></li>
                    </ul>
                </li>
				
                <li>
                    <a href="#" title="Khuyến mãi">Khuyến mãi <span class="hot">HOT</span></a>
                    <ul class="sub-menu">
                        <li><a href="http://ohnew.vn/landing-mu" title="Mũ lưỡi trai" target="_blank">Mũ lưỡi trai</a></li>
                        <li><a href="http://ohnew.vn/landing-aokhoac" title="Áo khoác" target="_blank">Áo khoác</a></li>
                        <li><a href="http://ohnew.vn/landing-dongphuc" title="Đồng phục" target="_blank">Đồng phục</a></li>
						<?php foreach($landing as $ld){?>
                        <li><a href="http://ohnew.vn/landing-page?id=<?= $ld->id ?>" title="<?= $ld->name ?>" target="_blank"><?= $ld->name ?></a></li>
						<?php }?>
                    </ul>
                </li>
                <li><a href="/contact" title="Liên hệ">Liên hệ</a></li>
            </ul>
        </div>
    </nav>
    <div class="banner pr">
        <div class="banner_slider">
            <?php foreach ($slide as $key => $val): ?>
                <div class="item">
                    <img src="media/upload/slide/<?=$val->photo?>" alt="Có lỗi xảy ra trong quá trình tải banner">
                </div>
            <?php endforeach; ?>
        </div>
        <div class="banner_content">
            <div class="container">
                <div class="content flex">
                    <h2>OHNEW UNIFORM</h2>
                    <p>Hơn 9.000 khách hàng đã tin tưởng và sử dụng sản phẩm <br>
                        95.000 sản phẩm đã sản xuất trong 3 năm qua
                    </p>
                    <div class="flex box-btn">
                        <a href="#baogia_popup" data-toggle="modal" data-target="#baogia_popup" class="btn btn_baogia">Báo giá</a>
                        <a href="/lien-he.html" title="Liên hệ" class="btn btn_contact">Liên hệ</a>
                    </div>
                </div>
                <div class="order_notifi flex border shadow rounded" style="right: -40rem">
                    <img src="./media/system/images/avatar.png" class="avatar" alt="Có lỗi xảy ra">
                    <div class="info">
                        <h5><span class="name">Phạm Văn Mạnh</span> <span class="phone">0339360xxx</span></h5>
                        <div class="status">Đã đặt hàng thành công</div>
                        <time><i class="icon icon-time"></i>1 phút trước</time>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="box-right">
        <ul class="list_contact">
            <li>
                <a href="tel:0913349945"><i class="icon-hotline"></i></a>
            </li>
            <li>
                <a href=""><i class="icon-message"></i></a>
            </li>
            <li>
                <a href=""><i class="icon-zalo"></i></a>
            </li>
        </ul>
    </div>-->
    <div class="modal fade popup-baogia" id="popup-baogia">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="wrap_img">
                    <img src="./media/system/images/img6.png" alt="">
                </div>
                <div class="text">
                    Nhận báo giá nhanh, tư vấn
                    thiết kế miễn phí và may mẫu
                </div>
                <form action="" method="">
                    <div class="form-group">
                        <input type="number" name="phone" placeholder="Nhập số điện thoại" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Nhập email" required>
                    </div>
                    <button type="submit" class="btn btn_submit">Tư vấn & báo giá</button>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade popup-success" id="popup-success">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <img src="./media/system/images/success.jpg" alt="">
            </div>
        </div>
    </div>

</header>
<style>
    .hot {
		font-size: 0.6vw;
		background-color: white;
		color: #f36e21;
		padding: 1px;
		margin-right: 0.3vw;
		margin-top: -3px;
		vertical-align: top;
	}
</style>
<script>
$('a[data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'bottom',
    html: true
});
</script>