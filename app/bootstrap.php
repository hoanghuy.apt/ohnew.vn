<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require 'constants.php';

require HELPER_DIR . 'helper.php';

spl_autoload_register('autoload');
function autoload($class)
{
    if (class_exists($class, false) || interface_exists($class, false)) {
        return true;
    }

    if (strpos($class, 'Lib') === 0) {
        $class = str_replace('Lib_', '', $class);
        $class = LIB_DIR . str_replace('_', DS, $class) . '.php';
        if (file_exists($class)) {
            require $class;
            return true;
        }
    } else {

    }

    return false;
}