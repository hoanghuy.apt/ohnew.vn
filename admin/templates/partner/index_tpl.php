<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

<style type="text/css">
    #img_size{
        max-height: 100px;
        max-width: 100px;
        object-fit: cover;
    }
    .chuxanh{color:#0000FF; font-size:12px;}
    .chuvang{color:#FF00FF; font-size:11px;}
</style>
<h3><a href="<?=url(null, 'edit')?>">Thêm đối tác</a></h3>

<?php $flash->display(); ?>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th style="width:1%;">ID</th>
            <th style="width:10%;">Tên đối tác</th>
            <th width="9%" style="width:4%;">Logo</th>
            <th style="width:18%;">Liên kết đích</th>
            <th width="9%" style="width:1%;">Sửa</th>
            <th width="9%" style="width:1%;">Xóa</th>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $count = count($items); $i < $count; $i++) { ?>
            <tr>
                <td style="width:1%;" align="center"><?= $items[$i]['id'] ?></td>
                <td style="width:20%;" align="center">
                    <a href="<?=url(null, 'edit', ['id' => $items[$i]['id']])?>" style="text-decoration:none;"><?= $items[$i]['name'] ?></a>
                </td>
                <td style="width:4%;" align="center"><img src="<?= _upload_partner . $items[$i]['logo'] ?>" id="img_size" title="Ảnh minh họa" alt="Chưa cập nhật hình"> </td>

                <td style="width:28%;">
                    <a href="<?= $items[$i]['target_link'] ?>" target="_blank"><?= $items[$i]['target_link'] ?></a>
                </td>

                <td style="width:1%;" align="center">
                    <a href="<?=url(null, 'edit', ['id' => $items[$i]['id']])?>">
                        <img src="media/images/edit.png"  border="0"/>
                    </a>
                </td>
                <td style="width:1%;">
                    <a href="<?=url(null, 'del', ['id' => $items[$i]['id']])?>" onClick="if (!confirm('Xác nhận xóa'))return false;">
                        <img src="media/images/delete.png" border="0" />
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script>
    $('#example1').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
</script>