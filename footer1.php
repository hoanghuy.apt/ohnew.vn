<?php
    $sql = mysql_query('select * from table_htdl order by id');
    $row = mysql_num_rows($sql);
    $sale_contacts = [];
    if ($row > 0) {
        $res = mysql_fetch_object($sql);
        $i = 0;
        while ($row = mysql_fetch_object($sql) and $i < 3) {
            $sale_contacts[] = $row;
            $i++;
        }
    }
    //url, ten,
    $sql = mysql_query('select * from table_tbmang order by uutien');

    $service = [];
    while ($res = mysql_fetch_object($sql)) {
        $service[] = $res;
    }

    $tel = '0913349945';
?>

<section class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.122355947918!2d105.75485871445463!3d21.027789693188332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134549820393c11%3A0x35a716d3be56a9c5!2zMTgzIMSQxrDhu51uZyBLMiwgQ-G6p3UgRGnhu4VuLCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sfr!2s!4v1576509548911!5m2!1sfr!2s" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</section>
<footer>

    <div class="container">
        <div class="flex footer_inner">
            <div class="col info">
                <a class="logo bg-light" href="/"><img src="./media/system/images/logo.png" alt=""></a>
                <div class="info_block">
                    <h4>CÔNG TY CP OHNEW</h4>
                    <p>Mã số thuế: 0102342884</p>
                </div>
                <div class="info_block">
                    <h4>VĂN PHÒNG CHÍNH</h4>
                    <p>183 - 185 Đường K2, Cầu Diễn <br> Nam Từ Liêm, Hà Nội</p>
                </div>
            </div>
            <div class="col facebook">
                <iframe src="https://www.facebook.com/plugins/page.php?href=<?=urlencode($face_lienhe)?>&tabs=timeline&width=290&height=320&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                        width="290"
                        height="320"
                        style="border:none;overflow:hidden"
                        scrolling="no" frameborder="0" allowTransparency="true"
                        allow="encrypted-media"></iframe>
            </div>
            <div class="col service">
                <h4>DỊCH VỤ KHÁCH HÀNG</h4>
                <ul class="service_list">
                    <?php foreach ($service as $val): ?>
                        <li><a href="dich-vu/<?=$val->url?>" title=""><?=$val->ten?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col advisory">
                <h4>TƯ VẤN VÀ BÁO GIÁ NGAY!</h4>
                <div class="phone flex">
                    <i class="icon icon-phone"></i>
                    <a href="tel:<?=$hotline_lienhe?>"><?=$hotline_lienhe?></a>
                </div>
                <div class="email flex">
                    <i class="icon icon-email"></i>
                    <div class="email_list">
                        <a href="mailto:<?=$email_lienhe?>"><?=$email_lienhe?></a>
                        <a href="mailto:dongphucohnew@gmail.com">dongphucohnew@gmail.com</a>
                    </div>
                </div>
                <div class="img_list flex">
                    <?php foreach ($sale_contacts as $val): ?>
                        <a href="tel:<?=$val->sdt?>">
                            <img src="media/upload/news/<?=$val->hinhanh?>" alt="<?=$val->ten?>" title="<?=$val->sdt?> - <?=$val->ten?>">
                        </a>
                    <?php endforeach; ?>
                </div>
                <a href="#baogia_popup" data-toggle="modal" data-target="#baogia_popup" class="open-popup-link btn btn_baogia">LẤY BÁO GIÁ NGAY>></a>
            </div>
        </div>
    </div>
</footer>

<a href="javascript:" id="return-to-top">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px"
         y="0px" width="50%" height="85%" viewBox="0 0 444.819 444.819"
         style="enable-background:new 0 0 444.819 444.819;" xml:space="preserve" class=""><g>
            <g>
                <path d="M433.968,278.657L248.387,92.79c-7.419-7.044-16.08-10.566-25.977-10.566c-10.088,0-18.652,3.521-25.697,10.566   L10.848,278.657C3.615,285.887,0,294.549,0,304.637c0,10.28,3.619,18.843,10.848,25.693l21.411,21.413   c6.854,7.23,15.42,10.852,25.697,10.852c10.278,0,18.842-3.621,25.697-10.852L222.41,213.271L361.168,351.74   c6.848,7.228,15.413,10.852,25.7,10.852c10.082,0,18.747-3.624,25.975-10.852l21.409-21.412   c7.043-7.043,10.567-15.608,10.567-25.693C444.819,294.545,441.205,285.884,433.968,278.657z"
                      data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF"/>
            </g>
        </g> </svg>
</a>

<div class="modal fade" id="baogia_popup" tabindex="-1" role="dialog" aria-labelledby="baogia_popup" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 d-none d-sm-block">
                        <img src="media/upload/slide/<?php echo $hinh_logo_5; ?>" id="baogia-logo" alt="<?php echo $title_s; ?>">
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <h3 class="mb-3">HÃY LIÊN HỆ NGAY VỚI OHNEW THEO 2 CÁCH DƯỚI ĐÂY :</h3>
                        <p class="mb-2"><strong>Cách 1:</strong> Gọi ngay đến Hotline hỗ trợ MIỄN PHÍ <?php echo $hotline_lienhe; ?></p>
                        <a class="callbackfree-button" href="tel:<?php echo $hotline_lienhe; ?>">Gọi ngay !</a> <br/>
                        <hr/>
                        <p class="mb-2"><strong>Cách 2:</strong> Anh/chị hãy điền Tên và SĐT bên dưới, Ohnew sẽ gọi lại tư vấn đến anh/chị ngay sau ít phút !</p>
                        <div id="getfly-optin-form-iframe-1541746999055">
                            <form id="baogia-submit" class="getfly-form">
                                <div class="getfly-row">
                                    <label class="getfly-label getfly-label-c">Tên công ty / Tên khách hàng<span class="getfly-span getfly-span-c">*</span></label>
                                    <input type="input" required id="baogia-fullname" class="getfly-input " name="account_name" placeholder="Tên *">
                                </div>
                                <div class="getfly-row">
                                    <label class="getfly-label getfly-label-c">Điện thoại<span class="getfly-span getfly-span-c">*</span></label>
                                    <input type="text" required name="account_phone" id="baogia-phone" class="getfly-input " placeholder="Số điện thoại *">
                                </div>
                                <div class="getfly-mt10 getfly-btn">
									<input type="hidden" id="baogia-tensp"/>
                                    <button type="submit" id="btnkhachhangBaogia" class="getfly-button getfly-button-bg ">Gọi Lại Cho Tôi Ngay!</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="hoptacdaily_popup" class="white-popup mfp-hide d-none">
  <div>
    <h4>NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ</h4>
    <div class="form-group">
      <input required type="text" id="txtTenDaily"  class="form-control" name="name-guest"  placeholder="Tên" value="">
    </div>
    <span class="error-message hidden">Vui lòng nhập số điện thoại hợp lệ gồm 10 hoặc 11 chữ số!</span>
    <div class="form-group">
      <input required type="text" id="txtDienthoaiDaily"  class="form-control" name="num-phone" placeholder="Số điện thoại" value="">
    </div>
    <div class="form-group">
      <input required type="email" id="txtEmailDaily"  class="form-control" name="mail" placeholder="Nhập địa chỉ email" value="">
    </div>
    <a id="btnDaily" class="daily-send btn orange_btn">Gửi !</a> </div>
</div>
<div>
	<div class="modal fade" id="camon_popup" tabindex="-1" role="dialog" aria-labelledby="camon_popup" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="border-0 modal-content">
				<div class="modal-body p-0">
<!--					<p><a id="more" href="#more">Đăng ký thành công</a></p>
					<br/>
					<p>Cảm ơn anh/chị đã để lại thông tin! Ohnew sẽ liên hệ để hỗ trợ anh/chị ngay ạ!</p>-->
                    <img src="./media/system/images/cam_on.jpg" alt="Đăng ký thành công" data-dismiss="modal" aria-label="Close">
				</div>
			</div>
		</div>
	</div>

	<!-- Link to open the modal -->
	<a href="#camon_popup" data-toggle="modal" data-target="#camon_popup" id="open-modal" style="display:none"></a>
	</p>
</div>
<a href="tel:<?php echo $tel; ?>" class="call-phone">
	<?php echo ' '.$tel; ?>
</a>
<a href="tel:<?php echo $tel; ?>" class="call-phone-mobile">
	<img src="/images/phone2.gif" width="120px" />
</a>

	<style>
		@media (min-width:768px) {
			.call-phone{
				display: block;
				position: fixed;
				bottom: 15px;
				left: 0;
				background-color: #f6571e;
				height: 50px;
				line-height: 50px;
				color: white !important;
				width: 136px;
				border-radius: 0 10px 10px 0;
				text-align: right;
				padding-right: 10px;
				font-size: 20px;
			}
			.call-phone-mobile{
				display:none;
			}
		}
		
		@media (max-width:768px) {
			.call-phone{
				display:none;
			}
			
			.call-phone-mobile{
				position: fixed;
				bottom: -10px;
				left: -11px;
				display:block;
			}
		}
	</style>
	
<?php include('ajax_loading.php'); ?>
<!--<script src='919/1000032919/1000192486/jquery.magnific-popup.mindcde.js?v=468' type='text/javascript'></script>
<script src='919/1000032919/1000192486/jquery.fancyboxdcde.js?v=468' type='text/javascript'></script>
<script src='0/0/global/design/plugins/fancybox/jquery.mousewheel-3.0.6.js' type='text/javascript'></script>-->

<script src="./media/scripts/jquery-3.1.1.min.js"></script>
<script src="./media/scripts/bootstrap.min.js"></script>
<script src="./media/scripts/jquery.fancybox.pack.js"></script>
<script src="./media/scripts/slick.js"></script>
<script src="./media/scripts/page_all.js"></script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml : true,
            version : 'v5.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="440961062614280"
     theme_color="#fa3c4c"
     logged_in_greeting="Chào bạn! chúng tôi có thể giúp gì được bạn ạ."
     logged_out_greeting="Chào bạn! chúng tôi có thể giúp gì được bạn ạ.">
</div>

<script>
	jQuery(document).ready(function() {
		jQuery('#landing-submit').submit(function(e) {
			e.preventDefault();
			var txtFullname = jQuery('#fullname');
			var txtPhone = jQuery('#phone');
			var txtEmail = jQuery('#email');
			var txtMessage = jQuery('#message');
			var url = window.location.href ;
			var ajax_loading = jQuery('#ajax_loading');

			if (txtFullname.val() == "") {
				alert("Bạn chưa nhập tên");
				txtFullname.focus();
			} else if (txtPhone.val() == "") {
				alert("Bạn chưa nhập số điện thoại");
				txtPhone.focus();
			} else {

				var UrlToPass = {
					"action" : "gui_baogia_new",
					"name" : txtFullname.val(),
					"phone" : txtPhone.val(),
					"message" : txtMessage.val(),
					"email" : txtEmail.val(),
					"from_web" : 1,
					"url" : url,
				}
				//console.log(UrlToPass);
				ajax_loading.removeClass('hidden');
				jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
					type: 'POST',
					data: UrlToPass,
					url: 'lienhe_xuly.php',
					success: function(responseText) { // Get the result and asign to each cases
						ajax_loading.addClass('hidden');
						if (responseText == 1) {
							$('#open-modal').click();
						} else if (responseText == 4) {
							alert("có lỗi trong quá trình liên hệ báo giá, hãy thử lại");
						}
					}
				});

			}

		});
		jQuery('#baogia-submit').submit(function(e) {
			e.preventDefault();
			var txtFullname = jQuery('#baogia-fullname');
			var txtPhone = jQuery('#baogia-phone');
			var txtEmail = "";
			var txtMessage = "Yêu cầu liên hệ xem sản phẩm : <b>" + $('#baogia-tensp').val() + "</b> Trên website";
			var url = window.location.href ;
			var ajax_loading = jQuery('#ajax_loading');

			if (txtFullname.val() == "") {
				alert("Bạn chưa nhập tên");
				txtFullname.focus();
			} else if (txtPhone.val() == "") {
				alert("Bạn chưa nhập số điện thoại");
				txtPhone.focus();
			} else {

				var UrlToPass = {
					"action" : "gui_baogia_new",
					"name" : txtFullname.val(),
					"phone" : txtPhone.val(),
					"message" : txtMessage,
					"email" : "",
					"from_web" : 1,
					"url" : url,
				}
				//console.log(UrlToPass);
				ajax_loading.removeClass('hidden');
				jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
					type: 'POST',
					data: UrlToPass,
					url: 'lienhe_xuly.php',
					success: function(responseText) { // Get the result and asign to each cases
						ajax_loading.addClass('hidden');
						if (responseText == 1) {
							$('#open-modal').click();
						} else if (responseText == 4) {
							alert("có lỗi trong quá trình liên hệ báo giá, hãy thử lại");
						}
					}
				});

			}

		});
	});
	function setBaoGiaLogo(url,tensp){
		$('#baogia-logo').attr('src',url);
		$('#baogia-tensp').val(tensp);
	}
</script>
<script>
$(document).ready(function(){
	if($(window).width() > 960){
		$(window).scroll(function(){
			if($(this).scrollTop() > $('header').height()){
				$( "#menuheaderfix" ).addClass( "topmainmenu" );
				$( "#alo-fixed" ).addClass( "show" );

			}else{
				$( "#menuheaderfix" ).removeClass( "topmainmenu" );
				$( "#alo-fixed" ).removeClass( "show" );
			}})
	}
	var owl = $(".customer-slider"); 
	owl.owlCarousel({
		itemsCustom : [
			[0, 1],
			[480, 1],
			[600, 1]
		],
		lazyLoad: true,
		pagination:	true
	});

<?php 
		$sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and duoi=1 order by uutien");
		if(mysql_num_rows($sql_1)>0){
			$res_1 = mysql_fetch_object($sql_1);
			do{
?>
	var feedback_index_<?php echo $res_1->id; ?> = $('.feedback_index_<?php echo $res_1->id; ?>');
	feedback_index_<?php echo $res_1->id; ?>.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 2],
			[768, 3]
		],
		lazyLoad: true,
		navigation:true,
		navigationText: [	"",	""],
		pagination:	true
	});
<?php }while($res_1 = mysql_fetch_object($sql_1)); } ?> 

	var owl_contactus_block = $('.contactus_block');
	owl_contactus_block.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 1],
			[768, 1]
		],
		autoPlay: 3000,
		lazyLoad: true,
		autoHeight : true,
		navigation:true,
		navigationText: [	"",	""],
		pagination:	true
	});

	var owl_img_feedback = $('.list_img_feedback');
	owl_img_feedback.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 1],
			[768, 1]
		],
		autoPlay: 3000,
		stopOnHover: false,
		lazyLoad: true,
		navigation:true,
		navigationText: ["",""],
		pagination: true
	});


	/*
$(".slides").owlCarousel({
	navigation : true, 
	video:true,
	merge:true,
	slideSpeed : 300,
	paginationSpeed : 400,
	singleItem: true,
	pagination: false,
	rewindSpeed: 500
});
*/

	$(".fancybox").fancybox({
		padding:0,
		margin:15,
		scrolling : 'yes'
	});
	$('.open-popup-link, .mb-open-popup-link').magnificPopup({
		type:'inline',
		midClick: true
	});

	$('#alo-fixed .open-popup-link').hover(function(){
		$(this).trigger('click');
	});



});


$(document).ready(function(){
	$('.articlelistOther h3, .articlelistCate h3').append('<div class="openc">+</div>');
	$('.articlelistOther ul li a.current, .articlelistCate ul li a.current').parents().eq(1).css("display","block");
	$('.articlelistOther ul li a.current, .articlelistCate ul li a.current').parents().eq(2).find(".openc").css("display","none");
	$('.articlelistOther h3,.articlelistCate h3.articlelistOther h3 .openc, .articlelistCate h3 .openc').click(function(){  
		var text = $(this).text()
		if(text == '+'){
			$(this).parents().eq(1).find('ul').slideToggle(500);
			$(this).text('-');
		}else{
			$(this).parents().eq(1).find('ul').slideToggle(500);
			$(this).text('+');  
		}
	});
});

</script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<a href="tel:<?php echo $hotline_lienhe; ?>" class="visible-xs phone-ring">
<div class="laziweb-alo-phone">
  <div class="animated infinite zoomIn laziweb-alo-ph-circle"></div>
  <div class="animated infinite pulse laziweb-alo-ph-circle-fill"></div>
  <div class="animated infinite tada laziweb-alo-ph-img-circle"></div>
</div>
</a>
<div class="hidden-xs quickcontact phone-ring"> <a href='tel:<?php echo $hotline_lienhe; ?>'>
  <div class="laziweb-alo-phone">
    <div class="animated infinite zoomIn laziweb-alo-ph-circle"></div>
    <div class="animated infinite pulse laziweb-alo-ph-circle-fill"></div>
    <div class="animated infinite tada laziweb-alo-ph-img-circle"></div>
  </div>
 <!-- 
  <div class="hot-line-wrap">
    <strong class="hidden-xs"><?php echo $hotline_lienhe; ?></strong>
  </div>
  -->
  </a> </div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#btnDaily').click(function(e){
			e.preventDefault();
			
			var txtFullname = jQuery('#txtTenDaily');
			var txtEmailInput = jQuery('#txtEmailDaily');
			var txtTitle = jQuery('#txtDienthoaiDaily');
			var ajax_loading = jQuery('#ajax_loading');
			
			if(txtFullname.val()==""){alert("Bạn chưa nhập tên"); txtFullname.focus();}
						else if(txtEmailInput.val()==""){alert("Bạn chưa nhập email"); txtEmailInput.focus();}
						
			else{
			
					var UrlToPass = 'action=gui_daily&lienhe_hoten='+txtFullname.val()+'&lienhe_email='+txtEmailInput.val()+'&lienhe_tieude='+txtTitle.val();
							//console.log(UrlToPass);
							ajax_loading.removeClass('hidden');
							jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
								type : 'POST',
								data : UrlToPass,
								url  : 'lienhe_xuly.php',
								success: function(responseText){ // Get the result and asign to each cases
									//alert(responseText);
									if(responseText == 2){
										ajax_loading.addClass('hidden');
										alert("Email không hợp lệ, vui lòng nhập lại");
										txtEmailInput.focus();
									}
									else if(responseText == 1){ ajax_loading.addClass('hidden');alert("Liên hệ NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ thành công, cảm ơn bạn!");
										window.location = "index.html";}
									else if(responseText == 4){ ajax_loading.addClass('hidden');alert("có lỗi trong quá trình liên hệ NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ, hãy thử lại");}
								}
							});
							
						}				
			
		});
	});
</script>
<style type="text/css">
	.content img{height:inherit!important;}
</style>
<noscript id="deferred-styles">

</noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
          window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/5d9ea1c0f82523213dc6889d/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();
</script>
<!--End of Tawk.to Script-->	