CREATE TABLE `table_ad` ( `id` INT(8) NOT NULL AUTO_INCREMENT , `top_ad_link` VARCHAR(500) NULL , `middle_ad_link` VARCHAR(500) NULL , `top_ad_image` VARCHAR(500) NULL , `middle_ad_image` VARCHAR(500) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `table_ad` 
ADD COLUMN `middle_ad_image2` VARCHAR(500) NULL COMMENT '' AFTER `middle_ad_image`
ADD COLUMN `middle_ad_link2` VARCHAR(500) NULL COMMENT '' AFTER `middle_ad_image`;

ALTER TABLE `table_nhommuc` 
ADD COLUMN `mota2` TEXT NULL COMMENT '' AFTER `mota`;
