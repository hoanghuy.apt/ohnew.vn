<h3><a href="index.php?com=hang&act=add">Thêm mới</a></h3>

	<!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- DataTables -->
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">


<table id="example1" class="table table-bordered table-striped">
	<thead>
    <tr>
        <th style="width:1%;">ID</th>
        <th style="width:16%;">Tên Thương hiệu</th>
		<th style="width:16%;">Hình</th>
		<th style="width:1%;">Thuộc loại</th>
		<th style="width:1%;">hot</th>
		<th style="width:1%;">new</th>
		<th style="width:1%;">sale</th>
        <th style="width:1%;">Sửa</th>
        <th style="width:1%;">Xóa</th>
    </tr>
		</thead>
	<tbody>	
    <?php for ($i = 0, $count = count($items); $i < $count; $i++) { ?>
        <tr>
            <td style="width:1%;"><?= $items[$i]['id_hang'] ?></td>
            <td style="width:6%;"><?= $items[$i]['tenhang'] ?></td>
			 <td style="width:6%;"><img src="<?= _upload_hang . $items[$i]['img'] ?>" alt="NO PHOTO"  width="100"/></td>
			<td style="width:6%;"><?php if($items[$i]['trongnuoc']==1){echo 'Nhập khẩu';}else{echo 'TRong nước';} ?></td>
			 <td style="width:1%;">
				<?php
				if(@$items[$i]['hot']==1)
				{
					?>
					<img src="media/images/active_1.png"  border="0"/>
				<?php
				}
				else
				{
					?>
				   <img src="media/images/active_0.png" border="0" />
				<?php
				} ?>        
				</td>
			<td style="width:1%;">
				<?php
				if(@$items[$i]['new']==1)
				{
					?>
					<img src="media/images/active_1.png"  border="0"/>
				<?php
				}
				else
				{
					?>
				   <img src="media/images/active_0.png" border="0" />
				<?php
				} ?>        
				</td>	
			<td style="width:1%;">
				<?php
				if(@$items[$i]['sale']==1)
				{
					?>
					<img src="media/images/active_1.png"  border="0"/>
				<?php
				}
				else
				{
					?>
				   <img src="media/images/active_0.png" border="0" />
				<?php
				} ?>        
				</td>	
            <td style="width:1%;"><a href="index.php?com=hang&act=edit&id=<?= $items[$i]['id_hang'] ?>"><img src="media/images/edit.png"  border="0"/></a></td>
            <td style="width:1%;"><a href="index.php?com=hang&act=delete&id=<?= $items[$i]['id_hang'] ?>" onClick="if (!confirm('Xác nhận xóa'))
                        return false;"><img src="media/images/delete.png" border="0" /></a></td>
        </tr>
<?php } ?>
	</tbody>
</table>

	<script>
      $('#example1').DataTable( {
        	"order": [[ 0, "desc" ]]
   		 } );
    </script>