<!doctype html>
<html class="no-js " lang="vi">
<head>
<?php include('head.php'); ?>
<?php include('title.php'); ?>
</head>
<body check="640/1000004640/1000126002/logo_checkout.png?v=1185" id="shop-ao-thun" class="template-index Default header-default layout-default" >
<div id="page" class="flypanels-container preload">
  <?php include('header.php'); ?>
  
  <div class="flypanels-main">
    <div class="flypanels-content">
      <section id="page_content" class="">
        <?php include('header_top.php'); ?>
        <?php include('content_index.php'); ?>
        <?php include('footer.php'); ?>
        <script src='640/1000004640/1000126002/timbera900.js?v=1185' type='text/javascript'></script>
        <script>
	
	
</script>
        <script src='640/1000004640/1000126002/handlebars.mina900.js?v=1185' type='text/javascript'></script>
        <script id="CartTemplate" type="text/template">
    
        <form action="/cart" method="post" novalidate>
            <div class="ajaxcart__inner">
                {{#items}}
                <div class="ajaxcart__product">
                    <div class="ajaxcart__row" data-id="{{id}}">
                        <div class="grid">
                            <div class="grid__item large--two-thirds">
                                <div class="grid">
                                    <div class="grid__item one-quarter">
                                        <a href="{{url}}" class="ajaxcart__product-image"><img src="{{img}}" alt=""></a>
                                    </div>
                                    <div class="grid__item three-quarters">
                                        <a href="{{url}}" class="h4">{{name}}</a>
                                        <p>{{variation}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--one-third">
                                <div class="grid">
                                    <div class="grid__item one-third">
                                        <div class="ajaxcart__qty">
                                            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
                                            <input type="text" class="ajaxcart__qty-num" value="{{itemQty}}" min="0" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*">
                                            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
                                        </div>
                                    </div>
                                    <div class="grid__item one-third text-center">
                                        <p>{{price}}</p>
                                    </div>
                                    <div class="grid__item one-third text-right">
                                        <p>
                                            <small><a href="/cart/change?id={{id}}&amp;quantity=0" class="ajaxcart__remove" data-id="{{id}}">Xóa</a></small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{/items}}
            </div>
            <div class="ajaxcart__row text-right medium-down--text-center">
                <span class="h3">Tổng {{totalPrice}}</span>
                <input type="submit" class="{{btnClass}}" name="checkout" value="Thanh Toán">
            </div>
        </form>
    
</script>
        <script id="DrawerTemplate" type="text/template">
    
        <div id="AjaxifyDrawer" class="ajaxcart ajaxcart--drawer">
            <div id="AjaxifyCart" class="container {{wrapperClass}}"></div>
        </div>
    
</script>
        <script id="ModalTemplate" type="text/template">
    
        <div id="AjaxifyModal" class="ajaxcart ajaxcart--modal">
            <div id="AjaxifyCart" class=""></div>
        </div>
    
</script>
        <script id="AjaxifyQty" type="text/template">
    
        <div class="ajaxcart__qty">
            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
            <input type="text" class="ajaxcart__qty-num" value="{{itemQty}}" min="0" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*">
            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
        </div>
    
</script>
        <script id="JsQty" type="text/template">
    
        <div class="js-qty">
            <input type="text" class=" js-qty__num" value="{{itemQty}}" min="1" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*" name="{{inputName}}" id="{{inputId}}">
            <button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
            <button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
        </div>
    
</script>
        <script src='640/1000004640/1000126002/ajaxifya900.js?v=1185' type='text/javascript'></script>
        <script>
		jQuery(function($) {
			ajaxifyHaravan.init({
				method: 'modal',
				wrapperClass: 'wrapper',
				formSelector: '.form-ajaxtocart',
				addToCartSelector: '.ajax_addtocart',
				cartCountSelector: '#CartCount',
				cartCostSelector: '#CartCost',
				toggleCartButton: '#CartToggle',
				useCartTemplate: true,
				btnClass: 'btn',
				moneyFormat: "{{amount}}₫",
				disableAjaxCart: false,
				enableQtySelectors: true,
				prependDrawerTo: 'body'
			});
		});
	</script>
        <script src='640/1000004640/1000126002/fastclick.mina900.js?v=1185' type='text/javascript'></script>
        <script src='640/1000004640/1000126002/owl.carousel.mina900.js?v=1185' type='text/javascript'></script>
        <script src='640/1000004640/1000126002/jquery.mmenu.min.alla900.js?v=1185' type='text/javascript'></script>
        <script>
	jQuery(function() {
		jQuery('.swatch :radio').change(function() {
	    	var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
	    	var optionValue = jQuery(this).val();
	    	jQuery(this)
  			.closest('form')
      		.find('.single-option-selector')
      		.eq(optionIndex)
      		.val(optionValue)
      		.trigger('change');
	  	}); 
	});
</script>
        <script>
	!function ($) {
		$(function(){
			$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { 
				e.stopPropagation(); 
            });
          	$(document.body).on('click', '[data-toggle="dropdown"]' ,function(){
                if(!$(this).parent().hasClass('open') && this.href && this.href != '#'){
                    window.location.href = this.href;
                }
            });
		})
	}(window.jQuery)
</script>
        <script>
    var Haravan = Haravan || {};
    Haravan.optionsMap = {};
    Haravan.updateOptionsInSelector = function(selectorIndex) {
        switch (selectorIndex) {
            case 0:
                var key = 'root';
                var selector = jQuery('.single-option-selector:eq(0)');
                break;
            case 1:
                var key = jQuery('.single-option-selector:eq(0)').val();
                var selector = jQuery('.single-option-selector:eq(1)');
                break;
            case 2:
                var key = jQuery('.single-option-selector:eq(0)').val();  
                key += ' / ' + jQuery('.single-option-selector:eq(1)').val();
                var selector = jQuery('.single-option-selector:eq(2)');
        }
        var initialValue = selector.val();
        selector.empty();    
        var availableOptions = Haravan.optionsMap[key];
        for (var i=0; i<availableOptions.length; i++) {
            var option = availableOptions[i];
            var newOption = jQuery('<option></option>').val(option).html(option);
            selector.append(newOption);
        }
        jQuery('.swatch[data-option-index="' + selectorIndex + '"] .swatch-element').each(function() {
            if (jQuery.inArray($(this).attr('data-value'), availableOptions) !== -1) {
                $(this).removeClass('soldout').show().find(':radio').removeAttr('disabled','disabled').removeAttr('checked');
            }
            else {
                $(this).addClass('soldout').hide().find(':radio').removeAttr('checked').attr('disabled','disabled');
            }
        });
        if (jQuery.inArray(initialValue, availableOptions) !== -1) {
            selector.val(initialValue);
        }
        selector.trigger('change');  
    };
    Haravan.linkOptionSelectors = function(product) {
        // Building our mapping object.
        for (var i=0; i<product.variants.length; i++) {
            var variant = product.variants[i];
            if (variant.available) {
                // Gathering values for the 1st drop-down.
                Haravan.optionsMap['root'] = Haravan.optionsMap['root'] || [];
                Haravan.optionsMap['root'].push(variant.option1);
                Haravan.optionsMap['root'] = Haravan.uniq(Haravan.optionsMap['root']);
                // Gathering values for the 2nd drop-down.
                if (product.options.length > 1) {
                    var key = variant.option1;
                    Haravan.optionsMap[key] = Haravan.optionsMap[key] || [];
                    Haravan.optionsMap[key].push(variant.option2);
                    Haravan.optionsMap[key] = Haravan.uniq(Haravan.optionsMap[key]);
                }
                // Gathering values for the 3rd drop-down.
                if (product.options.length === 3) {
                    var key = variant.option1 + ' / ' + variant.option2;
                    Haravan.optionsMap[key] = Haravan.optionsMap[key] || [];
                    Haravan.optionsMap[key].push(variant.option3);
                    Haravan.optionsMap[key] = Haravan.uniq(Haravan.optionsMap[key]);
                }
            }
        }
        // Update options right away.
        Haravan.updateOptionsInSelector(0);
        if (product.options.length > 1) Haravan.updateOptionsInSelector(1);
        if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
        // When there is an update in the first dropdown.
        jQuery(".single-option-selector:eq(0)").change(function() {
            Haravan.updateOptionsInSelector(1);
            if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
            return true;
        });
        // When there is an update in the second dropdown.
        jQuery(".single-option-selector:eq(1)").change(function() {
            if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
            return true;
        });
    };
</script>
      </section>
      <!-- <p id="back-top">
						<a class="btn btn-outline" href="#top" title="Scroll To Top">Scroll To Top</a>
					</p>-->
    </div>
  </div>
</div>
<?php include('js_footer.php'); ?>	
</body>
</html>
