<?php	if(!defined('_source')) die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch($act){
    case "man":
        get_items();
        $template = "tuyendung/items";
        break;
    case "add":
        $template = "tuyendung/item_add";
        break;
    case "edit":
        get_item();
        $template = "tuyendung/item_add";
        break;
    case "save":
        save_item();
        break;
    case "delete":
        delete_item();
        break;

    default:
        $template = "index";
}


function fns_Rand_digit($min,$max,$num)
{
    $result='';
    for($i=0;$i<$num;$i++){
        $result.=rand($min,$max);
    }
    return $result;
}

/* Hien noi dung toan bo bang */
function get_items(){
    global $d, $items, $paging;

    $sql = "select * from #_tuyendung";
    $d->query($sql);
    $items = $d->result_array();

    $curPage = isset($_GET['curPage']) ? $_GET['curPage'] : 1;
    $url="index.php?com=tuyendung&act=man";
    $maxR=10;
    $maxP=4;
    $paging=paging($items, $url, $curPage, $maxR, $maxP);
    $items=$paging['source'];
}

/* doc tung muc trong item */
function get_item(){
    global $d, $item_mot;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : "";
    if(!$id)
        transfer("Không nhận được dữ liệu", "index.php?com=tuyendung&act=man");

    $sql = "select * from #_tuyendung where id='".$id."'";
    $d->query($sql);
    if($d->num_rows()==0) transfer("Dữ liệu không có thực", "index.php?com=tuyendung&act=man");
    $item_mot = $d->fetch_array();
}

/* Cap nhat va them vao bang */
function save_item(){

    global $d;
    $file_name=fns_Rand_digit(0,9,8);
    if(empty($_POST)) transfer("Không nhận được dữ liệu", "index.php?com=tuyendung&act=man");
    $id = isset($_POST['id']) ? themdau($_POST['id']) : "";
    if($id){
        $id =  themdau($_POST['id']);
		
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
            $d->setTable('htbh');
            $d->setWhere('id', $id);
            $d->select();
            if($d->num_rows()>0){
                $row = $d->fetch_array();
                delete_file(_upload_gioithieu.$row['hinhanh']);
            }
        }
		
		$file_name=fns_Rand_digit(0,9,8);
		if($hinhanh1 = upload_image("file1", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh1'] = $hinhanh1;
            $d->setTable('htbh');
            $d->setWhere('id', $id);
            $d->select();
            if($d->num_rows()>0){
                $row = $d->fetch_array();
                delete_file(_upload_gioithieu.$row['hinhanh1']);
            }
        }
		
		$data['tieude'] = $_POST['tieude'];
		
		$data['tomtat1'] = $_POST['tomtat1'];
		$data['tomtat2'] = $_POST['tomtat2'];
		$data['tomtat3'] = $_POST['tomtat3'];
		$data['tomtat4'] = $_POST['tomtat4'];
       
	    $noidung = str_replace('=""','',$_POST['noidung']);
        $noidung = str_replace("'",'"',$noidung);
		$noidung = str_replace('""','"', $noidung);
        $data['noidung'] = $noidung;
		
		$bo = str_replace('+','',$_POST['tieude']);
		$bo = str_replace('~','',$bo);
		$bo = str_replace('%','',$bo);
		$bo = str_replace('&','',$bo);
		$bo = str_replace('/','',$bo);
		$bo = str_replace('?','',$bo);
		$bo = str_replace('---','-',$bo);
	  	$data['url'] = vn2latin(str_replace(',','',$bo),true);
		
		      
        $d->setTable('tuyendung');
        $d->setWhere('id', $id);
        if($d->update($data))
            redirect("index.php?com=tuyendung&act=man");
        else
            transfer("Cập nhật dữ liệu bị lỗi", "index.php?com=tuyendung&act=man");
    }else{
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
        }
		
		$file_name=fns_Rand_digit(0,9,8);
		if($hinhanh1 = upload_image("file1", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh1'] = $hinhanh1;
        }
	
		$data['tieude'] = $_POST['tieude'];
		$data['tomtat1'] = $_POST['tomtat1'];
		$data['tomtat2'] = $_POST['tomtat2'];
		$data['tomtat3'] = $_POST['tomtat3'];
		$data['tomtat4'] = $_POST['tomtat4'];
		
		$noidung = str_replace('=""','',$_POST['noidung']);
        $noidung = str_replace("'",'"',$noidung);
		$noidung = str_replace('""','"', $noidung);
        $data['noidung'] = $noidung;
		
		$bo = str_replace('+','',$_POST['tieude']);
		$bo = str_replace('~','',$bo);
		$bo = str_replace('%','',$bo);
		$bo = str_replace('&','',$bo);
		$bo = str_replace('/','',$bo);
		$bo = str_replace('?','',$bo);
		$bo = str_replace('---','-',$bo);
	  	$data['url'] = vn2latin(str_replace(',','',$bo),true);
		
        $d->setTable('tuyendung');
        if($d->insert($data))
            redirect("index.php?com=tuyendung&act=man");
        else
            transfer("Lưu dữ liệu bị lỗi", "index.php?com=tuyendung&act=man");
    }

}
function delete_item(){
    global $d;

    if(isset($_GET['id'])){
        $id =  themdau($_GET['id']);
		$d->reset();
		$sql = "select * from #_tuyendung where id='" . $id . "'";
		
        $d->query($sql);
        if ($d->num_rows() > 0) {
            while ($row = $d->fetch_array()) {
                delete_file(_upload_gioithieu . $row['hinhanh']);
				delete_file(_upload_gioithieu . $row['hinhanh1']);
            }
            $sql = "delete from #_tuyendung where id='".$id."'";
            $d->query($sql);
        }	
			
        // xoa item
        
        if($d->query($sql))
            header("Location:index.php?com=tuyendung&act=man");
        else
            transfer("Xóa dữ liệu bị lỗi", "index.php?com=tuyendung&act=man");
    }else transfer("Không nhận được dữ liệu", "index.php?com=tuyendung&act=man");
}
#--------------------------------------------------------------------------------------------- photo
?>