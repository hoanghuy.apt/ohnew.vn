<?php

if (!defined('_source'))
    die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "add":
        $template = "landingpage/add";
        break;
    case "do_add":
		create();
        break;
    case "edit":
		get_item();
        $template = "landingpage/edit";
        break;
    case "list":
		get_list();
        $template = "landingpage/list";
        break;
    case "save":
		// dump($_POST);
        save_item();
        break;
    default:
        $template = "index";
}
function get_list() {
    global $d,$items;
	$sql2 = "select * from table_landing_setting order by id";
    $d->query($sql2);
	if ($d->num_rows() == 0){
        transfer("Dữ liệu không có thực", "index.php");
	}
	$items = $d->result_array();
	// dump($items);
}

function create(){
	global $d;
    if (empty($_POST))
        transfer("Không nhận được dữ liệu", "index.php?com=bh&act=man");
	$sql = "select max(id) as id from table_landing_setting limit 1";
    
    $id = $d->query($sql); 
	$result = mysql_query($sql);
	$value = mysql_fetch_object($result);
	$id = $value->id;
    if ($id) {//cap nhat
		$id = $id + 1;
        $data['id'] = $id;
        $data['name'] = $_POST['name'];
        $data['count_down_sec'] = $_POST['count_down_sec'];
		$content = str_replace('=""','',$_POST['content']);
		$content = str_replace("'",'"',$content);
		$content = str_replace('""','"', $content);
        $data['content'] = $content;
		
		$feedback_content = str_replace('=""','',$_POST['feedback_content']);
		$feedback_content = str_replace("'",'"',$feedback_content);
		$feedback_content = str_replace('""','"', $feedback_content);
        $data['feedback_content'] = $feedback_content;
		
		$footer_content = str_replace('=""','',$_POST['footer_content']);
		$footer_content = str_replace("'",'"',$footer_content);
		$footer_content = str_replace('""','"', $footer_content);
        $data['footer_content'] = $footer_content;
		
        $products = isset($_POST['images'])?$_POST['products']:[];
        $images = isset($_POST['images'])?$_POST['images']:[];
		
		// update landing setting
		$d->setTable('landing_setting');
        if ($d->num_rows() > 0) {
			$row = $d->fetch_array();
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("top_slide", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['top_slide'] = $photo;
				delete_file(_upload_landing . $row['top_slide']);
			}
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("middle_slide", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['middle_slide'] = $photo;
				delete_file(_upload_landing . $row['middle_slide']);
			}
			$contact_image = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("contact_image", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['contact_image'] = $photo;
				delete_file(_upload_landing . $row['contact_image']);
			}
		}
        $d->insert($data);
		// update product 
		update_landing_image($products,$id);
		update_landing_image($images,$id);
		
		redirect("index.php?com=landing-page&act=edit&id=".$id);
		
    }
	redirect("index.php?com=landing-page&act=list");
}

function get_item() {
    global $d,$item,$products,$images,$setting_id;
	if (empty($_GET) || !isset($_GET['id'])){
        transfer("Không nhận được dữ liệu", "index.php");
	}
    $sql = "select * from table_landing_setting where id =".$_GET['id'];
    
    $d->query($sql);
    if ($d->num_rows() == 0){
        transfer("Dữ liệu không có thực", "index.php");
	}
    $item = $d->fetch_array();
	
	// get imgage
	$sql2 = "select * from table_landing_image where setting_id =".$_GET['id']." order by type desc,sort_index";
    $d->query($sql2);
	if ($d->num_rows() == 0){
        transfer("Dữ liệu không có thực", "index.php");
	}
	$result = $d->result_array();
	$images = [];
	$products = [];
	$setting_id = $_GET['id'];
	// dump($result);
	foreach($result as $value){
		if($value['type'] == 'OTHER'){
			array_push($images,$value);
		}else{
			array_push($products,$value);
		}
	}
    
}

function fns_Rand_digit($min, $max, $num) {
    $result = '';
    for ($i = 0; $i < $num; $i++) {
        $result.=rand($min, $max);
    }
    return $result;
}

function save_item() {
    global $d;
    if (empty($_POST))
        transfer("Không nhận được dữ liệu", "index.php?com=bh&act=man");
    $id = isset($_POST['id']) ? themdau($_POST['id']) : "";
    if ($id) {//cap nhat
        $data['name'] = $_POST['name'];
        $data['count_down_sec'] = $_POST['count_down_sec'];
		
		$content = str_replace('=""','',$_POST['content']);
		$content = str_replace("'",'"',$content);
		$content = str_replace('""','"', $content);
        $data['content'] = $content;
		
		$feedback_content = str_replace('=""','',$_POST['feedback_content']);
		$feedback_content = str_replace("'",'"',$feedback_content);
		$feedback_content = str_replace('""','"', $feedback_content);
        $data['feedback_content'] = $feedback_content;
		
		$footer_content = str_replace('=""','',$_POST['footer_content']);
		$footer_content = str_replace("'",'"',$footer_content);
		$footer_content = str_replace('""','"', $footer_content);
        $data['footer_content'] = $footer_content;
		
        $products = isset($_POST['images'])?$_POST['products']:[];
        $images = isset($_POST['images'])?$_POST['images']:[];
		
		// update landing setting
		$d->setTable('landing_setting');
        $d->setWhere('id', $id);
		$d->select();
        if ($d->num_rows() > 0) {
			$row = $d->fetch_array();
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("top_slide", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['top_slide'] = $photo;
				delete_file(_upload_landing . $row['top_slide']);
			}
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("middle_slide", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['middle_slide'] = $photo;
				delete_file(_upload_landing . $row['middle_slide']);
			}
			$contact_image = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("contact_image", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['contact_image'] = $photo;
				delete_file(_upload_landing . $row['contact_image']);
			}
		}
        $d->update($data);
		// update product 
		update_landing_image($products,$id);
		update_landing_image($images,$id);
		
		redirect("index.php?com=landing-page&act=edit&id=".$id);
		
    }
}
function update_landing_image($images,$id){
	// dump($images);
    global $d;
	foreach($images as $key => $value){
		$d->where = "";
		$d->setTable('landing_image');
		$data = [];
		// insert 
		if(isset($value['is_add']) && $value['is_add'] == 1){
			$data['name'] = $value['name'];
			$data['type'] = $value['type'];
			$data['sort_index'] = $value['sort_index'];
			$data['setting_id'] = $id;
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("image_url_".$key, 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
				$data['image_url'] = $photo;
			}
			// dump($data);
			$d->insert($data);		
		}else{ // update and delete
			$data['name'] = $value['name'];
			$data['type'] = $value['type'];
			$data['sort_index'] = $value['sort_index'];
			$data['setting_id'] = $value['setting_id'];
			
			$d->setWhere('id', $value['id']);
			$d->select();
			if ($d->num_rows() > 0) {
				if(!isset($value['is_delete']) || $value['is_delete'] == 0){
					$row = $d->fetch_array();
					$file_name = fns_Rand_digit(0, 9, 8);
					if ($photo = upload_image("image_url_".$value['id'], 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_landing, $file_name)) {
						$data['image_url'] = $photo;
						delete_file(_upload_landing . $row['image_url']);
					}
					
					$d->update($data);
				}else{
					$d->delete('landing_image','id='.$value['id']);
				}
				
			}
		}
	}
}

function delete_item() {
    global $d;

    if (isset($_GET['id'])) {
        $id = themdau($_GET['id']);


        // xoa item
        $sql = "delete from #_bh where id='" . $id . "'";
        if ($d->query($sql))
            header("Location:index.php?com=bh&act=man");
        else
            transfer("Xóa dữ liệu bị lỗi", "index.php?com=bh&act=man");
    } else
        transfer("Không nhận được dữ liệu", "index.php?com=bh&act=man");
}
?>



