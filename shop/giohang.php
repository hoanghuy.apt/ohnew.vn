<!doctype html>
<html class="no-js " lang="vi">
<head>
<?php include('head.php'); ?>
<?php include('title.php'); ?>
<style type="text/css">
	
	

.dongdau {display:table; color:#000000; font-size:13px; font-weight:bold; width:100%;}

.dongdau .dong1anh{border-left:1px solid #f7f7f7;width:200px;padding:10px; display:table-cell; background: #f7f7f7;border-bottom:1px solid #e2e2e2;text-align:center; color:#000;}
.dongdau .dong1sp{border-left:1px solid #f7f7f7;width:250px;padding:10px; display:table-cell; background: #f7f7f7;border-bottom:1px solid #e2e2e2;text-align:center; color:#000;}
.dongdau .dong1gia{border-left:1px solid #f7f7f7;width:140px;padding:10px; display:table-cell; background: #f7f7f7;border-bottom:1px solid #e2e2e2;text-align:center; color:#000;}
.dongdau .dong1sl{border-left:1px solid #f7f7f7;width:120px;padding:10px; display:table-cell; background: #f7f7f7;border-bottom:1px solid #e2e2e2;text-align:center; color:#000;}
.dongdau .dong1xoa{border-left:1px solid #f7f7f7;width:70px;padding:10px; display:table-cell; background: #f7f7f7;border-bottom:1px solid #e2e2e2;text-align:center; color:#000;border-right:1px solid #e2e2e2;}


.dongdau .dong2anh{border-left:1px solid #f7f7f7;width:200px;padding:10px; display:table-cell;border-bottom:1px solid #e2e2e2;text-align:center; color:#FFFFFF;}
.dongdau .dong2anh img{width:75px;}
.dongdau .dong2sp{width:250px;padding:10px; display:table-cell;border-bottom:1px solid #e2e2e2;vertical-align:middle;text-align:center;}
.dongdau .dong2gia{width:140px;padding:10px; display:table-cell;border-bottom:1px solid #e2e2e2;vertical-align:middle;text-align:center;}
.dongdau .dong2sl{width:120px;padding:10px; display:table-cell;border-bottom:1px solid #e2e2e2;vertical-align:middle;text-align:center;}
.dongdau .dong2xoa{width:70px;padding:10px; display:table-cell;border-bottom:1px solid #e2e2e2;vertical-align:middle;text-align:center;border-right:1px solid #e2e2e2;}
.dongdau .dong2xoa img{width:35px; height:35px; cursor:pointer;}


.tongcong_tien{font-size:18px;text-align:right;padding-right:100px; width:100%; margin-top:10px;}

.nutchon_giohang{overflow:hidden; margin:10px 15px; border-bottom:1px dotted #CCCCCC; padding-bottom:5px;}
.nutchon_giohang .xoatoanbo,.nutchon_giohang .capnhat,.nutchon_giohang .thanhtoan_giohang,.tieptuc_muahang,.gui_donhang,.submit_dathang{padding:0px 10px; border:1px solid #CCCCCC; float:left; margin-right:10px; margin-bottom:5px;
-webkit-border-radius: 15px;-moz-border-radius: 15px;-ms-border-radius: 15px;-o-border-radius: 15px;border-radius: 15px; background:url(images/bg_title_box.png) repeat-x; cursor:pointer; font-size:16px; color:#000; font-weight:bold; transition:all 0.5s ease; -webkit-transition:all 0.5s ease; -moz-transition:all 0.5s ease;
-o-transition:all 0.5s ease; -ms-transition:all 0.5s ease;}

.nutchon_giohang .xoatoanbo:hover,.nutchon_giohang .capnhat:hover,.nutchon_giohang .thanhtoan_giohang:hover,.tieptuc_muahang:hover, .gui_donhang:hover,.submit_dathang:hover{box-shadow: 0 0 2px #6600CC inset; -webkit-box-shadow: 0 0 2px #6600CC inset; -ms-box-shadow: 0 0 2px #6600CC inset; -o-box-shadow: 0 0 2px #6600CC inset; -moz-box-shadow: 0 0 2px #6600CC inset; border:1px solid #6600CC;}

.table_thontinnhanhang td{padding:5px;}
.table_thontinnhanhang tr, .table_thontinnhanhang td{border:1px solid #CCCCCC; color:#999999}
.thongtin_khachhang input,.thongtin_khachhang textarea{width:250px; margin-bottom:10px;
padding: 6px;
  border: 1px solid #c6c6c6;
  font: normal 13px 'Arial';
  color: #000;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  border-radius: 8px;
  box-shadow: none;
  outline: none !important;
  vertical-align: top;

}
.support-online-content {
background-color: #fff;
border: solid 1px #f44940;
padding: 0px 0px 10px 0px;
}	
.noidung_giohang{margin-top:10px; background:#FFFFFF;font-size: 13px;
    font-weight: normal;}
.thongtin_khachhang{color:#000;}

.dong2xoa a.remove-item {
    background-color: #fff;
    background-image: none;
    color: #333;
    cursor: pointer;
    padding: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    cursor: pointer;
    text-decoration: none;
    float: left;
    transition: all 0.3s linear;
    -moz-transition: all 0.3s linear;
    -webkit-transition: all 0.3s linear;
    border: none;
}.dong2xoa a.remove-item:before {
    content: "\f014";
    font-family: FontAwesome;
    font-size: 14px;
}
	
.totals .inner {
    padding: 4px 0px 15px 0px;
    background-color: #fff;
    margin-top: 0px;
}#shopping-cart-totals-table {
    float: right;
    width: 100%;
    padding-bottom: 8px;
}.totals table th, .totals table td {
    padding: 5px;
    border: none !important;
    background: none !important;
    border: medium none !important;
    box-shadow: none !important;
    color: #333 !important;
}
.totals .price {
    font-size: 14px;font-weight: 900;
    color: #7bbd42;
}.checkout {
    color: #666666;
    padding-top: 5px;
    text-align: center;
    list-style: none;
    padding: 0;
    margin: 0;
}button.button.btn-proceed-checkout {
    background: #7bbd42;
    padding: 20px 0px;
    color: #fff;
    width: 100%;
}button.button.btn-proceed-checkout:before {
    content: "\f00c";
    font-family: FontAwesome;
    font-size: 20px;
    padding-right: 5px;
}button.button.btn-proceed-checkout span {
    font-size: 18px;
    font-weight: normal; text-transform:uppercase;
}.cart-collaterals h3 {
    font-size: 15px;
    color: #000;
    margin-bottom: 15px;
    border-bottom: 1px #ddd solid;
    padding: 10px 0;
    font-family: sfu_daxcondensedregular,Arial;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin-top: 20px;
}#shopping-cart-table {
    border: none;
    float: left;
    width: 100%;
    background-color: #FFFFFF;
    text-align: left;
    margin-top: 12px;
}.data-table thead tr, .data-table tfoot tr {
    background-color: #f7f7f7;
}#shopping-cart-table tfoot tr td.last {
    border-top: none;
}.data-table td {
    line-height: 20px;
    padding: 10px;
    vertical-align: top;
}.data-table thead th, .data-table thead td, .data-table tfoot th, .data-table tfoot td, .cart .totals table th, .cart .totals table td {
    color: #333;
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    font-family: sfu_daxcondensedregular,Arial;
}#shopping-cart-table button.button {
    display: inline-block;
    border: 0;
    background: #fff;
    padding: 8px 12px;
    font-size: 11px;
    text-align: center;
    white-space: nowrap;
    color: #333;
    font-weight: normal;
    transition: all 0.3s linear;
    -moz-transition: all 0.3s linear;
    -webkit-transition: all 0.3s linear;
    vertical-align: top;
    cursor: pointer;
    overflow: visible;
    width: auto;
    outline: none;
    -webkit-border-fit: lines;
    text-transform: uppercase;
    border: 1px solid #ddd;
}
.cart-table .btn-continue {
    float: left;
}button.button.btn-continue:before {
    content: "\f061";
    font-family: FontAwesome;
    font-size: 12px;
    padding-right: 8px;
}button.button span {
    font-weight: bold;
    text-transform: uppercase;
}.cart-table .btn-update, .cart-table .btn-empty {
    float: right;
    margin-left: 8px;
	border: 1px #ddd solid;
    background: #fff;
    transition: color 300ms ease-in-out 0s, background-color 300ms ease-in-out 0s, background-position 300ms ease-in-out 0s;
}.button:hover {
    border: 1px solid #7bbd42;
    background: #7bbd42;
    color: #FFF;
}input.qty {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #ddd;
    font-size: 15px;
    font-weight: normal;
    margin: 0 !important;
    outline: medium none;
    padding: 8px 5px 8px 12px;
    width: 55px;
}.data-table th {
    line-height: 20px;
    padding: 10px;
    font-weight: bold;
    font-size: 12px;
    font-family: 'Open Sans', sans-serif;
}.data-table thead th, .data-table thead td, .data-table tfoot th, .data-table tfoot td, .cart .totals table th, .cart .totals table td {
    color: #333;
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    font-family: sfu_daxcondensedregular,Arial;
}#shopping-cart-table .product-name {
    margin: 0px;
    padding-bottom: 0px;
    font-size: 14px;
    font-family: sfu_daxcondensedregular,Arial;
}
.data-table .price {
    font-size: 13px;
    font-weight: normal;
    color: #7bbd42;
}
	
	</style>
</head>
<body check="640/1000004640/1000126002/logo_checkout.png?v=1185" id="shop-ao-thun" class="template-index Default header-default layout-default" >
<?php include('ajax_loading.php'); ?>
<div id="page" class="flypanels-container preload">
  <?php include('header.php'); ?>
  <div class="flypanels-main">
    <div class="flypanels-content">
      <section id="page_content" class="">
        <?php include('header_top.php'); ?>
        <main class="main-content" role="main">
          <section id="breadcrumbs" style=" background:url(640/1000004640/1000126002/bg_image_va.jpg?v=1185) center top repeat transparent; " class="hidden">
            <div class="container">
              <h1 class="page-heading product-listing cat-name name__collection text-center hidde-xs"><span>Giỏ hàng</span></h1>
              <nav role="navigation" aria-label="breadcrumbs">
                <ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                  <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <a href="/" title="Trở về cửa hàng" itemprop="item"> <span itemprop="name">Trang chủ</span> </a> </li>
                  <li class="active" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"> <span itemprop="name">Giỏ hàng</span> </li>
                </ol>
              </nav>
            </div>
          </section>
          <section id="columns" class="columns-container product">
            <div class="container">
              <div class="row">
                <section id="center_column" class="col-sm-12 col-md-12">
                  <div class="page-wrap">
                    
					<div id="ajax_noidung_giohang" style="overflow:hidden;"><?php include('xemnhanh_noidung_giohang.php'); ?></div>
					
                  </div>
                </section>
              </div>
            </div>
          </section>
        </main>
        <?php include('footer.php'); ?>
        <script src='640/1000004640/1000126002/timbera900.js?v=1185' type='text/javascript'></script>
        <script>
	
	
</script>
        <script src='640/1000004640/1000126002/handlebars.mina900.js?v=1185' type='text/javascript'></script>
        <script id="CartTemplate" type="text/template">
    
        <form action="/cart" method="post" novalidate>
            <div class="ajaxcart__inner">
                {{#items}}
                <div class="ajaxcart__product">
                    <div class="ajaxcart__row" data-id="{{id}}">
                        <div class="grid">
                            <div class="grid__item large--two-thirds">
                                <div class="grid">
                                    <div class="grid__item one-quarter">
                                        <a href="{{url}}" class="ajaxcart__product-image"><img src="{{img}}" alt=""></a>
                                    </div>
                                    <div class="grid__item three-quarters">
                                        <a href="{{url}}" class="h4">{{name}}</a>
                                        <p>{{variation}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--one-third">
                                <div class="grid">
                                    <div class="grid__item one-third">
                                        <div class="ajaxcart__qty">
                                            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
                                            <input type="text" class="ajaxcart__qty-num" value="{{itemQty}}" min="0" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*">
                                            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
                                        </div>
                                    </div>
                                    <div class="grid__item one-third text-center">
                                        <p>{{price}}</p>
                                    </div>
                                    <div class="grid__item one-third text-right">
                                        <p>
                                            <small><a href="/cart/change?id={{id}}&amp;quantity=0" class="ajaxcart__remove" data-id="{{id}}">Xóa</a></small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{/items}}
            </div>
            <div class="ajaxcart__row text-right medium-down--text-center">
                <span class="h3">Tổng {{totalPrice}}</span>
                <input type="submit" class="{{btnClass}}" name="checkout" value="Thanh Toán">
            </div>
        </form>
    
</script>
        <script id="DrawerTemplate" type="text/template">
    
        <div id="AjaxifyDrawer" class="ajaxcart ajaxcart--drawer">
            <div id="AjaxifyCart" class="container {{wrapperClass}}"></div>
        </div>
    
</script>
        <script id="ModalTemplate" type="text/template">
    
        <div id="AjaxifyModal" class="ajaxcart ajaxcart--modal">
            <div id="AjaxifyCart" class=""></div>
        </div>
    
</script>
        <script id="AjaxifyQty" type="text/template">
    
        <div class="ajaxcart__qty">
            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
            <input type="text" class="ajaxcart__qty-num" value="{{itemQty}}" min="0" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*">
            <button type="button" class="ajaxcart__qty-adjust ajaxcart__qty--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
        </div>
    
</script>
        <script id="JsQty" type="text/template">
    
        <div class="js-qty">
            <input type="text" class=" js-qty__num" value="{{itemQty}}" min="1" data-id="{{id}}" aria-label="quantity" pattern="[0-9]*" name="{{inputName}}" id="{{inputId}}">
            <button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--minus" data-id="{{id}}" data-qty="{{itemMinus}}">&minus;</button>
            <button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--plus" data-id="{{id}}" data-qty="{{itemAdd}}">+</button>
        </div>
    
</script>
        <script src='640/1000004640/1000126002/ajaxifya900.js?v=1185' type='text/javascript'></script>
        <script>
		jQuery(function($) {
			ajaxifyHaravan.init({
				method: 'modal',
				wrapperClass: 'wrapper',
				formSelector: '.form-ajaxtocart',
				addToCartSelector: '.ajax_addtocart',
				cartCountSelector: '#CartCount',
				cartCostSelector: '#CartCost',
				toggleCartButton: '#CartToggle',
				useCartTemplate: true,
				btnClass: 'btn',
				moneyFormat: "{{amount}}₫",
				disableAjaxCart: false,
				enableQtySelectors: true,
				prependDrawerTo: 'body'
			});
		});
	</script>
        <script src='640/1000004640/1000126002/fastclick.mina900.js?v=1185' type='text/javascript'></script>
        <script src='640/1000004640/1000126002/owl.carousel.mina900.js?v=1185' type='text/javascript'></script>
        <script src='640/1000004640/1000126002/jquery.mmenu.min.alla900.js?v=1185' type='text/javascript'></script>
        <script>
	jQuery(function() {
		jQuery('.swatch :radio').change(function() {
	    	var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
	    	var optionValue = jQuery(this).val();
	    	jQuery(this)
  			.closest('form')
      		.find('.single-option-selector')
      		.eq(optionIndex)
      		.val(optionValue)
      		.trigger('change');
	  	}); 
	});
</script>
        <script>
	!function ($) {
		$(function(){
			$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { 
				e.stopPropagation(); 
            });
          	$(document.body).on('click', '[data-toggle="dropdown"]' ,function(){
                if(!$(this).parent().hasClass('open') && this.href && this.href != '#'){
                    window.location.href = this.href;
                }
            });
		})
	}(window.jQuery)
</script>
        <script>
    var Haravan = Haravan || {};
    Haravan.optionsMap = {};
    Haravan.updateOptionsInSelector = function(selectorIndex) {
        switch (selectorIndex) {
            case 0:
                var key = 'root';
                var selector = jQuery('.single-option-selector:eq(0)');
                break;
            case 1:
                var key = jQuery('.single-option-selector:eq(0)').val();
                var selector = jQuery('.single-option-selector:eq(1)');
                break;
            case 2:
                var key = jQuery('.single-option-selector:eq(0)').val();  
                key += ' / ' + jQuery('.single-option-selector:eq(1)').val();
                var selector = jQuery('.single-option-selector:eq(2)');
        }
        var initialValue = selector.val();
        selector.empty();    
        var availableOptions = Haravan.optionsMap[key];
        for (var i=0; i<availableOptions.length; i++) {
            var option = availableOptions[i];
            var newOption = jQuery('<option></option>').val(option).html(option);
            selector.append(newOption);
        }
        jQuery('.swatch[data-option-index="' + selectorIndex + '"] .swatch-element').each(function() {
            if (jQuery.inArray($(this).attr('data-value'), availableOptions) !== -1) {
                $(this).removeClass('soldout').show().find(':radio').removeAttr('disabled','disabled').removeAttr('checked');
            }
            else {
                $(this).addClass('soldout').hide().find(':radio').removeAttr('checked').attr('disabled','disabled');
            }
        });
        if (jQuery.inArray(initialValue, availableOptions) !== -1) {
            selector.val(initialValue);
        }
        selector.trigger('change');  
    };
    Haravan.linkOptionSelectors = function(product) {
        // Building our mapping object.
        for (var i=0; i<product.variants.length; i++) {
            var variant = product.variants[i];
            if (variant.available) {
                // Gathering values for the 1st drop-down.
                Haravan.optionsMap['root'] = Haravan.optionsMap['root'] || [];
                Haravan.optionsMap['root'].push(variant.option1);
                Haravan.optionsMap['root'] = Haravan.uniq(Haravan.optionsMap['root']);
                // Gathering values for the 2nd drop-down.
                if (product.options.length > 1) {
                    var key = variant.option1;
                    Haravan.optionsMap[key] = Haravan.optionsMap[key] || [];
                    Haravan.optionsMap[key].push(variant.option2);
                    Haravan.optionsMap[key] = Haravan.uniq(Haravan.optionsMap[key]);
                }
                // Gathering values for the 3rd drop-down.
                if (product.options.length === 3) {
                    var key = variant.option1 + ' / ' + variant.option2;
                    Haravan.optionsMap[key] = Haravan.optionsMap[key] || [];
                    Haravan.optionsMap[key].push(variant.option3);
                    Haravan.optionsMap[key] = Haravan.uniq(Haravan.optionsMap[key]);
                }
            }
        }
        // Update options right away.
        Haravan.updateOptionsInSelector(0);
        if (product.options.length > 1) Haravan.updateOptionsInSelector(1);
        if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
        // When there is an update in the first dropdown.
        jQuery(".single-option-selector:eq(0)").change(function() {
            Haravan.updateOptionsInSelector(1);
            if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
            return true;
        });
        // When there is an update in the second dropdown.
        jQuery(".single-option-selector:eq(1)").change(function() {
            if (product.options.length === 3) Haravan.updateOptionsInSelector(2);
            return true;
        });
    };
</script>
      </section>
      <!-- <p id="back-top">
						<a class="btn btn-outline" href="#top" title="Scroll To Top">Scroll To Top</a>
					</p>-->
    </div>
  </div>
</div>
<?php include('js_footer.php'); ?>
<script src="js/ajax_popup_gohang.js" type="text/javascript"></script>
</body>
</html>
