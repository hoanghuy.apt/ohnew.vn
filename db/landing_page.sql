-- table_landing_setting
CREATE TABLE `table_landing_setting` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `top_slide` VARCHAR(500) NOT NULL , `middle_slide` VARCHAR(500) NOT NULL , `count_down_sec` INT(11) NOT NULL , `name` VARCHAR(500) NOT NULL , `is_active` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `table_landing_setting` ADD `contact_image` VARCHAR(500) NULL AFTER `middle_slide`;
ALTER TABLE `table_landing_setting` ADD `content` TEXT NULL AFTER `name`;
ALTER TABLE `table_landing_setting` ADD `feedback_content` TEXT NULL DEFAULT NULL AFTER `content`;
INSERT INTO `table_landing_setting` (`id`, `top_slide`, `middle_slide`, `contact_image`, `count_down_sec`, `name`, `content`, `is_active`) VALUES ('1', 'landingpage-slide.jpg', 'sale_50.jpg', 'contact.jpg', '50000', 'Landing page 1', 'Xưởng may túi vải bố, túi tote, túi canvas OHNEW Nhận may túi vải số lượng từ 10 chiếc đến 1.000.0000 + chiếc - Giá rẻ nhất - May nhanh nhất - Tư vẫn hỗ trợ 24/7/365 - Giao hàng miễn phí tận nơi Túi vải không dệt, túi vải quà tặng, túi tote, túi vải bố, túi canvas', '1');

-- table_landing_image
CREATE TABLE `table_landing_image` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `setting_id` INT(11) NOT NULL , `image_url` VARCHAR(500) NOT NULL , `type` VARCHAR(500) NOT NULL DEFAULT 'PRODUCT' , `name` VARCHAR(500) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `table_landing_image` 
ADD COLUMN `sort_index` INT(11) NOT NULL DEFAULT 0 COMMENT '' AFTER `name`;


-- insert product
INSERT INTO `table_landing_image` (`id`, `setting_id`, `image_url`, `type`, `name`) VALUES ('1', '1', 'product_1.jpg', 'TENDENCY', 'Túi 1'), ('2', '1', 'product_2.jpg', 'TENDENCY', 'Túi 2');

INSERT INTO `table_landing_image` (`id`, `setting_id`, `image_url`, `type`, `name`) VALUES ('3', '1', 'product_3.jpg', 'TENDENCY', 'Túi 3'), ('4', '1', 'product_4.jpg', 'TENDENCY', 'Túi 4');

INSERT INTO `table_landing_image` (`id`, `setting_id`, `image_url`, `type`, `name`) VALUES ('5', '1', 'product_5.jpg', 'PRODUCT', 'Túi 5'), ('6', '1', 'product_6.jpg', 'PRODUCT', 'Túi 6'), ('7', '1', 'product_7.jpg', 'PRODUCT', 'Túi 7'), ('8', '1', 'product_8.jpg', 'PRODUCT', 'Túi 8');

INSERT INTO `table_landing_image` (`id`, `setting_id`, `image_url`, `type`, `name`) VALUES ('9', '1', 'other_1.jpg', 'OHTER', 'Other 1'), ('10', '1', 'other_2.jpg', 'OTHER', 'Other 2'), ('11', '1', 'other_3.jpg', 'OTHER', 'Other 3'), ('12', '1', 'other_4.jpg', 'OTHER', 'Other 5');

ALTER TABLE `table_landing_setting` ADD `footer_content` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `is_active`;