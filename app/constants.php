<?php
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

//define project directories
define('BASE_DIR', dirname(__FILE__) . DS);
define('LIB_DIR', BASE_DIR . 'library' . DS);

define('HELPER_DIR', BASE_DIR . 'helper' . DS);