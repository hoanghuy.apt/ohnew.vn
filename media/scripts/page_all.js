/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2019. MIT licensed.
 */
$(document).ready(function () {
    // show hidden menu
    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active'); // effect navbar

    console.log($('.navbar-nav .col-menu .menu li'));
    $('.navbar-nav .col-menu .menu li').hover(function () {
        $('#product-img').attr('src', $(this).find('img').attr('src'));
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('header').addClass('scroll');
        } else {
            $('header').removeClass('scroll');
        }
    });
    $(window).on("load", function (e) {
        $(".navbar-nav > li > .sub-menu,.menu_product").parent("li").append("<span class='show-menu'><i class='icon'></i></span>");
        $('.show-menu').on('click', function () {
            $('.navbar-nav li').removeClass('is-open');
            $(this).parent('li').toggleClass('is-open');
        });
    }); //fancybox

    $(".contract_list a,.partner_img a[rel='img']").fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'titlePosition': 'over',
        'overlayColor': '#000',
        'overlayOpacity': 0.66
    });
    $("a[rel='video']").click(function () {
        $.fancybox({
            'padding': 0,
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'title': this.title,
            'width': 640,
            'height': 385,
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': 'swf',
            'swf': {
                'wmode': 'opaque',
                'allowfullscreen': 'true'
            }
        });
        return false;
    }); //slick js

    $('.contract_list').slick({
        centerMode: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        autoplay: true,
        dots: false,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2
            }
        }]
    });
    $('.partner_list').slick({
        centerMode: false,
        slidesToShow: 8,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        autoplay: true,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3
            }
        }]
    });
    $('.banner_slider').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        speed: 1200,
        autoplay: true,
        dots: false,
        arrows: true
    });

    $('.category_detail').slick({
        centerMode: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        autoplay: true,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.nav_contract a').on('click', function () {
        $('.nav_contract a').removeClass('active');
        $(this).addClass('active');
        let data_contract = $(this).data('contract');
        $('.contract_content img').attr('src', data_contract);
    });

    // ===== Scroll to Top ====
    $(window).scroll(function () {
        var scrollTop = $(this).scrollTop();

        if (scrollTop >= 300) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            if (scrollTop >= 100) {
                $(".order_notifi").css('top', '13px');
            } else if (scrollTop < 100) {
                $(".order_notifi").css('top', '10%');
            }
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function () {      // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 500);
    });

    var customerList = [
        {
            name: 'Ngô Vạn Thông',
            avatar: 'media/system/images/avatar.png',
            phoneNumber: '0334573xxx'
        },
        {
            name: 'Hoàng Phúc Hòa',
            avatar: 'media/system/images/avatar1.jpg',
            phoneNumber: '0362720xxx'
        },
        {
            name: 'Trịnh Thế Hùng',
            avatar: 'media/system/images/avatar3.png',
            phoneNumber: '0336705xxx'
        },
        {
            name: 'Đinh Phương Nam',
            avatar: 'https://uinames.com/api/photos/male/17.jpg',
            phoneNumber: '0338477xxx'
        },
        {
            name: 'Nguyễn Gia Vinh',
            avatar: 'media/system/images/avatar4.jpeg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Đặng Thế Dũng',
            avatar: 'media/system/images/avatar4.jpeg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Dương Hùng Dũng',
            avatar: 'media/system/images/avatar3.png',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Đào Việt Sơn',
            avatar: 'https://uinames.com/api/photos/male/14.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Nguyễn Thế Sơn',
            avatar: 'https://uinames.com/api/photos/male/14.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Mai Ánh Ngọc',
            avatar: 'https://uinames.com/api/photos/female/13.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Nguyễn Ngọc Hân',
            avatar: 'https://uinames.com/api/photos/female/13.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Nguyễn Quỳnh Lam',
            avatar: 'media/system/images/avatar7.png',
            phoneNumber: '0338477xxx'
        },
        {
            name: 'Phạm Thảo Uyên',
            avatar: 'media/system/images/avatar7.png',
            phoneNumber: '0333936xxx'
        },
        {
            name: 'Nguyễn Thúy Kiều',
            avatar: 'media/system/images/avatar6.jpg',
            phoneNumber: '0333936xxx'
        },
        {
            name: 'Phạm Thùy Giang',
            avatar: 'media/system/images/avatar2.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Hoàng Việt Cương',
            avatar: 'media/system/images/avatar5.jpg',
            phoneNumber: '0338477xxx'
        },
        {
            name: 'Nguyễn Anh Thái',
            avatar: 'media/system/images/avatar5.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Đặng Chí Kiên',
            avatar: 'media/system/images/avatar5.jpg',
            phoneNumber: '0339360xxx'
        },
        {
            name: 'Huỳnh Bá Phước',
            avatar: 'https://uinames.com/api/photos/male/14.jpg',
            phoneNumber: '0331780xxx'
        },
        {
            name: 'Đỗ Duy Minh',
            avatar: 'https://uinames.com/api/photos/male/8.jpg',
            phoneNumber: '0331780xxx'
        },
        {
            name: 'Quách Tuấn Anh',
            avatar: 'media/system/images/avatar1.jpg',
            phoneNumber: '0317800xxx'
        },
        {
            name: 'Phan Tùng Anh',
            avatar: 'https://uinames.com/api/photos/male/14.jpg',
            phoneNumber: '0331780xxx'
        },
        {
            name: 'Lê Sỹ Hoàng',
            avatar: 'media/system/images/avatar1.jpg',
            phoneNumber: '0317800xxx'
        },
    ];

    var showOrderNotification = function () {
        var rand = Math.floor(Math.random() * customerList.length);
        var cus = customerList[rand];
        $('.order_notifi .avatar').attr('src', cus.avatar);
        $('.order_notifi .name').html(cus.name);
        $('.order_notifi .phone').html(cus.phoneNumber);

        $(".order_notifi").animate({right: "1rem"}, "slow");

        setTimeout(function () {
            $(".order_notifi").animate({right: "-40rem"}, "slow");
        }, 5000);
    };

    setTimeout(showOrderNotification, 2000);
    setInterval(showOrderNotification, 13000);
}); //scroll effect

$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight() - 100;
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height() - 100;
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll load', function () {
    $('.fadeup').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('fadeInUp').css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }
    });
    $('.fadein').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('fadeIn').css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }
    });
    $('.zoomin').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('zoomIn').css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }
    });
    $('.fadeinleft').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('fadeInLeft').css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }
    });
    $('.fadeinright').each(function () {
        if ($(this).isInViewport()) {
            $(this).addClass('fadeInRight').css({
                'opacity': '1',
                'visibility': 'visible'
            });
        }
    });
});