<?php

if (!defined('_source')) die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "edit":
        if (isPost()) {
            return save();
        }

        show();

        $template = "partner/edit";
        break;
    case "del":
        delete();
        break;
    default:
        listItems();
        $template = "partner/index";
}

function listItems()
{
    global $d, $items;

    $sql = "select * from #_partner order by id desc";
    $d->query($sql);

    $items = $d->result_array();
}

function save()
{
    global $d, $flash;
    $d->setTable('partner');

    $logo = $_FILES['logo'];
    $id = isset($_POST['id']) ? themdau($_POST['id']) : null;

    $file_name = genFileName($logo['name']);
    $data = [];

    if ($id) {
        if (!empty($logo['name'])) {
            if ($photo = upload_image("logo", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_partner, $file_name)) {
                $data['logo'] = $photo;
            }
        }

        $data['name'] = $_POST['name'];
        $data['target_link'] = $_POST['target_link'];

        $d->setWhere('id', $id);
        if ($d->update($data)) {
            $flash->success('Cập nhật thông tin đối tác thành công');
            return redirectTo(router('admin', ['com' => 'partner']));
        }

        return transfer("Cập nhật dữ liệu bị lỗi", url());
    }

    if (empty($logo['name'])) {
        $flash->error('Vui lòng upload logo cho đối tác');
        return redirectTo(url());
    }

    $data['name'] = $_POST['name'];

    if ($photo = upload_image('logo', 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_partner, $file_name)) {
        $data['logo'] = $photo;
    }

    $data['target_link'] = $_POST['target_link'];

    if ($d->insert($data)) {
        $flash->success('Thêm mới đối tác thành công');
        return redirectTo(router('admin', ['com' => 'partner']));
    }
    $flash->error('Lưu dữ liệu bị lỗi');
    return redirectTo(url());
}

function show()
{
    global $d, $item;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $sql = "select * from #_partner where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() == 0) {
            return transfer("Dữ liệu không có thực", url());
        }

        $item = $d->fetch_array();
    }
}

function delete()
{
    global $d, $flash;

    $id = isset($_GET['id']) ? themdau($_GET['id']) : null;

    if ($id) {
        $d->reset();
        $sql = "select * from #_partner where id='" . $id . "'";
        $d->query($sql);
        if ($d->num_rows() > 0) {
            $sql = "delete from #_partner where id='" . $id . "'";
            $d->query($sql);
        }

        $flash->success('Xóa đối tác thành công');

        return redirectTo(router('admin', ['com' => 'partner']));
    }

    return transfer("Không nhận được dữ liệu", router('admin', ['com' => 'partner']));
}

?>


