<?php

if (!defined('_source'))
    die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "man":
        get_items();
        $template = "htdl/items";
        break;
    case "add":
       
        $template = "htdl/item_add";
        break;
    case "edit":
        get_item();
        
        $template = "htdl/item_add";
        break;
    case "save":
        save_item();
       
        break;
    case "delete":
        delete_item();
        break;

    default:
        $template = "index";
}

//doc cac muc menu cha va con
/* lay du lieu cho hai bang dat_tour va bang khach_hang */

function fns_Rand_digit($min,$max,$num)
{
    $result='';
    for($i=0;$i<$num;$i++){
        $result.=rand($min,$max);
    }
    return $result;
}
function get_items() // hien tat ca cac menu
{
   
    global $d, $items, $paging;
	
    $sql = "select * from #_htdl order by id DESC";
    $d->query($sql);
    $items = $d->result_array();
}

function get_item() {
    global $d, $item;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : "";
    if (!$id)
        transfer("Không nhận được dữ liệu", "index.php?com=htdl&act=man");

    $sql = "select * from #_htdl where id ='" . $id . "'";
    $d->query($sql);
    if ($d->num_rows() == 0)
        transfer("Dữ liệu không có thực", "index.php?com=htdl&act=man");
    $item = $d->fetch_array();
}

function save_item() {
    global $d;
	$file_name=fns_Rand_digit(0,9,8);
    if (empty($_POST))
        transfer("Không nhận được dữ liệu", "index.php?com=htdl&act=man");
    $id = isset($_POST['id']) ? themdau($_POST['id']) : "";
    if ($id) {//cap nhat
	
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
            $d->setTable('htdl');
            $d->setWhere('id', $id);
            $d->select();
            if($d->num_rows()>0){
                $row = $d->fetch_array();
                delete_file(_upload_gioithieu.$row['hinhanh']);
            }
        }
		
        $data['ten'] = $_POST['ten'];
        $data['sdt'] = $_POST['sdt'];
        $d->setTable('htdl');
        $d->setWhere('id', $id);
        if ($d->update($data))
            redirect("index.php?com=htdl&act=man");
        else
            transfer("Cập nhật dữ liệu bị lỗi", "index.php?com=htdl&act=man");
    }else {//them moi
	
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
        }
		
        $data['ten'] = $_POST['ten'];
        $data['sdt'] = $_POST['sdt'];
        
       
      
        $d->setTable('htdl');
        if ($d->insert($data))
            redirect("index.php?com=htdl&act=man");
        else
            transfer("Lưu dữ liệu bị lỗi", "index.php?com=htdl&act=man");
    }
}

function delete_item() {
    global $d;

    if (isset($_GET['id'])) {
        $id = themdau($_GET['id']);
		$d->reset();
		$sql = "select * from #_htdl where id='" . $id . "'";
		
        $d->query($sql);
        if ($d->num_rows() > 0) {
            while ($row = $d->fetch_array()) {
                delete_file(_upload_gioithieu . $row['hinhanh']);
				delete_file(_upload_gioithieu . $row['hinhanh1']);
            }
            $sql = "delete from #_htdl where id='" . $id . "'";
            $d->query($sql);
        }

        // xoa item
        
        if ($d->query($sql))
            header("Location:index.php?com=htdl&act=man");
        else
            transfer("Xóa dữ liệu bị lỗi", "index.php?com=htdl&act=man");
    } else
        transfer("Không nhận được dữ liệu", "index.php?com=htdl&act=man");
}
?>



