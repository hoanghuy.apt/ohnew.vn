String.prototype.replaceAll = function(t, e) {
    return this.valueOf().split(t).join(e)
}
,
String.prototype.format = function() {
    for (var t = this, e = 0; e < arguments.length; e++) {
        var i = new RegExp("\\{" + e + "\\}","gi");
        t = t.replace(i, arguments[e])
    }
    return t.valueOf()
}
,
Array.prototype.removeSpace = function() {
    var t = [];
    return this.forEach(function(e) {
        (e = e.trim()).length > 0 && t.push(e)
    }),
    t
}
,
Array.prototype.random = function() {
    return this[Math.floor(Math.random() * this.length)]
}
,
Array.prototype.unique = function() {
    return this.filter(function(t, e, i) {
        return i.indexOf(t) == e
    })
}
,
Array.prototype.except = function(t) {
    var e = this;
    return Array.isArray(t) && t.forEach(function(t) {
        var i = e.indexOf(t);
        -1 != i && e.splice(i, 1)
    }),
    e
}
,
Array.prototype.only = function(t) {
    var e = [];
    return Array.isArray(t) && this.forEach(function(i) {
        -1 != t.indexOf(i) && e.push(i)
    }),
    e
}
;
var LadiPageScriptV2 = LadiPageScriptV2 || function() {}
;
LadiPageScriptV2.prototype.init = function() {
    this.const = {
        DESKTOP: "desktop",
        MOBILE: "mobile",
        DOMAIN_GOOGLE_DOCS: "docs.google.com",
        POWERED_BY_IMAGE: "https://w.ladicdn.com/source/ladipage.svg",
        APP_RUNTIME_PREFIX: "_runtime",
        DATA_ACTION_TYPE: {
            link: "link",
            section: "section",
            email: "email",
            phone: "phone",
            popup: "popup",
            hidden_show: "hidden_show",
            lightbox_image: "lightbox_image",
            lightbox_video: "lightbox_video",
            lightbox_iframe: "lightbox_iframe"
        },
        COUNTDOWN_TYPE: {
            countdown: "countdown",
            daily: "daily",
            endtime: "endtime"
        },
        COUNTDOWN_ITEM_TYPE: {
            day: "day",
            hour: "hour",
            minute: "minute",
            seconds: "seconds"
        },
        VIDEO_TYPE: {
            youtube: "youtube"
        },
        TRACKING_NAME: "ladicid",
        PUBLISH_STYLE: {
            desktop_min_width: 768
        },
        ANIMATED_LIST: ["rotate-1", "rotate-2", "rotate-3", "type", "scale", "loading-bar", "slide", "clip", "zoom", "push"],
        POSITION_TYPE: {
            default: "default",
            top: "top",
            bottom: "bottom",
            top_left: "top_left",
            top_center: "top_center",
            top_right: "top_right",
            center_left: "center_left",
            center_right: "center_right",
            bottom_left: "bottom_left",
            bottom_center: "bottom_center",
            bottom_right: "bottom_right"
        },
        INPUT_TYPE: {
            tel: "tel",
            text: "text",
            select_multiple: "select_multiple",
            number: "number",
            email: "email",
            textarea: "textarea",
            select: "select",
            radio: "radio",
            checkbox: "checkbox"
        },
        FORM_THANKYOU_TYPE: {
            default: "default",
            url: "url",
            popup: "popup"
        },
        GAME_RESULT_TYPE: {
            default: "default",
            popup: "popup"
        },
        PERCENT_TRACKING_SCROLL: [0, 25, 50, 75, 100],
        TIME_ONPAGE_TRACKING: [10, 30, 60, 120, 180, 300, 600],
        FORM_CONFIG_TYPE: {
            email: "EMAIL",
            mail_chimp: "MAIL_CHIMP",
            infusion_soft: "INFUSION_SOFT",
            active_campaign: "ACTIVE_CAMPAIGN",
            hubspot: "HUBSPOT",
            smtp: "SMTP",
            get_response: "GET_RESPONSE",
            google_sheet: "GOOGLE_SHEET",
            google_form: "GOOGLE_FORM",
            custom_api: "CUSTOM_API",
            ladisales: "LADISALES"
        },
        LANG: {
            ALERT_TITLE: "Thông báo",
            ALERT_BUTTON_TEXT: "OK",
            GAME_RESULT_MESSAGE: "Chúc mừng bạn nhận được {{coupon_text}}. Nhập mã: {{coupon_code}} để sử dụng. Bạn còn {{spin_turn_left}} lượt quay.",
            GAME_MAX_TURN_MESSAGE: "Bạn đã hết lượt quay.",
            FORM_SEND_DATA_ERROR: "Đã xảy ra lỗi, vui lòng thử lại!",
            FORM_SEND_DATA_NO_CONFIG: "Vui lòng kiểm tra lại cấu hình Form!",
            FORM_THANKYOU_MESSAGE_DEFAULT: "Cảm ơn bạn đã quan tâm!",
            FORM_INPUT_REQUIRED_ERROR: "Vui lòng nhập đầy đủ các trường thông tin!",
            FORM_INPUT_EMAIL_REGEX: "Vui lòng nhập đúng định dạng email!",
            FORM_INPUT_TEXT_REGEX: "Vui lòng nhập đúng định dạng!"
        }
    },
    this.runtime = {
        backdrop_popup_id: "backdrop-popup",
        lightbox_screen_id: "lightbox-screen",
        builder_section_popup_id: "SECTION_POPUP",
        ladipage_powered_by_id: "ladipage_powered_by",
        current_element_mouse_down_carousel: null,
        current_element_mouse_down_carousel_position_x: 0,
        current_element_mouse_down_carousel_diff: 40,
        current_element_mouse_down_gallery_control: null,
        current_element_mouse_down_gallery_control_time: 0,
        current_element_mouse_down_gallery_control_time_click: 300,
        current_element_mouse_down_gallery_control_position_x: 0,
        current_element_mouse_down_gallery_view: null,
        current_element_mouse_down_gallery_view_position_x: 0,
        current_element_mouse_down_gallery_view_diff: 40,
        scroll_show_popup: {},
        scroll_depth: [],
        scroll_to_section: {},
        isMobileOnly: !1,
        interval_countdown: null,
        interval_gallery: null,
        timeout_gallery: {},
        interval_carousel: null,
        timenext_carousel: {},
        interval_powered_by: null,
        isClient: !1,
        isDesktop: !0,
        isIE: !1,
        device: this.const.DESKTOP,
        ladipage_id: null,
        tmp: {},
        tabindexForm: 0,
        eventData: {},
        timenow: 0,
        widthScrollBar: 0,
        replaceStr: {},
        replacePrefixStart: "{{",
        replacePrefixEnd: "}}"
    }
}
,
LadiPageScriptV2.prototype.run = function(t) {
    var e = this;
    this.runtime.isIE = !!document.documentMode,
    this.runtime.isIE = this.runtime.isIE ? this.runtime.isIE : !this.runtime.isIE && !!window.StyleMedia,
    this.runtime.isClient = t,
    this.runtime.timenow = this.getCookie("_timenow"),
    this.isEmpty(this.runtime.timenow) ? (this.runtime.timenow = Date.now(),
    this.setCookie(null, "_timenow", this.runtime.timenow, 0, !0, window.location.pathname)) : this.runtime.timenow = parseFloat(this.runtime.timenow) || 0;
    try {
        this.runtime.widthScrollBar = window.innerWidth - document.documentElement.clientWidth
    } catch (t) {}
    if (t) {
        if (this.isString(this.runtime.eventData))
            try {
                var i = decodeURIComponent(this.runtime.eventData);
                this.runtime.eventData = JSON.parse(i)
            } catch (t) {
                String.prototype.decode = function() {
                    return this.valueOf().replaceAll(/&amp;/g, "&").replaceAll(/&gt;/g, ">").replaceAll(/&lt;/g, "<").replaceAll(/&quot;/g, '"')
                }
                ;
                var n = this.runtime.eventData.decode();
                n = n.replaceAll("\r\n", "").replaceAll("\n", ""),
                this.runtime.eventData = JSON.parse(n)
            }
    } else
        this.runtime.eventData = LadiPage.generateEventDataAll(t),
        this.runtime.isMobileOnly = LadiPage.data.is_mobile_only,
        this.runtime.ladipage_id = LadiPage.publish.id;
    this.runtime.isMobileOnly && Object.keys(e.runtime.eventData).forEach(function(t) {
        Object.keys(e.runtime.eventData[t]).forEach(function(i) {
            if (i.toLowerCase().startsWith(e.const.MOBILE)) {
                var n = e.const.DESKTOP + i.substring(e.const.MOBILE.length);
                e.runtime.eventData[t][n] = e.runtime.eventData[t][i]
            }
        })
    });
    this.isNull(window.ladi_is_desktop) ? this.runtime.isDesktop = t ? !/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()) : LadiPage.isDesktop() : this.runtime.isDesktop = t ? window.ladi_is_desktop : LadiPage.isDesktop(),
    this.runtime.device = this.runtime.isDesktop ? this.const.DESKTOP : this.const.MOBILE,
    this.runtime.tmp.isFirstScroll = !0,
    this.runtime.tmp.runAfterLocation = [];
    var a = e.getURLSearchParams(window.location.search, null, !0)
      , r = "";
    Object.keys(a).forEach(function(t) {
        t != e.const.TRACKING_NAME && (e.setDataReplaceStr(t, a[t]),
        (e.isArray(a[t]) ? a[t] : [a[t]]).forEach(function(i) {
            e.isEmpty(r) ? r += "?" : r += "&",
            r += t + "=" + encodeURIComponent(i)
        }))
    });
    var o = this.runtime.isDesktop
      , l = Object.keys(this.runtime.eventData)
      , s = {};
    s[this.const.TRACKING_NAME] = e.isEmpty(a[this.const.TRACKING_NAME]) ? e.getCookie(this.const.TRACKING_NAME) : a[this.const.TRACKING_NAME],
    this.historyReplaceState(window.location.pathname + r + window.location.hash),
    this.deleteCookie(this.const.TRACKING_NAME);
    var c, d = function(t, i, n, a) {
        if (e.isEmpty(e.runtime.timeout_gallery[t]) && e.isEmpty(e.runtime.current_element_mouse_down_gallery_view) && e.isEmpty(e.runtime.current_element_mouse_down_gallery_control)) {
            var r = document.getElementById(t);
            if (!(e.isEmpty(r) || e.runtime.tmp.gallery_playing_video && i)) {
                var o = r.getElementsByClassName("ladi-gallery-view-item")
                  , l = r.getElementsByClassName("ladi-gallery-control-item");
                if (0 != o.length && 0 != o.length) {
                    var s = r.getAttribute("data-is-next") || "true";
                    s = "true" == s.toLowerCase();
                    var c = parseFloat(r.getAttribute("data-current")) || 0
                      , d = parseFloat(r.getAttribute("data-max-item")) || 0;
                    i ? s ? c >= d - 1 ? (c = d - 2,
                    s = !1) : c++ : c <= 0 ? (c = 1,
                    s = !0) : c-- : s ? c++ : c--,
                    c < 0 && (c = 0),
                    c >= d - 1 && (c = d - 1),
                    e.isEmpty(n) && (n = s ? "next" : "prev"),
                    e.isEmpty(a) && (a = s ? "left" : "right"),
                    e.runtime.tmp.gallery_playing_video && !o[c].classList.contains("selected") && e.stopAllVideo(!0),
                    o[c].classList.add(n),
                    r.querySelectorAll(".ladi-gallery-view-item.selected")[0].classList.add(a);
                    var u = 1e3 * (parseFloat(getComputedStyle(o[c]).transitionDuration) || 0);
                    e.runtime.timeout_gallery[t] = e.runTimeout(function() {
                        o[c].classList.add(a),
                        e.runtime.timeout_gallery[t] = e.runTimeout(function() {
                            for (var i = 0; i < o.length; i++)
                                i == c ? o[i].classList.add("selected") : o[i].classList.remove("selected"),
                                o[i].style.removeProperty("left"),
                                o[i].classList.remove(n),
                                o[i].classList.remove(a);
                            delete e.runtime.timeout_gallery[t]
                        }, u - 5)
                    }, 5);
                    for (var p = 0; p < l.length; p++)
                        (parseFloat(l[p].getAttribute("data-index")) || 0) == c ? l[p].classList.add("selected") : l[p].classList.remove("selected");
                    var m = e.getElementBoundingClientRect(r)
                      , g = e.getElementBoundingClientRect(r.getElementsByClassName("ladi-gallery-control-item")[c]);
                    if (r.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-top") || r.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-bottom")) {
                        var y = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control")[0]).width) || 0
                          , h = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control-item")[c]).width) || 0
                          , f = g.x - m.x - (y - h) / 2;
                        f = -(f -= parseFloat(r.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("left")) || 0) > 0 ? 0 : -f;
                        var _ = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control-box")[0]).width) || 0;
                        f < (_ = (_ = -(_ -= parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control")[0]).width) || 0)) > 0 ? 0 : _) && (f = _),
                        r.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("left", f + "px")
                    } else {
                        var v = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control")[0]).height) || 0
                          , E = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control-item")[c]).height) || 0
                          , w = g.y - m.y - (v - E) / 2;
                        w = -(w -= parseFloat(r.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("top")) || 0) > 0 ? 0 : -w;
                        var L = parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control-box")[0]).height) || 0;
                        w < (L = (L = -(L -= parseFloat(getComputedStyle(r.getElementsByClassName("ladi-gallery-control")[0]).height) || 0)) > 0 ? 0 : L) && (w = L),
                        r.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("top", w + "px")
                    }
                    r.setAttribute("data-is-next", s),
                    r.setAttribute("data-current", c)
                }
            }
        }
    }, u = function(t, i) {
        if ("gallery" == i) {
            var n = document.getElementById(t);
            if (!e.isEmpty(n)) {
                var a = n.getElementsByClassName("ladi-gallery-control-item").length;
                n.setAttribute("data-max-item", a);
                var r = function(i) {
                    i.stopPropagation(),
                    function(t, i) {
                        var n = i.getAttribute("data-video-type")
                          , a = i.getAttribute("data-video-url")
                          , r = e.getVideoId(n, a)
                          , o = i.getAttribute("data-index")
                          , l = document.getElementById(t + "_" + o + "_player");
                        e.isEmpty(l) && (e.stopAllVideo(),
                        e.runtime.tmp.gallery_playing_video = !0,
                        n == e.const.VIDEO_TYPE.youtube && (l = document.createElement("iframe"),
                        i.parentElement.insertBefore(l, i.nextSibling),
                        l.outerHTML = '<iframe id="' + t + "_" + o + '_player" class="iframe-video-play" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" src="https://www.youtube.com/embed/' + r + '?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'))
                    }(t, i.target)
                };
                if (a > 0) {
                    for (var o = 0; o < a; o++) {
                        var l = n.getElementsByClassName("ladi-gallery-view-item")[o];
                        e.isEmpty(l) || l.classList.contains("play-video") && l.addEventListener("click", r)
                    }
                    n.setAttribute("data-current", 0),
                    n.setAttribute("data-is-next", !0)
                }
            }
        }
    }, p = function(t, i) {
        if ((e.isEmpty(e.runtime.timenext_carousel[t]) || !(e.runtime.timenext_carousel[t] > Date.now())) && e.isEmpty(e.runtime.current_element_mouse_down_carousel)) {
            var n = document.getElementById(t);
            if (!e.isEmpty(n)) {
                var a = n.getAttribute("data-is-next") || "true";
                a = "true" == a.toLowerCase();
                var r = parseFloat(n.getAttribute("data-current")) || 0
                  , o = parseFloat(e.runtime.eventData[t][e.runtime.device + ".option.carousel_crop.width"]) || 0
                  , l = parseFloat(e.runtime.eventData[t][e.runtime.device + ".option.carousel_crop.width_item"]) || 0;
                l > n.clientWidth && (l = n.clientWidth);
                var s = Math.ceil(o / l);
                i ? a ? r >= s - 1 ? (r = s - 2,
                a = !1) : r++ : r <= 0 ? (r = 1,
                a = !0) : r-- : a ? r++ : r--,
                r < 0 && (r = 0),
                r >= s - 1 && (r = s - 1);
                var c = 1e3 * (parseFloat(getComputedStyle(n.getElementsByClassName("ladi-carousel-content")[0]).transitionDuration) || 0);
                e.runtime.timenext_carousel[t] = Date.now() + c;
                var d = e.getElementBoundingClientRect(n)
                  , u = d.x + r * l - d.x - (n.clientWidth - l) / 2;
                u = -u > 0 ? 0 : -u;
                var p = -(o - n.clientWidth);
                u < p && (u = p),
                n.getElementsByClassName("ladi-carousel-content")[0].style.setProperty("left", u + "px"),
                n.setAttribute("data-is-next", a),
                n.setAttribute("data-current", r)
            }
        }
    };
    l.forEach(function(i) {
        var n = e.runtime.eventData[i]
          , a = LadiPageApp[n.type + e.const.APP_RUNTIME_PREFIX];
        e.isEmpty(a) ? (!function(t, i, n) {
            var a = document.getElementById(t);
            e.isEmpty(a) || (e.isEmpty(n) ? a.addEventListener("click", function(i) {
                e.runEventTracking(t, !1)
            }) : (n.type == e.const.DATA_ACTION_TYPE.link && a.addEventListener("click", function(i) {
                if ("true" == a.getAttribute("data-action") && !e.isEmpty(n.action)) {
                    var r = n.action
                      , o = e.createTmpElement("a", "", {
                        href: r,
                        target: n.target
                    });
                    r = e.getLinkUTMRedirect(o.href, window.location.search),
                    r = e.convertDataReplaceStr(r),
                    o.href = r,
                    o.click()
                }
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.email && a.addEventListener("click", function(i) {
                "true" != a.getAttribute("data-action") || e.isEmpty(n.action) || e.createTmpElement("a", "", {
                    href: "mailto:" + n.action
                }).click(),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.phone && a.addEventListener("click", function(i) {
                "true" != a.getAttribute("data-action") || e.isEmpty(n.action) || e.createTmpElement("a", "", {
                    href: "tel:" + n.action
                }).click(),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.section && a.addEventListener("click", function(i) {
                var r = document.getElementById(n.action);
                if (!e.isEmpty(r)) {
                    var o = e.findAncestor(a, "ladi-popup");
                    if (!e.isEmpty(o)) {
                        var l = e.findAncestor(o, "ladi-element");
                        l.hasAttribute("data-popup-backdrop") && window.ladi(l.id).hide()
                    }
                    window.ladi(n.action).scroll(),
                    e.runEventTracking(t, !1)
                }
            }),
            n.type == e.const.DATA_ACTION_TYPE.popup && a.addEventListener("click", function(i) {
                var a = document.getElementById(n.action);
                e.isEmpty(a) || window.ladi(n.action).show(),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.hidden_show && a.addEventListener("click", function(i) {
                e.isArray(n.hidden_ids) && n.hidden_ids.forEach(function(t) {
                    window.ladi(t).hide()
                }),
                e.isArray(n.show_ids) && n.show_ids.forEach(function(t) {
                    window.ladi(t).show()
                }),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.lightbox_image && a.addEventListener("click", function(i) {
                lightbox_image(n.image_url),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.lightbox_video && a.addEventListener("click", function(i) {
                lightbox_video(n.video_url, n.video_type),
                e.runEventTracking(t, !1)
            }),
            n.type == e.const.DATA_ACTION_TYPE.lightbox_iframe && a.addEventListener("click", function(i) {
                lightbox_iframe(n.iframe_url),
                e.runEventTracking(t, !1)
            })))
        }(i, n.type, n["option.data_action"]),
        function(t, i, n, a, r, l) {
            if ("video" == i && !e.isEmpty(n)) {
                var s = document.getElementById(t);
                if (!e.isEmpty(s)) {
                    var c = function() {
                        var i = document.getElementById(t + "_player");
                        if (e.isEmpty(i)) {
                            var r = e.getVideoId(a, n)
                              , o = s.getElementsByClassName("ladi-video")[0];
                            e.stopAllVideo(),
                            a == e.const.VIDEO_TYPE.youtube && (o.innerHTML = o.innerHTML + '<iframe id="' + t + '_player" class="iframe-video-play" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" src="https://www.youtube.com/embed/' + r + '?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
                        }
                    };
                    (o && r || !o && l) && c(),
                    s.addEventListener("click", function(t) {
                        t.stopPropagation(),
                        c()
                    })
                }
            }
        }(i, n.type, n["option.video_value"], n["option.video_type"], n[e.const.DESKTOP + ".option.video_autoplay"], n[e.const.MOBILE + ".option.video_autoplay"]),
        function(t, i, n, a) {
            "popup" == i && n && ((e.isEmpty(a) || a < 0) && (a = 0),
            e.runTimeout(function() {
                window.ladi(t).show()
            }, 1e3 * a))
        }(i, n.type, n["option.show_popup_welcome_page"], n["option.delay_popup_welcome_page"]),
        function(t, i, n, a, r, o, l) {
            if ("countdown" == i && !e.isEmpty(n)) {
                var s = document.getElementById(t);
                e.isEmpty(s) || (s.setAttribute("data-type", n),
                n != e.const.COUNTDOWN_TYPE.countdown || e.isEmpty(a) || s.setAttribute("data-minute", a),
                n != e.const.COUNTDOWN_TYPE.endtime || e.isEmpty(l) || s.setAttribute("data-endtime", l),
                n != e.const.COUNTDOWN_TYPE.daily || e.isEmpty(r) || e.isEmpty(o) || (s.setAttribute("data-daily-start", r),
                s.setAttribute("data-daily-end", o)))
            }
        }(i, n.type, n["option.countdown_type"], n["option.countdown_minute"], n["option.countdown_daily_start"], n["option.countdown_daily_end"], n["option.countdown_endtime"]),
        function(t, i, n) {
            if ("countdown_item" == i && !e.isEmpty(n)) {
                var a = document.getElementById(t);
                e.isEmpty(a) || a.setAttribute("data-item-type", n)
            }
        }(i, n.type, n["option.countdown_item_type"]),
        function(t, i, n, a) {
            if ("section" == i) {
                var r = document.getElementById(t);
                if (!e.isEmpty(r)) {
                    var l = r.getElementsByClassName("ladi-section-arrow-down")[0];
                    if (e.isEmpty(l)) {
                        if (o) {
                            if (e.isEmpty(n))
                                return void r.removeAttribute("data-opacity");
                            var s = (parseFloat(n) || 0) + 50;
                            if (s > r.clientHeight)
                                return void r.removeAttribute("data-opacity");
                            r.style.setProperty("height", s + "px"),
                            r.classList.add("overflow-hidden")
                        } else {
                            if (e.isEmpty(a))
                                return void r.removeAttribute("data-opacity");
                            var c = (parseFloat(a) || 0) + 50;
                            if (c > r.clientHeight)
                                return void r.removeAttribute("data-opacity");
                            r.style.setProperty("height", c + "px"),
                            r.classList.add("overflow-hidden")
                        }
                        (l = document.createElement("div")).className = "ladi-section-arrow-down",
                        r.appendChild(l),
                        r.removeAttribute("data-opacity"),
                        l.addEventListener("click", function(t) {
                            t.stopPropagation(),
                            r.classList.add("ladi-section-readmore"),
                            r.style.removeProperty("height"),
                            r.classList.remove("overflow-hidden"),
                            l.parentElement.removeChild(l),
                            e.runTimeout(function() {
                                r.classList.remove("ladi-section-readmore")
                            }, 1e3 * parseFloat(getComputedStyle(r).transitionDuration))
                        })
                    }
                }
            }
        }(i, n.type, n[e.const.DESKTOP + ".option.readmore_range"], n[e.const.MOBILE + ".option.readmore_range"]),
        function(t, i, n) {
            if ("form_item" == i) {
                var a = null;
                if (n == e.const.INPUT_TYPE.select || n == e.const.INPUT_TYPE.select_multiple)
                    for (var r = document.getElementById(t).getElementsByClassName("ladi-form-control"), o = 0; o < r.length; o++)
                        r[o].addEventListener("change", function(t) {
                            t.target.setAttribute("data-selected", t.target.value)
                        });
                if (n == e.const.INPUT_TYPE.checkbox) {
                    a = document.getElementById(t).getElementsByClassName("ladi-form-checkbox-item");
                    for (var l = function(t) {
                        t.stopPropagation();
                        var i = e.findAncestor(t.target, "ladi-form-checkbox-item");
                        e.isEmpty(i) || i.getElementsByTagName("span")[0].setAttribute("data-checked", t.target.checked)
                    }, s = function(t) {
                        t.stopPropagation();
                        var i = e.findAncestor(t.target, "ladi-form-checkbox-item");
                        e.isEmpty(i) || i.getElementsByTagName("input")[0].click()
                    }, c = 0; c < a.length; c++) {
                        var d = a[c].getElementsByTagName("input")[0];
                        a[c].getElementsByTagName("span")[0].addEventListener("click", s),
                        d.addEventListener("change", l)
                    }
                }
                if (n == e.const.INPUT_TYPE.radio) {
                    a = document.getElementById(t).getElementsByClassName("ladi-form-checkbox-item");
                    for (var u = function(t) {
                        var i = e.findAncestor(t.target, "ladi-form-checkbox-item")
                          , n = e.findAncestor(i, "ladi-form-checkbox");
                        if (!e.isEmpty(n)) {
                            for (var a = n.querySelectorAll(".ladi-form-checkbox-item span"), r = 0; r < a.length; r++)
                                a[r].setAttribute("data-checked", !1);
                            e.isEmpty(i) || i.getElementsByTagName("span")[0].setAttribute("data-checked", t.target.checked)
                        }
                    }, p = function(t) {
                        t.stopPropagation();
                        var i = e.findAncestor(t.target, "ladi-form-checkbox-item");
                        e.isEmpty(i) || i.getElementsByTagName("input")[0].click()
                    }, m = 0; m < a.length; m++) {
                        var g = a[m].getElementsByTagName("input")[0];
                        a[m].getElementsByTagName("span")[0].addEventListener("click", p),
                        g.addEventListener("change", u)
                    }
                }
            }
        }(i, n.type, n["option.input_type"]),
        u(i, n.type),
        e.startAutoScroll(i, n.type, n[e.const.DESKTOP + ".option.auto_scroll"], n[e.const.MOBILE + ".option.auto_scroll"])) : a(n, t).run(i, o)
    }),
    function() {
        if (e.runtime.isClient && !e.runtime.isDesktop && !e.isEmpty(e.runtime.bodyFontSize)) {
            var t = (parseFloat(getComputedStyle(document.body).fontSize) || 0) / e.runtime.bodyFontSize;
            if (1 != t)
                for (var i = document.querySelectorAll(".ladi-paragraph, .ladi-list-paragraph, .ladi-headline, .ladi-countdown, .ladi-form"), n = 0; n < i.length; n++) {
                    var a = (parseFloat(getComputedStyle(i[n]).fontSize) || 0) / (t * t);
                    i[n].style.setProperty("font-size", a + "px")
                }
        }
    }(),
    function() {
        var i = document.getElementsByClassName("ladi-form")
          , n = null
          , a = null
          , r = null
          , o = null
          , l = null
          , c = null
          , d = ["utm_source", "utm_medium", "utm_campaign", "utm_term", "utm_content"]
          , u = ["name", "email", "phone", "address", "ward", "district", "state", "country"]
          , p = ["ward", "district", "state", "country"]
          , m = e.copy(p).reverse()
          , g = function(t) {
            var i = [];
            if (o.forEach(function(t) {
                e.isEmpty(n[t]) && i.push(t)
            }),
            i.length > 0)
                return e.showMessage(e.const.LANG.FORM_INPUT_REQUIRED_ERROR, null, function() {
                    var n = t.querySelector('[name="' + i[0] + '"]');
                    e.isEmpty(n) || n.focus()
                }),
                !1;
            var a = !0
              , r = 0
              , s = function() {
                var i = t.querySelector('[name="' + l[r].name + '"]');
                e.isEmpty(i) || i.focus()
            };
            for (r = 0; r < l.length; r++) {
                var c = n[l[r].name];
                if (!e.isEmpty(c))
                    try {
                        if (!new RegExp("^" + l[r].pattern + "$",l[r].pattern_flag).test(c)) {
                            e.showMessage(l[r].title, null, s),
                            a = !1;
                            break
                        }
                    } catch (t) {}
            }
            return a
        }
          , y = function(t) {
            n = {};
            for (var i = t.querySelectorAll(".ladi-element > .ladi-form-item-container [name]"), r = {}, s = null, c = 0; c < i.length; c++)
                s = i[c].getAttribute("name"),
                r[s] = parseInt(i[c].getAttribute("tabindex")) || 0;
            var d = Object.keys(r).sort(function(t, e) {
                return r[t] - r[e]
            });
            if (d.only(p).length == p.length)
                for (var g = 0; g < d.length; g++) {
                    var y = p.indexOf(d[g]);
                    -1 != y && (d[g] = m[y])
                }
            for (var h = 0; h < d.length; h++)
                n[d[h]] = "";
            a = Object.keys(n);
            for (var f = 0; f < i.length; f++) {
                if (s = i[f].getAttribute("name"),
                i[f].required && -1 == o.indexOf(s) && o.push(s),
                "INPUT" == i[f].tagName) {
                    var _ = i[f].getAttribute("type").trim().toLowerCase()
                      , v = i[f].getAttribute("pattern")
                      , E = i[f].getAttribute("title");
                    if ("email" == _ ? l.push({
                        name: s,
                        pattern: '(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))',
                        pattern_flag: "gi",
                        title: e.const.LANG.FORM_INPUT_EMAIL_REGEX
                    }) : e.isEmpty(v) || l.push({
                        name: s,
                        pattern: v,
                        title: e.isEmpty(E) ? e.const.LANG.FORM_INPUT_TEXT_REGEX : E
                    }),
                    "checkbox" == _) {
                        e.isArray(n[s]) || (n[s] = []),
                        i[f].checked && n[s].push(i[f].value);
                        continue
                    }
                    if ("radio" == _) {
                        i[f].checked && (n[s] = i[f].value);
                        continue
                    }
                }
                n[s] = i[f].value
            }
            u.forEach(function(t) {
                e.isNull(n[t]) || (e.setCookie(null, "_ladipage_" + t, n[t], 365),
                e.isArray(e.runtime.DOMAIN_SET_COOKIE) && e.runtime.DOMAIN_SET_COOKIE.forEach(function(i) {
                    i != window.location.host && e.setCookie(i, "_ladipage_" + t, n[t], 365)
                }))
            })
        }
          , h = function(t) {
            var i = {
                form_config_id: r,
                ladipage_id: e.runtime.ladipage_id,
                tracking_form: [],
                form_data: []
            };
            e.isEmpty(e.runtime.time_zone) || (i.time_zone = e.runtime.time_zone),
            a.forEach(function(t) {
                var a = n[t];
                e.isArray(a) && 0 == a.length && (a = ""),
                i.form_data.push({
                    name: t,
                    value: a
                })
            }),
            i.tracking_form.push({
                name: "url_page",
                value: window.location.href
            }),
            d.forEach(function(t) {
                var n = c[t];
                n = e.isNull(n) ? "" : n,
                i.tracking_form.push({
                    name: t,
                    value: n
                })
            }),
            e.isFunction(t) && t(i)
        }
          , f = function(t) {
            t.reset();
            for (var i = t.querySelectorAll(".ladi-element > .ladi-form-item-container .ladi-form-checkbox-item input"), n = 0; n < i.length; n++) {
                var a = e.findAncestor(i[n], "ladi-form-checkbox-item").querySelector("span");
                e.isEmpty(a) || a.setAttribute("data-checked", i[n].checked)
            }
        }
          , _ = function(t) {
            y(t),
            g(t) && (h(function(t) {
                e.sendRequest("POST", e.const.API_FORM_DATA, JSON.stringify(t), !0, {
                    "Content-Type": "application/json"
                })
            }),
            e.showMessage(e.const.LANG.FORM_SEND_DATA_NO_CONFIG),
            f(t))
        }
          , v = function(i) {
            c = e.getURLSearchParams(window.location.search),
            n = {},
            a = [],
            r = null,
            o = [],
            l = [];
            var u = i.getElementsByClassName("ladi-form")[0];
            if (!e.isEmpty(u)) {
                var p = e.runtime.eventData[i.id];
                if (e.isEmpty(p))
                    _(u);
                else if (e.isNull(e.runtime.tmp.form_sending) && (e.runtime.tmp.form_sending = {}),
                e.isNull(e.runtime.tmp.form_button_headline) && (e.runtime.tmp.form_button_headline = {}),
                !e.runtime.tmp.form_sending[i.id]) {
                    r = p["option.form_config_id"];
                    var m = p["option.form_send_ladipage"]
                      , v = p["option.form_api_data"]
                      , E = p["option.thankyou_type"]
                      , w = p["option.thankyou_value"];
                    if (e.isEmpty(r))
                        _(u);
                    else if (y(u),
                    g(u)) {
                        var L = 0
                          , S = 0
                          , P = []
                          , b = !1
                          , A = !1
                          , C = !0
                          , T = function(t) {
                            t && f(u),
                            delete e.runtime.tmp.form_sending[i.id],
                            u.querySelector(".ladi-button > .ladi-element > .ladi-headline").innerHTML = e.runtime.tmp.form_button_headline[i.id]
                        }
                          , x = function() {
                            var a, r, o, l;
                            a = function() {
                                var a = e.findAncestor(i, "ladi-popup");
                                if (!e.isEmpty(a)) {
                                    var r = e.findAncestor(a, "ladi-element").id;
                                    e.runRemovePopup(r, t)
                                }
                                var o = p["option.form_auto_funnel"];
                                E == e.const.FORM_THANKYOU_TYPE.default && (w = e.isEmpty(w) ? e.const.LANG.FORM_THANKYOU_MESSAGE_DEFAULT : w,
                                e.showMessage(w, n)),
                                E == e.const.FORM_THANKYOU_TYPE.popup && (o && e.setDataReplaceElement(!1, !1, n, w),
                                window.ladi(w).show()),
                                E == e.const.FORM_THANKYOU_TYPE.url && window.ladi(w).open_url("_top", o ? n : null)
                            }
                            ,
                            r = null,
                            o = !1,
                            l = function() {
                                o || (e.isFunction(a) && a(),
                                e.removeTimeout(r),
                                o = !0)
                            }
                            ,
                            e.isEmpty(s[e.const.TRACKING_NAME]) ? l() : (r = e.runTimeout(l, 3e3),
                            e.sendRequest("POST", e.const.API_CHECKING_FORM, JSON.stringify(s), !0, {
                                "Content-Type": "application/json"
                            }, function(t, e, i) {
                                i.readyState == XMLHttpRequest.DONE && l()
                            }))
                        }
                          , N = function(t, n, a, r) {
                            if (a.readyState == XMLHttpRequest.DONE) {
                                if (r == e.const.API_FORM_DATA) {
                                    var o = {};
                                    try {
                                        o = JSON.parse(t)
                                    } catch (t) {}
                                    200 == o.code ? L++ : (S++,
                                    C = !1)
                                } else
                                    200 == n || 201 == n ? L++ : e.getElementAHref(r).host == e.const.DOMAIN_GOOGLE_DOCS ? L++ : S++;
                                L + S == P.length && (C && !b && L >= 1 ? (b = !0,
                                e.runEventTracking(i.id, !0),
                                x(),
                                T(!0)) : !A && S >= 1 && (A = !0,
                                e.showMessage(e.const.LANG.FORM_SEND_DATA_ERROR),
                                T(!1)))
                            }
                        }
                          , k = function(t) {
                            P.push({
                                url: e.const.API_FORM_DATA,
                                data: JSON.stringify(t),
                                async: !0,
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                callback: N
                            })
                        };
                        m && h(k),
                        e.isArray(v) && v.forEach(function(t) {
                            if (!e.isEmpty(t.api_url) && e.isArray(t.custom_fields) && 0 != t.custom_fields.length) {
                                var i = e.getElementAHref(t.api_url).host == e.const.DOMAIN_GOOGLE_DOCS
                                  , a = {};
                                t.custom_fields.forEach(function(t) {
                                    var i = n[t.ladi_name];
                                    e.isNull(i) || (e.isArray(i) ? 0 == i.length ? a[t.name] = "" : a[t.name] = JSON.stringify(i) : a[t.name] = i)
                                }),
                                i || (a.link = window.location.href,
                                d.forEach(function(t) {
                                    var i = c[t];
                                    e.isNull(i) || (a[t] = i)
                                }));
                                var r = Object.keys(a).reduce(function(t, e) {
                                    return t.push(e + "=" + encodeURIComponent(a[e])),
                                    t
                                }, []).join("&");
                                P.push({
                                    url: t.api_url,
                                    data: r,
                                    async: !0,
                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    },
                                    callback: N
                                })
                            }
                        });
                        var I = function() {
                            e.runtime.tmp.form_sending[i.id] = !0;
                            var t = u.querySelector(".ladi-button > .ladi-element > .ladi-headline");
                            e.isNull(e.runtime.tmp.form_button_headline[i.id]) && (e.runtime.tmp.form_button_headline[i.id] = t.innerHTML),
                            t.innerHTML = "● ● ●"
                        };
                        P.length > 0 ? I() : m ? _(u) : (I(),
                        h(k)),
                        P.forEach(function(t) {
                            e.sendRequest("POST", t.url, t.data, t.async, t.headers, t.callback)
                        })
                    }
                }
            }
        }
          , E = function(t) {
            var i = e.findAncestor(t.target, "ladi-element");
            if (!e.isEmpty(i))
                for (var n = i.querySelectorAll('[type="checkbox"]'), a = 0; a < n.length; a++)
                    n[a].removeAttribute("required")
        }
          , w = function(t) {
            var i = e.findAncestor(t.target, "ladi-element");
            if (!e.isEmpty(i)) {
                for (var n = i.querySelectorAll('[ladi-checkbox-required="true"]'), a = -1, r = 0; r < n.length; r++)
                    if (0 == n[r].querySelectorAll('[type="checkbox"]:checked').length) {
                        a = r;
                        break
                    }
                if (-1 == a)
                    v(i);
                else {
                    var o = n[a].querySelectorAll('[type="checkbox"]');
                    if (o.length > 0) {
                        o[0].setAttribute("required", "required");
                        for (var l = 0; l < o.length; l++)
                            o[l].removeEventListener("change", E),
                            o[l].addEventListener("change", E);
                        i.querySelector(".ladi-form").reportValidity()
                    }
                }
            }
            return !1
        }
          , L = function(t) {
            var i = e.findAncestor(t.target, "ladi-form");
            if (!e.isEmpty(i)) {
                var n = i.querySelectorAll('[type="submit"]')[0];
                e.isEmpty(n) || n.click()
            }
        }
          , S = {};
        u.forEach(function(t) {
            S[t] = e.getCookie("_ladipage_" + t)
        });
        var P = 0
          , b = !1
          , A = !1
          , C = function(t) {
            for (var n = i[P].querySelectorAll('.ladi-element > .ladi-form-item-container [name="' + t + '"]'), a = 0; a < n.length; a++)
                if (!e.isEmpty(S[t]))
                    if ("SELECT" == n[a].tagName)
                        n[a].querySelectorAll('option[value="' + S[t] + '"]').length > 0 && (n[a].value = S[t],
                        b && n[a].dispatchEvent(new Event("change")));
                    else {
                        if (A) {
                            var r = S[t].split(":");
                            n[a].value = 2 == r.length ? r[1] : r[0]
                        } else
                            n[a].value = S[t];
                        b && n[a].dispatchEvent(new Event("change"))
                    }
        };
        for (P = 0; P < i.length; P++) {
            i[P].onsubmit = w;
            var T = i[P].getElementsByClassName("ladi-button")[0];
            e.isEmpty(T) || e.findAncestor(T, "ladi-element").addEventListener("click", L);
            for (var x = i[P].querySelectorAll("[tabindex]"), N = 0, k = 0; k < x.length; k++) {
                var I = parseInt(x[k].getAttribute("tabindex")) || 0;
                I > N && (N = I),
                x[k].setAttribute("tabindex", e.runtime.tabindexForm + I)
            }
            e.runtime.tabindexForm += N
        }
        var B = function(t, n, a) {
            for (b = n,
            A = a,
            P = 0; P < i.length; P++) {
                var r = e.findAncestor(i[P], "ladi-element");
                !e.isEmpty(e.runtime.eventData[r.id]) && e.runtime.eventData[r.id]["option.form_auto_complete"] && t.forEach(C)
            }
        };
        B(e.copy(u).except(p));
        e.runtime.tmp.runAfterLocation.push(function() {
            var t = ""
              , n = ""
              , a = ""
              , r = ""
              , o = function(e) {
                var i = window.LadiLocation[n].data[e];
                t += '<option value="' + i.id + ":" + i.name + '">' + i.name + "</option>"
            }
              , l = function(t) {
                var i = window.LadiLocation[t.target.getAttribute("data-country")].data[t.target.value.split(":")[0]];
                a = "",
                e.isEmpty(i) || Object.keys(i.data).forEach(function(t) {
                    var e = i.data[t];
                    a += '<option value="' + e.id + ":" + e.name + '">' + e.name + "</option>"
                });
                var n = e.findAncestor(t.target, "ladi-element");
                if (!e.isEmpty(n)) {
                    var r = n.querySelector('select[name="district"]');
                    e.isEmpty(r) || (r.setAttribute("data-selected", ""),
                    r.innerHTML = r.querySelector("option").outerHTML + a);
                    var o = n.querySelector('select[name="ward"]');
                    e.isEmpty(o) || (o.setAttribute("data-selected", ""),
                    o.innerHTML = o.querySelector("option").outerHTML)
                }
            }
              , s = function(t) {
                var i = e.findAncestor(t.target, "ladi-element");
                if (!e.isEmpty(i)) {
                    var n = i.querySelector('select[name="ward"]');
                    if (!e.isEmpty(n)) {
                        n.setAttribute("data-selected", ""),
                        r = "";
                        var a = i.querySelector('select[name="state"]');
                        if (!e.isEmpty(a)) {
                            var o = a.getAttribute("data-selected");
                            if (!e.isEmpty(o)) {
                                o = o.split(":")[0];
                                var l = window.LadiLocation[a.getAttribute("data-country")].data[o];
                                if (!e.isEmpty(l)) {
                                    var s = l.data[t.target.value.split(":")[0]];
                                    e.isEmpty(s) || Object.keys(s.data).forEach(function(t) {
                                        var e = s.data[t];
                                        r += '<option value="' + e.id + ":" + e.name + '">' + e.name + "</option>"
                                    })
                                }
                            }
                        }
                        n.innerHTML = n.querySelector("option").outerHTML + r
                    }
                }
            };
            for (P = 0; P < i.length; P++) {
                var c = i[P].querySelectorAll('.ladi-element > .ladi-form-item-container [name="state"]')
                  , d = 0
                  , u = null;
                for (d = 0; d < c.length; d++)
                    if (u = e.findAncestor(c[d], "ladi-element"),
                    !e.isEmpty(u) && (n = e.runtime.eventData[u.id]["option.input_country"],
                    !e.isEmpty(n) && (t = "",
                    n = n.split(":")[0],
                    !e.isEmpty(window.LadiLocation[n])))) {
                        var p = window.LadiLocation[n].data;
                        Object.keys(p).forEach(o),
                        c[d].innerHTML = c[d].querySelector("option").outerHTML + t,
                        c[d].setAttribute("data-country", n),
                        c[d].addEventListener("change", l)
                    }
                var m = i[P].querySelectorAll('.ladi-element > .ladi-form-item-container [name="district"]');
                for (d = 0; d < m.length; d++)
                    m[d].addEventListener("change", s)
            }
        }),
        e.runtime.tmp.runAfterLocation.push(function() {
            B(m, !0, !0)
        })
    }(),
    (c = function() {
        l.forEach(function(t) {
            if ("countdown" == e.runtime.eventData[t].type) {
                var i = document.getElementById(t);
                if (!e.isEmpty(i)) {
                    var n = i.getAttribute("data-type")
                      , a = 0
                      , r = 0
                      , o = Date.now();
                    if (i.hasAttribute("data-date-start") || i.hasAttribute("data-date-end"))
                        a = parseFloat(i.getAttribute("data-date-start")) || 0,
                        r = parseFloat(i.getAttribute("data-date-end")) || 0;
                    else {
                        if (n == e.const.COUNTDOWN_TYPE.countdown) {
                            var l = parseInt(i.getAttribute("data-minute")) || 0;
                            if (l <= 0)
                                return;
                            for (r = e.runtime.timenow; r <= o; )
                                r += 60 * l * 1e3
                        }
                        if (n == e.const.COUNTDOWN_TYPE.endtime && (r = parseInt(i.getAttribute("data-endtime")) || 0),
                        n == e.const.COUNTDOWN_TYPE.daily) {
                            var s = i.getAttribute("data-daily-start")
                              , c = i.getAttribute("data-daily-end");
                            if (!e.isEmpty(s) && !e.isEmpty(c)) {
                                var d = (new Date).toDateString();
                                a = new Date(d + " " + s).getTime(),
                                r = new Date(d + " " + c).getTime()
                            }
                        }
                        i.setAttribute("data-date-start", a),
                        i.setAttribute("data-date-end", r)
                    }
                    if (!(a > o)) {
                        var u = r - o;
                        u < 0 && (u = 0);
                        for (var p = e.getCountdownTime(u), m = i.querySelectorAll("[data-item-type]"), g = 0; g < m.length; g++)
                            m[g].querySelectorAll(".ladi-countdown-text span")[0].textContent = p[m[g].getAttribute("data-item-type")]
                    }
                }
            }
        })
    }
    )(),
    e.runtime.interval_countdown = e.runInterval(c, 1e3),
    l.forEach(function(t) {
        var i = e.runtime.eventData[t];
        if ("gallery" == i.type) {
            var n = document.getElementById(t);
            if (!e.isEmpty(n)) {
                var a = i[e.runtime.device + ".option.gallery_control.autoplay"]
                  , r = i[e.runtime.device + ".option.gallery_control.autoplay_time"]
                  , o = 0;
                a && !e.isEmpty(r) && (o = r);
                var l = function(e) {
                    e.stopPropagation();
                    var i = parseFloat(e.target.getAttribute("data-index")) || 0
                      , a = null
                      , r = null;
                    (parseFloat(n.getAttribute("data-current")) || 0) > i ? (a = "prev",
                    r = "right") : (a = "next",
                    r = "left");
                    var l = n.getAttribute("data-is-next") || "true";
                    (l = "true" == l.toLowerCase()) ? i-- : i++,
                    n.setAttribute("data-current", i),
                    n.setAttribute("data-next-time", Date.now() + 1e3 * o),
                    d(t, !1, a, r)
                }
                  , s = function(i) {
                    i.stopPropagation(),
                    i = e.getEventCursorData(i),
                    e.isEmpty(e.runtime.timeout_gallery[t]) && (e.runtime.current_element_mouse_down_gallery_view = t,
                    e.runtime.current_element_mouse_down_gallery_view_position_x = i.pageX)
                }
                  , c = function(i) {
                    i.stopPropagation(),
                    i = e.getEventCursorData(i),
                    (n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-top") || n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-bottom")) && (e.runtime.current_element_mouse_down_gallery_control = t,
                    e.runtime.current_element_mouse_down_gallery_control_time = Date.now(),
                    e.runtime.current_element_mouse_down_gallery_control_position_x = i.pageX,
                    n.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("transition-duration", "0ms"),
                    n.getElementsByClassName("ladi-gallery-control-box")[0].setAttribute("data-left", getComputedStyle(n.getElementsByClassName("ladi-gallery-control-box")[0]).left))
                };
                n.getElementsByClassName("ladi-gallery-view-arrow-left")[0].addEventListener("click", function(e) {
                    e.stopPropagation(),
                    n.setAttribute("data-is-next", !1),
                    n.setAttribute("data-next-time", Date.now() + 1e3 * o),
                    d(t, !1)
                }),
                n.getElementsByClassName("ladi-gallery-view-arrow-right")[0].addEventListener("click", function(e) {
                    e.stopPropagation(),
                    n.setAttribute("data-is-next", !0),
                    n.setAttribute("data-next-time", Date.now() + 1e3 * o),
                    d(t, !1)
                }),
                n.getElementsByClassName("ladi-gallery-control-arrow-left")[0].addEventListener("click", function(t) {
                    t.stopPropagation();
                    var i = n.getElementsByClassName("ladi-gallery-control-item")[0];
                    if (!e.isEmpty(i)) {
                        var a = getComputedStyle(i);
                        if (n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-top") || n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-bottom")) {
                            var r = (parseFloat(a.width) || 0) + (parseFloat(a.marginRight) || 0);
                            (r += parseFloat(n.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("left")) || 0) > 0 && (r = 0),
                            n.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("left", r + "px")
                        } else {
                            var l = (parseFloat(a.height) || 0) + (parseFloat(a.marginBottom) || 0);
                            (l += parseFloat(n.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("top")) || 0) > 0 && (l = 0),
                            n.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("top", l + "px")
                        }
                        n.setAttribute("data-next-time", Date.now() + 1e3 * o)
                    }
                }),
                n.getElementsByClassName("ladi-gallery-control-arrow-right")[0].addEventListener("click", function(t) {
                    t.stopPropagation();
                    var i = n.getElementsByClassName("ladi-gallery-control-item")[0];
                    if (!e.isEmpty(i)) {
                        var a = getComputedStyle(i);
                        if (n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-top") || n.getElementsByClassName("ladi-gallery")[0].classList.contains("ladi-gallery-bottom")) {
                            var r = (parseFloat(a.width) || 0) + (parseFloat(a.marginRight) || 0);
                            r = -r + (parseFloat(n.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("left")) || 0);
                            var l = parseFloat(getComputedStyle(n.getElementsByClassName("ladi-gallery-control-box")[0]).width) || 0;
                            r < (l = (l = -(l -= parseFloat(getComputedStyle(n.getElementsByClassName("ladi-gallery-control")[0]).width) || 0)) > 0 ? 0 : l) && (r = l),
                            n.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("left", r + "px")
                        } else {
                            var s = (parseFloat(a.height) || 0) + (parseFloat(a.marginBottom) || 0);
                            s = -s + (parseFloat(n.getElementsByClassName("ladi-gallery-control-box")[0].style.getPropertyValue("top")) || 0);
                            var c = parseFloat(getComputedStyle(n.getElementsByClassName("ladi-gallery-control-box")[0]).height) || 0;
                            s < (c = (c = -(c -= parseFloat(getComputedStyle(n.getElementsByClassName("ladi-gallery-control")[0]).height) || 0)) > 0 ? 0 : c) && (s = c),
                            n.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("top", s + "px")
                        }
                        n.setAttribute("data-next-time", Date.now() + 1e3 * o)
                    }
                }),
                n.getElementsByClassName("ladi-gallery-view")[0].addEventListener("mousedown", s),
                n.getElementsByClassName("ladi-gallery-view")[0].addEventListener("touchstart", s),
                n.getElementsByClassName("ladi-gallery-control")[0].addEventListener("mousedown", c),
                n.getElementsByClassName("ladi-gallery-control")[0].addEventListener("touchstart", c);
                for (var u = n.getElementsByClassName("ladi-gallery-control-item"), p = 0; p < u.length; p++)
                    u[p].addEventListener("click", l)
            }
        }
    }),
    e.runtime.interval_gallery = e.runInterval(function() {
        l.forEach(function(t) {
            var i = e.runtime.eventData[t];
            if ("gallery" == i.type) {
                var n = document.getElementById(t);
                if (!e.isEmpty(n)) {
                    var a = i[e.runtime.device + ".option.gallery_control.autoplay"]
                      , r = i[e.runtime.device + ".option.gallery_control.autoplay_time"]
                      , o = 0;
                    if (a && !e.isEmpty(r) && (o = r),
                    o > 0) {
                        var l = n.getAttribute("data-next-time")
                          , s = Date.now();
                        e.isEmpty(l) && (l = s + 1,
                        n.setAttribute("data-next-time", l)),
                        s >= l && (d(t, !0),
                        n.setAttribute("data-next-time", s + 1e3 * o))
                    }
                }
            }
        })
    }, 1e3),
    l.forEach(function(t) {
        var i = e.runtime.eventData[t];
        if ("carousel" == i.type) {
            var n = document.getElementById(t);
            if (!e.isEmpty(n)) {
                var a = i[e.runtime.device + ".option.carousel_setting.autoplay"]
                  , r = i[e.runtime.device + ".option.carousel_setting.autoplay_time"]
                  , o = 0;
                a && !e.isEmpty(r) && (o = r);
                var l = function(i) {
                    i.stopPropagation(),
                    i = e.getEventCursorData(i),
                    !e.isEmpty(e.runtime.timenext_carousel[t]) && e.runtime.timenext_carousel[t] > Date.now() || (e.runtime.timenext_carousel[t] = Date.now() + 864e5,
                    e.runtime.current_element_mouse_down_carousel = t,
                    e.runtime.current_element_mouse_down_carousel_position_x = i.pageX,
                    n.getElementsByClassName("ladi-carousel-content")[0].style.setProperty("transition-duration", "0ms"),
                    n.getElementsByClassName("ladi-carousel-content")[0].setAttribute("data-left", getComputedStyle(n.getElementsByClassName("ladi-carousel-content")[0]).left))
                };
                n.getElementsByClassName("ladi-carousel-arrow-left")[0].addEventListener("click", function(e) {
                    e.stopPropagation(),
                    n.getElementsByClassName("ladi-carousel-content")[0].style.removeProperty("transition-duration"),
                    n.setAttribute("data-is-next", !1),
                    n.setAttribute("data-next-time", Date.now() + 1e3 * o),
                    p(t, !1)
                }),
                n.getElementsByClassName("ladi-carousel-arrow-right")[0].addEventListener("click", function(e) {
                    e.stopPropagation(),
                    n.getElementsByClassName("ladi-carousel-content")[0].style.removeProperty("transition-duration"),
                    n.setAttribute("data-is-next", !0),
                    n.setAttribute("data-next-time", Date.now() + 1e3 * o),
                    p(t, !1)
                }),
                n.getElementsByClassName("ladi-carousel")[0].addEventListener("mousedown", l),
                n.getElementsByClassName("ladi-carousel")[0].addEventListener("touchstart", l)
            }
        }
    }),
    e.runtime.interval_carousel = e.runInterval(function() {
        l.forEach(function(t) {
            var i = e.runtime.eventData[t];
            if ("carousel" == i.type) {
                var n = document.getElementById(t);
                if (!e.isEmpty(n)) {
                    var a = i[e.runtime.device + ".option.carousel_setting.autoplay"]
                      , r = i[e.runtime.device + ".option.carousel_setting.autoplay_time"]
                      , o = 0;
                    if (a && !e.isEmpty(r) && (o = r),
                    o > 0) {
                        var l = n.getAttribute("data-next-time")
                          , s = Date.now();
                        e.isEmpty(l) && (l = s + 1,
                        n.setAttribute("data-next-time", l)),
                        s >= l && (p(t, !0),
                        n.setAttribute("data-next-time", s + 1e3 * o))
                    }
                }
            }
        })
    }, 1e3),
    function() {
        l.forEach(function(t) {
            var i = e.runtime.eventData[t];
            if (!e.isEmpty(i["option.data_action"])) {
                var n = document.getElementById(t);
                if (!e.isEmpty(n) && "true" != n.getAttribute("data-action") && i["option.data_action"].type == e.const.DATA_ACTION_TYPE.link) {
                    var a = e.getLinkUTMRedirect(n.href, window.location.search);
                    n.setAttribute("data-replace-href", a),
                    n.href = e.convertDataReplaceStr(a)
                }
            }
        });
        for (var t = document.querySelectorAll(".ladi-headline a[href], .ladi-paragraph a[href], .ladi-list-paragraph a[href]"), i = 0; i < t.length; i++) {
            var n = e.getLinkUTMRedirect(t[i].href, window.location.search);
            t[i].setAttribute("data-replace-href", n),
            t[i].href = e.convertDataReplaceStr(n)
        }
    }(),
    t && e.const.TIME_ONPAGE_TRACKING.forEach(function(t) {
        e.runTimeout(function() {
            e.isFunction(window.gtag) && window.gtag("event", "TimeOnPage_" + t + "_seconds", {
                event_category: "LadiPageTimeOnPage",
                event_label: window.location.host + window.location.pathname,
                non_interaction: !0
            }),
            e.isFunction(window.fbq) && window.fbq("trackCustom", "TimeOnPage_" + t + "_seconds")
        }, 1e3 * t)
    }),
    function() {
        for (var t = 2500, i = 3800, n = 800, a = 50, r = 150, o = 500, l = 1300, s = 600, c = 1500, d = t, u = function(i, n, a, r) {
            e.isEmpty(i) || (i.classList.remove("in"),
            i.classList.add("out"));
            var o = e.isEmpty(i) ? null : i.nextSibling;
            if (e.isEmpty(o) ? a && e.runTimeout(function() {
                h(m(n))
            }, t) : e.runTimeout(function() {
                u(o, n, a, r)
            }, r),
            e.isEmpty(o) && document.querySelectorAll("html")[0].classList.contains("no-csstransitions")) {
                var l = m(n);
                g(n, l)
            }
        }, p = function(i, n, a, r) {
            var o = n.parentElement
              , l = o.parentElement;
            l.classList.contains("ladipage-animated-headline") || (l = l.parentElement),
            e.isEmpty(i) || (i.classList.add("in"),
            i.classList.remove("out"));
            var s = e.isEmpty(i) ? null : i.nextSibling;
            e.isEmpty(s) ? ((l.classList.contains("rotate-2") || l.classList.contains("rotate-3") || l.classList.contains("scale")) && o.style.setProperty("width", n.clientWidth + "px"),
            e.isEmpty(e.findAncestor(n, "type")) || e.runTimeout(function() {
                var t = e.findAncestor(n, "ladipage-animated-words-wrapper");
                e.isEmpty(t) || t.classList.add("waiting")
            }, 200),
            a || e.runTimeout(function() {
                h(n)
            }, t)) : e.runTimeout(function() {
                p(s, n, a, r)
            }, r)
        }, m = function(t) {
            var i = t.nextSibling;
            return e.isEmpty(i) || i.classList.contains("after") ? t.parentElement.firstChild : i
        }, g = function(t, e) {
            t.classList.remove("is-visible"),
            t.classList.add("is-hidden"),
            e.classList.remove("is-hidden"),
            e.classList.add("is-visible")
        }, y = function(t, i) {
            e.isEmpty(e.findAncestor(t, "type")) ? e.isEmpty(e.findAncestor(t, "clip")) || (e.findAncestor(t, "ladipage-animated-words-wrapper").style.setProperty("width", t.clientWidth + 5 + "px"),
            e.runTimeout(function() {
                h(t)
            }, s + c)) : (p(t.querySelectorAll("i")[0], t, !1, i),
            t.classList.add("is-visible"),
            t.classList.remove("is-hidden"))
        }, h = function(c) {
            if (!e.isEmpty(c)) {
                var d = m(c);
                if (e.isEmpty(e.findAncestor(c, "type")))
                    if (e.isEmpty(e.findAncestor(c, "letters")))
                        e.isEmpty(e.findAncestor(c, "clip")) ? e.isEmpty(e.findAncestor(c, "loading-bar")) ? (g(c, d),
                        e.runTimeout(function() {
                            h(d)
                        }, t)) : (e.findAncestor(c, "ladipage-animated-words-wrapper").classList.remove("is-loading"),
                        g(c, d),
                        e.runTimeout(function() {
                            h(d)
                        }, i),
                        e.runTimeout(function() {
                            e.findAncestor(c, "ladipage-animated-words-wrapper").classList.add("is-loading")
                        }, n)) : (e.findAncestor(c, "ladipage-animated-words-wrapper").style.setProperty("width", "2px"),
                        e.runTimeout(function() {
                            g(c, d),
                            y(d)
                        }, s));
                    else {
                        var f = c.querySelectorAll("i").length >= d.querySelectorAll("i").length;
                        u(c.querySelectorAll("i")[0], c, f, a),
                        p(d.querySelectorAll("i")[0], d, f, a)
                    }
                else {
                    var _ = e.findAncestor(c, "ladipage-animated-words-wrapper");
                    _.classList.add("selected"),
                    _.classList.remove("waiting"),
                    e.runTimeout(function() {
                        _.classList.remove("selected"),
                        c.classList.remove("is-visible"),
                        c.classList.add("is-hidden");
                        for (var t = c.querySelectorAll("i"), e = 0; e < t.length; e++)
                            t[e].classList.remove("in"),
                            t[e].classList.add("out")
                    }, o),
                    e.runTimeout(function() {
                        y(d, r)
                    }, l)
                }
            }
        }, f = function(t) {
            var a = !1;
            if (e.const.ANIMATED_LIST.forEach(function(e) {
                t.classList.contains(e) && (a = !0)
            }),
            a) {
                var r = t.getElementsByClassName("ladipage-animated-words-wrapper")[0];
                if (!e.isEmpty(r)) {
                    var o = e.isEmpty(r.getAttribute("data-word")) ? [] : JSON.parse(r.getAttribute("data-word"));
                    if (0 != o.length) {
                        var l = r.textContent.trim();
                        if (r.textContent = "",
                        r.innerHTML = r.innerHTML + '<b class="is-visible">' + l + "</b>",
                        o.forEach(function(t) {
                            e.isEmpty(t) ? r.innerHTML = r.innerHTML + "<b>" + l + "</b>" : r.innerHTML = r.innerHTML + "<b>" + t.trim() + "</b>"
                        }),
                        !e.isEmpty(e.findAncestor(r, "type")) || !e.isEmpty(e.findAncestor(r, "loading-bar")) || !e.isEmpty(e.findAncestor(r, "clip"))) {
                            r.innerHTML = r.innerHTML + '<div class="after"></div>';
                            for (var s = getComputedStyle(r).color, c = r.getElementsByClassName("after"), u = 0; u < c.length; u++)
                                c[u].style.setProperty("background-color", s)
                        }
                        if (t.classList.contains("type") && r.classList.add("waiting"),
                        (t.classList.contains("type") || t.classList.contains("rotate-2") || t.classList.contains("rotate-3") || t.classList.contains("scale")) && t.classList.add("letters"),
                        function(t) {
                            for (var i = 0; i < t.length; i++) {
                                var n = t[i]
                                  , a = n.textContent.trim().split("")
                                  , r = n.classList.contains("is-visible");
                                for (var o in a) {
                                    " " == a[o] && (a[o] = "&nbsp;");
                                    var l = e.findAncestor(n, "rotate-2");
                                    e.isEmpty(l) || (a[o] = "<em>" + a[o] + "</em>"),
                                    a[o] = r ? '<i class="in">' + a[o] + "</i>" : "<i>" + a[o] + "</i>"
                                }
                                var s = a.join("");
                                n.innerHTML = s,
                                n.style.setProperty("opacity", 1)
                            }
                        }(document.querySelectorAll(".letters b")),
                        t.classList.contains("loading-bar"))
                            d = i,
                            e.runTimeout(function() {
                                r.classList.add("is-loading")
                            }, n);
                        else if (t.classList.contains("clip")) {
                            var p = r.clientWidth + 5;
                            r.style.setProperty("width", p + "px")
                        }
                        e.runTimeout(function() {
                            h(t.getElementsByClassName("is-visible")[0])
                        }, d)
                    }
                }
            }
        }, _ = document.getElementsByClassName("ladipage-animated-headline"), v = 0; v < _.length; v++)
            f(_[v])
    }(),
    document.addEventListener("mouseleave", e.runEventMouseLeave),
    document.addEventListener("mousemove", e.runEventMouseMove),
    document.addEventListener("touchmove", e.runEventMouseMove),
    document.addEventListener("mouseup", e.runEventMouseUp),
    document.addEventListener("touchend", e.runEventMouseUp),
    window.addEventListener("scroll", e.runEventScroll),
    window.addEventListener("resize", e.runEventResize),
    window.addEventListener("orientationchange", e.runEventOrientationChange),
    document.getElementById(e.runtime.backdrop_popup_id).addEventListener("click", e.runEventBackdropClick),
    e.reloadLazyload(),
    function() {
        if (t) {
            var i = function() {
                e.runtime.ladipage_powered_by_id = e.randomId();
                var t = '<a id="' + e.runtime.ladipage_powered_by_id + '" href="https://ladipage.vn/?utm_source=freemium" style=\'width: 200px; height: 40px; background: url("' + e.const.POWERED_BY_IMAGE + '") no-repeat; position: fixed; bottom: 0; right: 110px; margin: 0;padding: 0; z-index: 10000000000;\' target="_blank" rel="nofollow"></a>'
                  , n = document.createElement("a");
                if (document.body.insertBefore(n, document.body.childNodes[e.randomInt(0, document.body.childNodes.length)]),
                n.outerHTML = t,
                e.isEmpty(e.runtime.interval_powered_by)) {
                    var a = e.runInterval(function() {
                        var t = document.getElementById(e.runtime.ladipage_powered_by_id);
                        e.isEmpty(t) && i()
                    }, 5e3);
                    e.runtime.isClient || (e.runtime.interval_powered_by = a)
                }
            }
              , n = !1
              , a = e.isArray(e.runtime.DOMAIN_FREE) ? e.runtime.DOMAIN_FREE : []
              , r = window.location.href;
            ["/", ".", "/"].forEach(function(t) {
                for (; r.endsWith(t); )
                    r = r.substr(0, r.length - t.length)
            });
            var o = e.getElementAHref(r).host.toLowerCase();
            if (a.forEach(function(t) {
                n || (n = o.endsWith(t.toLowerCase()))
            }),
            n)
                i();
            else {
                var l = {
                    domain: window.location.host
                };
                e.isEmpty(s[e.const.TRACKING_NAME]) || (l[e.const.TRACKING_NAME] = s[e.const.TRACKING_NAME]),
                e.sendRequest("POST", e.const.API_CHECK_VERIFY, JSON.stringify(l), !0, {
                    "Content-Type": "application/json"
                }, function(t, e, n) {
                    n.readyState == XMLHttpRequest.DONE && 200 == e && 0 == JSON.parse(t).data && i()
                })
            }
        }
    }(),
    e.setDataReplaceStart(),
    t || e.runAfterLocation(),
    "complete" === document.readyState || "loading" !== document.readyState && !document.documentElement.doScroll ? e.documentLoaded() : document.addEventListener("DOMContentLoaded", e.documentLoaded)
}
,
LadiPageScriptV2.prototype.stop = function(t) {
    var e = Object.keys(this.runtime.eventData)
      , i = this
      , n = this.runtime.isDesktop;
    e.forEach(function(e) {
        var a = i.runtime.eventData[e]
          , r = LadiPageApp[a.type + i.const.APP_RUNTIME_PREFIX];
        i.isEmpty(r) || r(a, t).stop(e, n)
    }),
    this.runtime.eventData = null,
    this.runtime.isMobileOnly = !1,
    this.runtime.ladipage_id = null,
    this.runtime.scroll_show_popup = {},
    this.runtime.scroll_depth = {},
    this.runtime.scroll_to_section = {},
    this.runtime.replaceStr = {},
    this.runtime.tmp.bodyScrollY = null,
    this.runtime.tmp.isFirstScroll = !1,
    this.runtime.tmp.runAfterLocation = [],
    this.runtime.isIE = !1,
    this.runtime.widthScrollBar = 0,
    i.removeInterval(i.runtime.interval_gallery),
    Object.keys(i.runtime.timeout_gallery).forEach(function(t) {
        i.removeTimeout(i.runtime.timeout_gallery[t])
    }),
    i.removeTimeout(i.runtime.tmp.timeoutViewport),
    i.removeInterval(i.runtime.interval_carousel),
    i.removeInterval(i.runtime.interval_countdown),
    i.removeInterval(i.runtime.interval_powered_by),
    i.runtime.interval_gallery = null,
    i.runtime.timeout_gallery = {},
    i.runtime.interval_carousel = null,
    i.runtime.timenext_carousel = {},
    i.runtime.interval_countdown = null,
    i.runtime.interval_powered_by = null;
    var a = document.getElementById(i.runtime.ladipage_powered_by_id);
    this.isEmpty(a) || a.parentElement.removeChild(a),
    document.removeEventListener("mouseleave", i.runEventMouseLeave),
    document.removeEventListener("mousemove", i.runEventMouseMove),
    document.removeEventListener("touchmove", i.runEventMouseMove),
    document.removeEventListener("mouseup", i.runEventMouseUp),
    document.removeEventListener("touchend", i.runEventMouseUp),
    window.removeEventListener("scroll", i.runEventScroll),
    window.removeEventListener("resize", i.runEventResize),
    window.removeEventListener("orientationchange", i.runEventOrientationChange),
    document.getElementById(this.runtime.backdrop_popup_id).removeEventListener("click", i.runEventBackdropClick)
}
,
LadiPageScriptV2.prototype.copy = function(t) {
    return JSON.parse(JSON.stringify(t))
}
,
LadiPageScriptV2.prototype.historyReplaceState = function(t) {
    try {
        window.history.replaceState(null, null, t)
    } catch (t) {}
}
,
LadiPageScriptV2.prototype.resetViewport = function() {
    this.isEmpty(this.runtime.tmp.timeoutViewport) || this.removeTimeout(this.runtime.tmp.timeoutViewport),
    this.isFunction(window.ladi_viewport) && (this.runtime.tmp.timeoutViewport = this.runTimeout(window.ladi_viewport, 10))
}
,
LadiPageScriptV2.prototype.runEventResize = function(t) {
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    e.resetViewport()
}
,
LadiPageScriptV2.prototype.runEventOrientationChange = function(t) {
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    e.resetViewport()
}
,
LadiPageScriptV2.prototype.runAfterLocation = function() {
    var t = this;
    for (t instanceof LadiPageScriptV2 || (t = LadiPageScript); t.runtime.tmp.runAfterLocation.length > 0; ) {
        t.runtime.tmp.runAfterLocation.shift()()
    }
}
,
LadiPageScriptV2.prototype.randomId = function() {
    var t = Date.now()
      , e = window.performance && window.performance.now && 1e3 * window.performance.now() || 0;
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(i) {
        var n = 16 * Math.random();
        return t > 0 ? (n = (t + n) % 16 | 0,
        t = Math.floor(t / 16)) : (n = (e + n) % 16 | 0,
        e = Math.floor(e / 16)),
        ("x" === i ? n : 3 & n | 8).toString(16)
    })
}
,
LadiPageScriptV2.prototype.removeLazyloadPopup = function(t) {
    var e = document.getElementById(t);
    if (!this.isEmpty(e))
        for (var i = e.getElementsByClassName("ladi-lazyload"); i.length > 0; )
            i[0].classList.remove("ladi-lazyload")
}
,
LadiPageScriptV2.prototype.reloadLazyload = function() {
    var t = this;
    t instanceof LadiPageScriptV2 || (t = LadiPageScript);
    for (var e = document.getElementsByClassName("ladi-lazyload"), i = [], n = 0; n < e.length; n++) {
        var a = t.getElementBoundingClientRect(e[n]).y + window.scrollY;
        window.scrollY + t.getHeightDevice() > a && a + e[n].offsetHeight > window.scrollY && i.push(e[n])
    }
    i.forEach(function(t) {
        t.classList.remove("ladi-lazyload")
    });
    for (var r = document.querySelectorAll(".ladi-gallery .ladi-gallery-view-item.selected:not(.ladi-lazyload)"), o = 0; o < r.length; o++)
        if (t.isEmpty(r[o].getAttribute("data-lazyload"))) {
            r[o].setAttribute("data-lazyload", !0);
            for (var l = r[o].parentElement.getElementsByClassName("ladi-lazyload"); l.length > 0; )
                l[0].classList.remove("ladi-lazyload")
        }
}
,
LadiPageScriptV2.prototype.documentLoaded = function() {
    var t = this;
    t instanceof LadiPageScriptV2 || (t = LadiPageScript);
    var e = t.getURLSearchParams(window.location.search, null, !0)
      , i = e.ladishow
      , n = e.ladihide
      , a = e.laditop;
    t.isEmpty(i) ? i = [] : t.isArray(i) || (i = i.split(",").removeSpace()),
    t.isEmpty(n) ? n = [] : t.isArray(n) || (n = n.split(",").removeSpace()),
    t.isEmpty(a) ? a = [] : t.isArray(a) || (a = a.split(",").removeSpace().reverse()),
    n.forEach(function(t) {
        window.ladi(t).hide()
    }),
    i.forEach(function(t) {
        window.ladi(t).show()
    }),
    a.forEach(function(t) {
        window.ladi(t).top()
    });
    var r = window.location.hash;
    if (!t.isEmpty(r))
        try {
            var o = document.querySelector(r);
            t.isEmpty(o) || t.isEmpty(o.id) || t.runTimeout(function() {
                window.ladi(o.id).scroll()
            }, 100)
        } catch (t) {}
}
,
LadiPageScriptV2.prototype.getWidthDevice = function() {
    return window.outerWidth > 0 ? window.outerWidth : window.screen.width
}
,
LadiPageScriptV2.prototype.getHeightDevice = function() {
    return window.outerHeight > 0 ? window.outerHeight : window.innerHeight
}
,
LadiPageScriptV2.prototype.startAutoScroll = function(t, e, i, n) {
    if (this.runtime.isDesktop ? i : n) {
        var a = document.getElementById(t);
        if (!this.isEmpty(a) && !a.classList.contains("ladi-auto-scroll")) {
            var r = 0;
            if ("section" != e) {
                if (a.clientWidth <= this.getWidthDevice())
                    return;
                r = (r = parseFloat(getComputedStyle(a).left) || 0) > 0 ? 0 : -1 * r
            } else {
                for (var o = a.querySelectorAll(".ladi-container > .ladi-element"), l = 0; l < o.length; l++) {
                    var s = parseFloat(getComputedStyle(o[l]).left) || 0;
                    s < r && (r = s)
                }
                r = r > 0 ? 0 : -1 * r,
                a.querySelector(".ladi-container").style.setProperty("margin-left", r + "px")
            }
            a.classList.add("ladi-auto-scroll"),
            a.scrollLeft = r
        }
    }
}
,
LadiPageScriptV2.prototype.stopAllVideo = function(t) {
    if (this.runtime.isDesktop || t) {
        var e = document.getElementsByClassName("iframe-video-play");
        if (e.length > 0) {
            if (e[0].classList.contains("lightbox-item"))
                document.getElementById(this.runtime.lightbox_screen_id).getElementsByClassName("lightbox-close")[0].click();
            else
                e[0].parentElement.removeChild(e[0]);
            this.stopAllVideo(t)
        } else
            delete this.runtime.tmp.gallery_playing_video
    }
}
,
LadiPageScriptV2.prototype.getLinkUTMRedirect = function(t, e, i) {
    var n = this.createTmpElement("a", "", {
        href: t
    })
      , a = this
      , r = this.getURLSearchParams(e)
      , o = r.utm_source;
    if (!this.isEmpty(o)) {
        o = "utm_source=" + encodeURIComponent(o);
        var l = r.utm_medium
          , s = r.utm_campaign
          , c = r.utm_term
          , d = r.utm_content;
        this.isEmpty(l) || (o += "&utm_medium=" + encodeURIComponent(l)),
        this.isEmpty(s) || (o += "&utm_campaign=" + encodeURIComponent(s)),
        this.isEmpty(c) || (o += "&utm_term=" + encodeURIComponent(c)),
        this.isEmpty(d) || (o += "&utm_content=" + encodeURIComponent(d)),
        this.isEmpty(n.href) || this.isEmpty(n.host) || !this.isEmpty(this.getURLSearchParams(n.search, "utm_source")) || (n.search = n.search + (this.isEmpty(n.search) ? "?" : "&") + o)
    }
    if (i) {
        var u = Object.keys(this.runtime.replaceStr)
          , p = "";
        (u = u.except(["utm_source", "utm_medium", "utm_campaign", "utm_term", "utm_content"])).forEach(function(t) {
            var e = a.getDataReplaceStr(t);
            e = a.isEmpty(e) ? "" : e,
            a.isArray(e) ? e.forEach(function(e) {
                a.isEmpty(p) || (p += "&"),
                p += t + "=" + encodeURIComponent(e)
            }) : (a.isEmpty(p) || (p += "&"),
            p += t + "=" + encodeURIComponent(e))
        }),
        this.isEmpty(n.href) || (n.search = n.search + (this.isEmpty(n.search) ? "?" : "&") + p)
    }
    return n.href
}
,
LadiPageScriptV2.prototype.randomInt = function(t, e) {
    return t = Math.ceil(t),
    e = Math.floor(e),
    Math.floor(Math.random() * (e - t + 1)) + t
}
,
LadiPageScriptV2.prototype.runCallback = function(t, e) {
    if (this.isFunction(e)) {
        var i = this;
        if (t) {
            var n = i.runInterval(function() {
                i.removeInterval(n),
                i.runCallback(!1, e)
            }, 0);
            return
        }
        e()
    }
}
,
LadiPageScriptV2.prototype.runTimeout = function(t, e) {
    if (this.isFunction(t)) {
        if (!this.isEmpty(e) && e > 0)
            return setTimeout(t, e);
        t()
    }
}
,
LadiPageScriptV2.prototype.removeTimeout = function(t) {
    return clearTimeout(t)
}
,
LadiPageScriptV2.prototype.removeInterval = function(t) {
    return clearInterval(t)
}
,
LadiPageScriptV2.prototype.runInterval = function(t, e) {
    if (this.isFunction(t))
        return setInterval(t, e)
}
,
LadiPageScriptV2.prototype.deleteCookie = function(t) {
    document.cookie = t + "=; expires = Thu, 01 Jan 1970 00:00:00 GMT; path = /"
}
,
LadiPageScriptV2.prototype.setCookie = function(t, e, i, n, a, r) {
    var o = "";
    if (a)
        o = "0";
    else {
        var l = new Date;
        l.setTime(l.getTime() + 24 * n * 60 * 60 * 1e3),
        o = "expires = " + l.toUTCString()
    }
    var s = e + " = " + i;
    this.isEmpty(o) || (s += "; " + o),
    this.isEmpty(t) || (s += "; domain = " + t),
    this.isEmpty(r) || this.runtime.isIE || (s += "; path = " + r),
    document.cookie = s
}
,
LadiPageScriptV2.prototype.getCookie = function(t) {
    for (var e = t + "=", i = decodeURIComponent(document.cookie).split(";"), n = 0; n < i.length; n++) {
        for (var a = i[n]; " " == a.charAt(0); )
            a = a.substring(1);
        if (0 == a.indexOf(e))
            return a.substring(e.length, a.length)
    }
    return ""
}
,
LadiPageScriptV2.prototype.getURLSearchParams = function(t, e, i) {
    var n = {};
    if (t = this.isNull(t) ? window.location.search : t,
    !this.isEmpty(t))
        for (var a = t.substr(1).split("&"), r = 0; r < a.length; ++r) {
            var o = a[r].split("=", 2);
            this.isNull(n[o[0]]) ? 1 == o.length ? n[o[0]] = "" : n[o[0]] = decodeURIComponent(o[1].replace(/\+/g, " ")) : i && (this.isArray(n[o[0]]) || (n[o[0]] = [n[o[0]]]),
            1 == o.length ? n[o[0]].push("") : n[o[0]].push(decodeURIComponent(o[1].replace(/\+/g, " "))))
        }
    return this.isEmpty(e) ? n : n[e]
}
,
LadiPageScriptV2.prototype.getVideoId = function(t, e) {
    if (this.isEmpty(e))
        return e;
    if (t == this.const.VIDEO_TYPE.youtube) {
        var i = this.createTmpElement("a", "", {
            href: e
        });
        -1 != e.toLowerCase().indexOf("watch") ? e = this.getURLSearchParams(i.search, "v") : -1 != e.toLowerCase().indexOf("embed/") ? e = i.pathname.substring("/embed/".length) : -1 != e.toLowerCase().indexOf("youtu.be") && (e = i.pathname.substring("/".length))
    }
    return e
}
,
LadiPageScriptV2.prototype.sendRequest = function(t, e, i, n, a, r) {
    var o = new XMLHttpRequest;
    try {
        if (this.isFunction(r) && (o.onreadystatechange = function() {
            r(o.responseText, o.status, o, e)
        }
        ),
        o.open(t, e, n),
        this.isObject(a))
            Object.keys(a).forEach(function(t) {
                o.setRequestHeader(t, a[t])
            });
        o.send(i)
    } catch (t) {
        this.isFunction(r) && r(t, 0, o, e)
    }
}
,
LadiPageScriptV2.prototype.runEventBackdropClick = function(t) {
    t.stopPropagation();
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript);
    for (var i = null, n = document.querySelectorAll('[data-popup-backdrop="true"]'), a = 0; a < n.length; a++)
        "none" != getComputedStyle(n[a]).display && (i = n[a].id);
    e.runRemovePopup(i, e.runtime.isClient)
}
,
LadiPageScriptV2.prototype.runEventMouseMove = function(t) {
    t.stopPropagation();
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    t = e.getEventCursorData(t);
    var i = null
      , n = 0
      , a = 0
      , r = 0;
    e.isEmpty(e.runtime.current_element_mouse_down_carousel) || (i = document.getElementById(e.runtime.current_element_mouse_down_carousel),
    n = t.pageX - e.runtime.current_element_mouse_down_carousel_position_x,
    a = parseFloat(i.getElementsByClassName("ladi-carousel-content")[0].getAttribute("data-left")) || 0,
    (a += n) < (r = -((parseFloat(e.runtime.eventData[e.runtime.current_element_mouse_down_carousel][e.runtime.device + ".option.carousel_crop.width"]) || 0) - i.clientWidth)) && (a = r),
    a > 0 && (a = 0),
    i.getElementsByClassName("ladi-carousel-content")[0].style.setProperty("left", a + "px"));
    if (!e.isEmpty(e.runtime.current_element_mouse_down_gallery_view)) {
        i = document.getElementById(e.runtime.current_element_mouse_down_gallery_view),
        n = t.pageX - e.runtime.current_element_mouse_down_gallery_view_position_x;
        var o = parseFloat(i.getAttribute("data-current")) || 0;
        o == (parseFloat(i.getAttribute("data-max-item")) || 0) - 1 && n < 0 && (n = 0),
        n > 0 && 0 == o && (n = 0),
        n >= e.runtime.current_element_mouse_down_gallery_view_diff ? (e.runtime.current_element_mouse_down_gallery_view = null,
        e.runtime.current_element_mouse_down_gallery_view_position_x = 0,
        i.getElementsByClassName("ladi-gallery-view-arrow-left")[0].click()) : n <= -e.runtime.current_element_mouse_down_gallery_view_diff ? (e.runtime.current_element_mouse_down_gallery_view = null,
        e.runtime.current_element_mouse_down_gallery_view_position_x = 0,
        i.getElementsByClassName("ladi-gallery-view-arrow-right")[0].click()) : i.querySelectorAll(".ladi-gallery-view-item.selected").length > 0 && i.querySelectorAll(".ladi-gallery-view-item.selected")[0].style.setProperty("left", n + "px")
    }
    e.isEmpty(e.runtime.current_element_mouse_down_gallery_control) || (i = document.getElementById(e.runtime.current_element_mouse_down_gallery_control),
    n = t.pageX - e.runtime.current_element_mouse_down_gallery_control_position_x,
    a = parseFloat(i.getElementsByClassName("ladi-gallery-control-box")[0].getAttribute("data-left")) || 0,
    (a += n) < (r = (parseFloat(getComputedStyle(i.getElementsByClassName("ladi-gallery-control")[0]).width) || 0) - (parseFloat(getComputedStyle(i.getElementsByClassName("ladi-gallery-control-box")[0]).width) || 0)) && (a = r),
    a > 0 && (a = 0),
    i.getElementsByClassName("ladi-gallery-control-box")[0].style.setProperty("left", a + "px"))
}
,
LadiPageScriptV2.prototype.runEventMouseUp = function(t) {
    t.stopPropagation();
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    t = e.getEventCursorData(t);
    var i = null
      , n = 0;
    if (!e.isEmpty(e.runtime.current_element_mouse_down_carousel)) {
        delete e.runtime.timenext_carousel[e.runtime.current_element_mouse_down_carousel],
        n = t.pageX - e.runtime.current_element_mouse_down_carousel_position_x;
        var a = (i = document.getElementById(e.runtime.current_element_mouse_down_carousel)).getElementsByClassName("ladi-carousel-content")[0].getAttribute("data-left");
        i.getElementsByClassName("ladi-carousel-content")[0].removeAttribute("data-left"),
        i.getElementsByClassName("ladi-carousel-content")[0].style.removeProperty("transition-duration"),
        e.runtime.current_element_mouse_down_carousel = null,
        n >= e.runtime.current_element_mouse_down_carousel_diff ? i.getElementsByClassName("ladi-carousel-arrow-left")[0].click() : n <= -e.runtime.current_element_mouse_down_carousel_diff ? i.getElementsByClassName("ladi-carousel-arrow-right")[0].click() : i.getElementsByClassName("ladi-carousel-content").length > 0 && (i.getElementsByClassName("ladi-carousel-content")[0].style.setProperty("transition-duration", "100ms"),
        i.getElementsByClassName("ladi-carousel-content")[0].style.setProperty("left", a),
        e.runTimeout(function() {
            i.getElementsByClassName("ladi-carousel-content")[0].style.removeProperty("transition-duration")
        }, 1))
    }
    e.isEmpty(e.runtime.current_element_mouse_down_gallery_view) || (i = document.getElementById(e.runtime.current_element_mouse_down_gallery_view)).querySelectorAll(".ladi-gallery-view-item.selected").length > 0 && i.querySelectorAll(".ladi-gallery-view-item.selected")[0].style.removeProperty("left"),
    e.isEmpty(e.runtime.current_element_mouse_down_gallery_control) || ((i = document.getElementById(e.runtime.current_element_mouse_down_gallery_control)).getElementsByClassName("ladi-gallery-control-box")[0].removeAttribute("data-left"),
    i.getElementsByClassName("ladi-gallery-control-box")[0].style.removeProperty("transition-duration")),
    e.runtime.current_element_mouse_down_carousel_position_x = 0,
    e.runtime.current_element_mouse_down_gallery_view = null,
    e.runtime.current_element_mouse_down_gallery_view_position_x = 0;
    var r = 0;
    e.runtime.current_element_mouse_down_gallery_control_time + e.runtime.current_element_mouse_down_gallery_control_time_click < Date.now() && (r = 5),
    e.runTimeout(function() {
        e.runtime.current_element_mouse_down_gallery_control = null,
        e.runtime.current_element_mouse_down_gallery_control_time = 0,
        e.runtime.current_element_mouse_down_gallery_control_position_x = 0
    }, r)
}
,
LadiPageScriptV2.prototype.runEventMouseLeave = function(t) {
    var e = this;
    e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    Object.keys(e.runtime.eventData).forEach(function(t) {
        var i = e.runtime.eventData[t];
        "popup" == i.type && i["option.show_popup_exit_page"] && window.ladi(t).show()
    })
}
,
LadiPageScriptV2.prototype.runEventScroll = function(t) {
    var e = this;
    if (e instanceof LadiPageScriptV2 || (e = LadiPageScript),
    !e.runtime.tmp.is_run_show_popup) {
        var i = e.runtime.isDesktop;
        if (Object.keys(e.runtime.eventData).forEach(function(t) {
            var n = e.runtime.eventData[t]
              , a = null
              , r = null
              , o = n[e.runtime.device + ".style.animation-name"];
            if (!e.isEmpty(o) && (a = document.getElementById(t),
            !e.isEmpty(a) && !a.classList.contains("ladi-animation"))) {
                var l = parseFloat(n[e.runtime.device + ".style.animation-delay"]) || 0;
                r = e.getElementBoundingClientRect(a).y + window.scrollY;
                var s = window.scrollY > r && window.scrollY <= r + a.offsetHeight || window.scrollY + e.getHeightDevice() >= r && window.scrollY + e.getHeightDevice() <= r + a.offsetHeight || r >= window.scrollY && window.scrollY + e.getHeightDevice() >= r + a.offsetHeight;
                e.runtime.tmp.isFirstScroll && l > 0 && !s && a.classList.add("ladi-animation-hidden"),
                s && (a.classList.add("ladi-animation"),
                e.runTimeout(function() {
                    a.classList.remove("ladi-animation-hidden")
                }, 1e3 * l))
            }
            var c = null
              , d = null
              , u = null;
            if (n[e.runtime.device + ".option.sticky"] && (c = n[e.runtime.device + ".option.sticky_position"],
            d = parseFloat(n[e.runtime.device + ".option.sticky_position_top"]),
            u = parseFloat(n[e.runtime.device + ".option.sticky_position_bottom"])),
            !e.isEmpty(c)) {
                var p = document.getElementById(t);
                if (!e.isEmpty(p) && -1 != ["default", "top", "bottom"].indexOf(c)) {
                    var m = e.getElementBoundingClientRect(p)
                      , g = p.getAttribute("data-top")
                      , y = p.getAttribute("data-left");
                    e.isEmpty(g) ? (p.setAttribute("data-top", m.y + window.scrollY),
                    g = m.y) : g = parseFloat(g),
                    e.isEmpty(y) ? (p.setAttribute("data-left", m.x + window.scrollX),
                    y = m.x) : y = parseFloat(y);
                    var h = null
                      , f = null;
                    if ("default" == c && (d > g - window.scrollY ? (h = d + "px",
                    f = y + "px") : e.getHeightDevice() - u - p.clientHeight < g - window.scrollY && (h = "calc(100% - " + (u + p.clientHeight) + "px)",
                    f = y + "px")),
                    "top" == c && (d > g - window.scrollY || e.getHeightDevice() - 0 < g - window.scrollY) && (h = d + "px",
                    f = y + "px"),
                    "bottom" == c && (e.getHeightDevice() - u - p.clientHeight < g - window.scrollY || 0 > g + p.clientHeight - window.scrollY) && (h = "calc(100% - " + (u + p.clientHeight) + "px)",
                    f = y + "px"),
                    e.isEmpty(h) || e.isEmpty(f))
                        p.style.removeProperty("top"),
                        p.style.removeProperty("left"),
                        p.style.removeProperty("width"),
                        p.style.removeProperty("position"),
                        p.style.removeProperty("z-index");
                    else if (p.style.setProperty("top", h),
                    p.style.setProperty("left", f),
                    "section" == n.type && (e.runtime.isMobileOnly ? p.style.setProperty("width", document.getElementsByClassName("ladi-wraper")[0].clientWidth + "px") : i && p.style.setProperty("width", "100%")),
                    p.style.setProperty("position", "fixed"),
                    p.style.setProperty("z-index", "90000040"),
                    !p.hasAttribute("data-first")) {
                        p.setAttribute("data-first", !0),
                        p.classList.contains("ladi-animation-hidden") && (p.classList.remove("ladi-animation-hidden"),
                        p.classList.add("ladi-animation"));
                        for (var _ = p.getElementsByClassName("ladi-animation-hidden"); _.length > 0; )
                            _[0].classList.add("ladi-animation"),
                            _[0].classList.remove("ladi-animation-hidden");
                        p.classList.remove("ladi-lazyload");
                        for (var v = p.getElementsByClassName("ladi-lazyload"); v.length > 0; )
                            v[0].classList.remove("ladi-lazyload")
                    }
                }
            }
            if ("popup" == n.type) {
                if (!e.isEmpty(e.runtime.scroll_show_popup[t]))
                    return;
                e.isEmpty(n["option.popup_welcome_page_scroll_to"]) || (a = document.getElementById(n["option.popup_welcome_page_scroll_to"]),
                e.isEmpty(a) || (r = a.offsetTop,
                (window.scrollY > r && window.scrollY <= r + a.offsetHeight || window.scrollY + e.getHeightDevice() >= r && window.scrollY + e.getHeightDevice() <= r + a.offsetHeight) && (e.runtime.scroll_show_popup[t] = !0,
                window.ladi(t).show())))
            }
            if ("section" == n.type) {
                if (!e.isEmpty(e.runtime.scroll_to_section[t]))
                    return;
                a = document.getElementById(t),
                e.isEmpty(a) || (r = a.offsetTop,
                (window.scrollY > r && window.scrollY <= r + a.offsetHeight || window.scrollY + e.getHeightDevice() >= r && window.scrollY + e.getHeightDevice() <= r + a.offsetHeight) && (e.runtime.scroll_to_section[t] = !0,
                e.runEventTracking(t, !1)))
            }
        }),
        e.runtime.isClient)
            for (var n = Math.round((window.scrollY + e.getHeightDevice()) / document.body.clientHeight * 100), a = 1; a < e.const.PERCENT_TRACKING_SCROLL.length; a++)
                n > e.const.PERCENT_TRACKING_SCROLL[a - 1] && n <= e.const.PERCENT_TRACKING_SCROLL[a] && -1 == e.runtime.scroll_depth.indexOf(e.const.PERCENT_TRACKING_SCROLL[a]) && (e.runtime.scroll_depth.push(e.const.PERCENT_TRACKING_SCROLL[a]),
                e.isFunction(window.gtag) && window.gtag("event", "ScrollDepth_" + e.const.PERCENT_TRACKING_SCROLL[a] + "_percent", {
                    event_category: "LadiPageScrollDepth",
                    event_label: window.location.host + window.location.pathname,
                    non_interaction: !0
                }),
                e.isFunction(window.fbq) && window.fbq("trackCustom", "ScrollDepth_" + e.const.PERCENT_TRACKING_SCROLL[a] + "_percent"));
        e.runtime.tmp.isFirstScroll = !1
    }
}
,
LadiPageScriptV2.prototype.runRemovePopup = function(t, e, i, n) {
    var a = this
      , r = document.querySelectorAll("#" + this.runtime.builder_section_popup_id + " .ladi-container > .ladi-element")
      , o = !1;
    e || LadiPagePlugin.getPlugin("popup").removeStyleShowPopupBuilder();
    var l = function() {
        var t = document.getElementById("style_popup");
        a.isEmpty(t) || t.parentElement.removeChild(t)
    }
      , s = [];
    if (this.isEmpty(t))
        for (var c = 0; c < r.length; c++)
            s.push(r[c].id);
    else
        s.push(t);
    s.forEach(function(t) {
        var i = document.getElementById(t);
        if (!a.isEmpty(i)) {
            a.isEmpty(i.style.getPropertyValue("display")) || (o = !0),
            i.style.removeProperty("display"),
            i.style.removeProperty("z-index");
            var n = i.getElementsByClassName("popup-close")[0];
            if (a.isEmpty(n) || n.style.removeProperty("display"),
            i.hasAttribute("data-popup-backdrop")) {
                l(),
                e && (a.isEmpty(a.runtime.tmp.bodyScrollY) || window.scrollTo(0, a.runtime.tmp.bodyScrollY)),
                a.runtime.tmp.bodyScrollY = null;
                for (var s = 0; s < r.length; s++)
                    r[s].style.removeProperty("z-index")
            }
        }
    }),
    o && this.isFunction(i) && i(),
    n && l()
}
,
LadiPageScriptV2.prototype.runShowPopup = function(t, e, i, n, a) {
    var r = "";
    if (!this.isEmpty(t)) {
        var o = document.getElementById(t)
          , l = null;
        if (!n || !this.isEmpty(o) && !this.isEmpty(this.runtime.eventData[t])) {
            var s = this;
            this.runtime.tmp.is_run_show_popup = !0;
            var c = 0;
            n || LadiPagePlugin.getPlugin("popup").showStyleShowPopupBuilder(t),
            n && (e = this.runtime.eventData[t][this.runtime.device + ".option.popup_position"],
            i = this.runtime.eventData[t][this.runtime.device + ".option.popup_backdrop"]);
            var d = function() {
                if (r += "body {",
                r += "overflow: hidden !important;",
                n) {
                    var e = window.scrollY;
                    if (!s.isEmpty(s.runtime.tmp.bodyScrollY)) {
                        var i = getComputedStyle(document.body);
                        "fixed" == i.position && (parseFloat(i.top) || 0) == -1 * s.runtime.tmp.bodyScrollY && (e = s.runtime.tmp.bodyScrollY)
                    }
                    "block" != o.style.getPropertyValue("display") || s.isEmpty(s.runtime.tmp.bodyScrollY) || (e = s.runtime.tmp.bodyScrollY),
                    r += "position: fixed !important;",
                    r += "width: 100% !important;",
                    r += "top: -" + e + "px",
                    s.runtime.tmp.bodyScrollY = e,
                    l = function() {
                        s.runtime.tmp.bodyScrollY = e
                    }
                }
                r += "}";
                for (var a = document.querySelectorAll("#" + s.runtime.builder_section_popup_id + " .ladi-container > .ladi-element"), d = 0; d < a.length; d++)
                    a[d].id != t && a[d].style.setProperty("z-index", "1", "important");
                c = 500
            };
            e == this.const.POSITION_TYPE.default ? (r += "#" + this.runtime.backdrop_popup_id + " { display: block !important; " + i + "}",
            o.setAttribute("data-popup-backdrop", !0),
            d()) : n || (r += "#" + this.runtime.backdrop_popup_id + " { display: block !important;}",
            o.setAttribute("data-popup-backdrop", !0),
            d()),
            this.isFunction(a) && "block" != o.style.getPropertyValue("display") && (a(),
            this.isFunction(l) && l()),
            o.style.setProperty("display", "block", "important");
            var u = o.hasAttribute("data-scroll")
              , p = !1;
            if (!u && o.clientHeight > .9 * this.getHeightDevice() && (n ? (o.setAttribute("data-scroll", !0),
            o.getElementsByClassName("ladi-popup")[0].style.setProperty("height", o.clientHeight + "px", "important"),
            o.style.setProperty("max-height", .9 * this.getHeightDevice() + "px"),
            o.style.setProperty("overflow-y", "auto", "important"),
            o.style.setProperty("overflow-x", "hidden", "important"),
            u = !0) : p = !0),
            n || LadiPagePlugin.getPlugin("popup").styleShowPopupBuilderUpDown(t, p),
            s.isEmpty(r) || this.createStyleElement("style_popup", r),
            n && !this.isEmpty(o)) {
                var m = o.getElementsByClassName("popup-close")[0];
                this.isEmpty(m) && ((m = document.createElement("div")).className = "popup-close",
                o.appendChild(m),
                m.addEventListener("click", function(e) {
                    e.stopPropagation(),
                    s.runRemovePopup(t, n)
                })),
                o.getElementsByClassName("popup-close")[0].style.setProperty("display", "block", "important");
                var g = function() {
                    var t = o.getElementsByClassName("popup-close")[0];
                    if (!s.isEmpty(t)) {
                        var e = s.getElementBoundingClientRect(o)
                          , i = 15
                          , n = 15;
                        e.x + 30 - t.clientWidth > n && (n = e.x + 30 - t.clientWidth),
                        e.y + 30 - t.clientHeight > i && (i = e.y + 30 - t.clientHeight),
                        u && (n += s.runtime.widthScrollBar),
                        t.style.setProperty("right", n + "px"),
                        t.style.setProperty("top", i + "px"),
                        t.style.setProperty("position", "fixed")
                    }
                };
                u && (g(),
                o.hasAttribute("data-resize-scroll") || (o.setAttribute("data-resize-scroll", !0),
                window.addEventListener("resize", g)))
            }
            n && this.runEventTracking(t, !1),
            s.runTimeout(function() {
                delete s.runtime.tmp.is_run_show_popup
            }, c)
        }
    }
}
,
LadiPageScriptV2.prototype.runEventTracking = function(t, e) {
    if (this.runtime.isClient && !this.isEmpty(t)) {
        var i = this.runtime.eventData[t]
          , n = i.type
          , a = null
          , r = null
          , o = null
          , l = null;
        if (e && "form" == n ? (a = i["option.form_conversion_name"],
        r = i["option.form_google_ads_conversion"],
        o = i["option.form_google_ads_label"],
        l = i["option.form_event_custom_script"]) : (a = i["option.conversion_name"],
        r = i["option.google_ads_conversion"],
        o = i["option.google_ads_label"],
        l = i["option.event_custom_script"]),
        this.isFunction(window.fbq) && !this.isEmpty(a) && window.fbq("trackCustom", a),
        this.isFunction(window.gtag) && (this.isEmpty(r) || this.isEmpty(o) || window.gtag("event", "conversion", {
            send_to: "AW-" + r + "/" + o
        }),
        !this.isEmpty(a))) {
            var s = "";
            s = "section" == n ? "LadiPageSection" : "popup" == n ? "LadiPagePopup" : "form" == n ? "LadiPageConversion" : "LadiPageClick";
            var c = LadiPageApp[i.type + this.const.APP_RUNTIME_PREFIX];
            if (!this.isEmpty(c)) {
                var d = c(i, this.runtime.isClient);
                this.isFunction(d.getEventTrackingCategory) && (s = c(i, this.runtime.isClient).getEventTrackingCategory())
            }
            window.gtag("event", a, {
                event_category: s,
                event_label: window.location.host + window.location.pathname
            })
        }
        this.isEmpty(l) || this.runFunctionString(l)
    }
}
,
LadiPageScriptV2.prototype.runFunctionString = function(t) {
    try {
        new Function(t)()
    } catch (t) {}
}
,
LadiPageScriptV2.prototype.setDataReplaceStr = function(t, e) {
    this.runtime.replaceStr[t] = e
}
,
LadiPageScriptV2.prototype.getDataReplaceStr = function(t, e) {
    var i = null;
    return this.isNull(e) || (i = e[t]),
    this.isNull(i) && (i = this.runtime.replaceStr[t]),
    i
}
,
LadiPageScriptV2.prototype.highlightText = function(t, e, i, n) {
    if (0 != e.length) {
        var a = i ? "gi" : "g"
          , r = [];
        e.forEach(function(t) {
            r.push(t.replaceAll("|", "\\|"))
        }),
        r.sort(function(t, e) {
            return e.length - t.length
        });
        var o = this;
        Array.from(t.childNodes).forEach(function(t) {
            var l = new RegExp(r.join("|"),a);
            if (3 !== t.nodeType)
                o.highlightText(t, e, i, n);
            else if (l.test(t.textContent)) {
                var s = document.createDocumentFragment()
                  , c = 0;
                t.textContent.replace(l, function(e, i) {
                    var a = document.createTextNode(t.textContent.slice(c, i))
                      , r = null;
                    o.isFunction(n) ? r = n(e) : (r = document.createElement("span")).textContent = e,
                    s.appendChild(a),
                    s.appendChild(r),
                    c = i + e.length
                });
                var d = document.createTextNode(t.textContent.slice(c));
                s.appendChild(d),
                t.parentNode.replaceChild(s, t)
            }
        })
    }
}
,
LadiPageScriptV2.prototype.convertDataReplaceStr = function(t, e, i, n) {
    for (var a = this, r = t, o = new RegExp(a.runtime.replacePrefixStart + "[^" + a.runtime.replacePrefixStart + "]*" + a.runtime.replacePrefixEnd,"gi"), l = null, s = [], c = function(t) {
        if (i)
            s.push(t);
        else {
            var e = t
              , o = e.split("|");
            e = (e = o.length > 1 ? o[o.length - 1] : e.substr(a.runtime.replacePrefixStart.length)).substr(0, e.length - a.runtime.replacePrefixEnd.length);
            var l = a.getDataReplaceStr(e, n);
            if (a.isEmpty(l))
                if (o.length > 1) {
                    var c = a.randomInt(0, o.length - 2);
                    l = o[c],
                    0 == c && (l = l.substr(a.runtime.replacePrefixStart.length))
                } else
                    l = "";
            r = r.replaceAll(t, l)
        }
    }; null !== (l = o.exec(t)); )
        l.index === o.lastIndex && o.lastIndex++,
        l.forEach(c);
    return s = s.unique(),
    a.highlightText(e, s, !0, function(t) {
        var e = document.createElement("span");
        return e.setAttribute("data-replace-str", t),
        e
    }),
    r
}
,
LadiPageScriptV2.prototype.setDataReplaceElement = function(t, e, i, n, a) {
    for (var r = this.isEmpty(n) ? document : document.getElementById(n), o = r.querySelectorAll("span[data-replace-str]"), l = 0; l < o.length; l++) {
        var s = o[l].getAttribute("data-replace-str");
        o[l].innerHTML = this.convertDataReplaceStr(s, null, !1, i)
    }
    for (var c = r.querySelectorAll("a[data-replace-href]"), d = 0; d < c.length; d++) {
        var u = c[d].getAttribute("data-replace-href");
        u = this.convertDataReplaceStr(u, null, !1, i),
        c[d].href = u
    }
    if (t)
        for (var p = r.querySelectorAll(".ladi-element > .ladi-form-item-container [name]"), m = 0; m < p.length; m++) {
            var g = p[m].getAttribute("name").trim();
            if (-1 != a.indexOf(g)) {
                var y = this.getDataReplaceStr(g, i);
                if (!this.isNull(y)) {
                    var h = this.isArray(y) ? y[0] : y;
                    if ("INPUT" == p[m].tagName) {
                        var f = p[m].getAttribute("type").trim();
                        if ("checkbox" == f || "radio" == f) {
                            var _ = !1;
                            if ("checkbox" == f) {
                                var v = y;
                                this.isArray(v) || (v = [v]),
                                _ = -1 != v.indexOf(p[m].getAttribute("value"))
                            }
                            "radio" == f && (_ = p[m].getAttribute("value") == h.trim()),
                            _ ? (p[m].checked = !0,
                            e && p[m].setAttribute("checked", "checked")) : (p[m].checked = !1,
                            e && p[m].removeAttribute("checked"));
                            var E = this.findAncestor(p[m], "ladi-form-checkbox-item");
                            if (!this.isEmpty(E)) {
                                var w = E.querySelector("span");
                                this.isEmpty(w) || w.setAttribute("data-checked", p[m].checked)
                            }
                        } else
                            p[m].value = h.trim(),
                            e && p[m].setAttribute("value", p[m].value)
                    }
                    if ("TEXTAREA" == p[m].tagName && (p[m].value = h.trim(),
                    e && (p[m].innerHTML = p[m].value)),
                    "SELECT" == p[m].tagName) {
                        var L = p[m].querySelector('option[value="' + h.trim() + '"]');
                        if (!this.isEmpty(L) && (p[m].value = L.getAttribute("value"),
                        e && !L.hasAttribute("selected"))) {
                            for (var S = p[m].querySelectorAll("option"), P = 0; P < S.length; P++)
                                S[P].removeAttribute("selected");
                            L.setAttribute("selected", "selected")
                        }
                    }
                }
            }
        }
}
,
LadiPageScriptV2.prototype.setDataReplaceStart = function() {
    for (var t = document.querySelectorAll(".ladi-headline, .ladi-paragraph, .ladi-list-paragraph ul"), e = 0; e < t.length; e++)
        this.convertDataReplaceStr(t[e].innerHTML, t[e], !0);
    this.setDataReplaceElement(!0, !0, null, null, Object.keys(this.runtime.replaceStr))
}
,
LadiPageScriptV2.prototype.showMessage = function(t, e, i) {
    if (this.isEmpty(t))
        this.isFunction(i) && i();
    else {
        var n = document.getElementsByClassName("ladipage-message")[0];
        if (this.isEmpty(n)) {
            var a = this;
            t = a.convertDataReplaceStr(t, null, !1, e),
            (n = document.createElement("div")).className = "ladipage-message",
            document.body.appendChild(n);
            var r = document.createElement("div");
            r.className = "ladipage-message-box",
            n.appendChild(r);
            var o = document.createElement("h1");
            r.appendChild(o),
            o.textContent = this.const.LANG.ALERT_TITLE;
            var l = document.createElement("div");
            l.className = "ladipage-message-text",
            r.appendChild(l),
            l.innerHTML = t;
            var s = document.createElement("button");
            s.className = "ladipage-message-close",
            r.appendChild(s),
            s.textContent = this.const.LANG.ALERT_BUTTON_TEXT,
            s.focus(),
            s.addEventListener("click", function(t) {
                t.stopPropagation(),
                n.parentElement.removeChild(n),
                a.isFunction(i) && i()
            })
        } else
            this.isFunction(i) && i()
    }
}
,
LadiPageScriptV2.prototype.findAncestor = function(t, e) {
    for (; (t = t.parentElement) && !t.classList.contains(e); )
        ;
    return t
}
,
LadiPageScriptV2.prototype.createStyleElement = function(t, e) {
    var i = document.getElementById(t);
    return this.isEmpty(i) && ((i = document.createElement("style")).id = t,
    i.type = "text/css",
    document.head.appendChild(i)),
    i.innerHTML != e && (i.innerHTML = e),
    i
}
,
LadiPageScriptV2.prototype.createTmpElement = function(t, e, i, n, a) {
    var r = document.createElement(t);
    this.isEmpty(i) || Object.keys(i).forEach(function(t) {
        r.setAttribute(t, i[t])
    });
    var o = document.createElement("div");
    return o.appendChild(r),
    n ? r.outerHTML = e : r.innerHTML = e,
    a ? o : o.firstChild
}
,
LadiPageScriptV2.prototype.getCountdownTime = function(t, e) {
    var i = Math.floor(t / 1e3)
      , n = i % 86400
      , a = n % 3600
      , r = Math.floor(i / 86400)
      , o = Math.floor(n / 3600)
      , l = Math.floor(a / 60)
      , s = a % 60;
    r = r < 0 ? 0 : r,
    o = o < 0 ? 0 : o,
    l = l < 0 ? 0 : l,
    s = s < 0 ? 0 : s,
    r = r < 10 ? "0" + r : r,
    o = o < 10 ? "0" + o : o,
    l = l < 10 ? "0" + l : l,
    s = s < 10 ? "0" + s : s;
    var c = {};
    return c[this.const.COUNTDOWN_ITEM_TYPE.day] = r,
    c[this.const.COUNTDOWN_ITEM_TYPE.hour] = o,
    c[this.const.COUNTDOWN_ITEM_TYPE.minute] = l,
    c[this.const.COUNTDOWN_ITEM_TYPE.seconds] = s,
    this.isEmpty(e) ? c : c[e]
}
,
LadiPageScriptV2.prototype.getElementBoundingClientRect = function(t) {
    var e = t.getBoundingClientRect();
    return (this.isNull(e.x) || this.isNull(e.y)) && (e.x = e.left,
    e.y = e.top),
    e
}
,
LadiPageScriptV2.prototype.getEventCursorData = function(t) {
    return (this.isNull(t.pageX) || this.isNull(t.pageY)) && (!this.isEmpty(t.touches) && t.touches.length > 0 ? (t.pageX = t.touches[0].pageX,
    t.pageY = t.touches[0].pageY) : !this.isEmpty(t.targetTouches) && t.targetTouches.length > 0 ? (t.pageX = t.targetTouches[0].pageX,
    t.pageY = t.targetTouches[0].pageY) : !this.isEmpty(t.changedTouches) && t.changedTouches.length > 0 && (t.pageX = t.changedTouches[0].pageX,
    t.pageY = t.changedTouches[0].pageY)),
    t
}
,
LadiPageScriptV2.prototype.getElementAHref = function(t, e) {
    var i = document.createElement("a");
    return !e || t.toLowerCase().startsWith("http://") || t.toLowerCase().startsWith("https://") || (t = "http://" + t),
    i.href = t,
    i
}
,
LadiPageScriptV2.prototype.loadScript = function(t, e, i) {
    var n = document.createElement("script");
    n.type = "text/javascript",
    e && (n.async = !0),
    n.src = t,
    n.onload = i,
    document.body.appendChild(n)
}
,
LadiPageScriptV2.prototype.isObject = function(t) {
    return !this.isFunction(t) && !this.isArray(t) && t instanceof Object
}
,
LadiPageScriptV2.prototype.isArray = function(t) {
    return t instanceof Array
}
,
LadiPageScriptV2.prototype.isFunction = function(t) {
    return "function" == typeof t || t instanceof Function
}
,
LadiPageScriptV2.prototype.isString = function(t) {
    return "string" == typeof t || t instanceof String
}
,
LadiPageScriptV2.prototype.isEmpty = function(t) {
    return !!this.isNull(t) || (this.isString(t) ? 0 == (t = t.trim()).length || "undefined" == t.toLowerCase() : !!this.isArray(t) && 0 == t.length)
}
,
LadiPageScriptV2.prototype.isNull = function(t) {
    return void 0 === t || void 0 == t
}
;
var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(t) {
        var e, i, n, a, r, o, l, s = "", c = 0;
        for (t = Base64._utf8_encode(t); c < t.length; )
            a = (e = t.charCodeAt(c++)) >> 2,
            r = (3 & e) << 4 | (i = t.charCodeAt(c++)) >> 4,
            o = (15 & i) << 2 | (n = t.charCodeAt(c++)) >> 6,
            l = 63 & n,
            isNaN(i) ? o = l = 64 : isNaN(n) && (l = 64),
            s = s + Base64._keyStr.charAt(a) + Base64._keyStr.charAt(r) + Base64._keyStr.charAt(o) + Base64._keyStr.charAt(l);
        return s
    },
    decode: function(t) {
        var e, i, n, a, r, o, l = "", s = 0;
        for (t = t.replace(/[^A-Za-z0-9\+\/\=]/g, ""); s < t.length; )
            e = Base64._keyStr.indexOf(t.charAt(s++)) << 2 | (a = Base64._keyStr.indexOf(t.charAt(s++))) >> 4,
            i = (15 & a) << 4 | (r = Base64._keyStr.indexOf(t.charAt(s++))) >> 2,
            n = (3 & r) << 6 | (o = Base64._keyStr.indexOf(t.charAt(s++))),
            l += String.fromCharCode(e),
            64 != r && (l += String.fromCharCode(i)),
            64 != o && (l += String.fromCharCode(n));
        return l = Base64._utf8_decode(l)
    },
    _utf8_encode: function(t) {
        t = t.replace(/\r\n/g, "\n");
        for (var e = "", i = 0; i < t.length; i++) {
            var n = t.charCodeAt(i);
            n < 128 ? e += String.fromCharCode(n) : n > 127 && n < 2048 ? (e += String.fromCharCode(n >> 6 | 192),
            e += String.fromCharCode(63 & n | 128)) : (e += String.fromCharCode(n >> 12 | 224),
            e += String.fromCharCode(n >> 6 & 63 | 128),
            e += String.fromCharCode(63 & n | 128))
        }
        return e
    },
    _utf8_decode: function(t) {
        for (var e = "", i = 0, n = 0, a = 0; i < t.length; )
            (n = t.charCodeAt(i)) < 128 ? (e += String.fromCharCode(n),
            i++) : n > 191 && n < 224 ? (a = t.charCodeAt(i + 1),
            e += String.fromCharCode((31 & n) << 6 | 63 & a),
            i += 2) : (a = t.charCodeAt(i + 1),
            c3 = t.charCodeAt(i + 2),
            e += String.fromCharCode((15 & n) << 12 | (63 & a) << 6 | 63 & c3),
            i += 3);
        return e
    }
}
  , LadiPageScript = LadiPageScript || new LadiPageScriptV2;
LadiPageScript.init();
var lightbox_run = function(t, e) {
    var i = document.getElementById(LadiPageScript.runtime.lightbox_screen_id);
    if (!LadiPageScript.isEmpty(i)) {
        i.innerHTML = '<div class="lightbox-close"></div>' + t;
        var n = i.getElementsByClassName("lightbox-close")[0]
          , a = i.getElementsByClassName("lightbox-item")[0]
          , r = function() {
            var t = document.getElementById(LadiPageScript.runtime.backdrop_popup_id);
            return LadiPageScript.isEmpty(t) || "none" == getComputedStyle(t).display
        }
          , o = 0;
        r() ? (o = window.scrollY,
        LadiPageScript.runtime.tmp.bodyScrollY = o) : o = LadiPageScript.runtime.tmp.bodyScrollY;
        var l = function(t) {
            t.style.removeProperty("display"),
            t.innerHTML = "";
            var e = document.getElementById("style_lightbox");
            LadiPageScript.isEmpty(e) || e.parentElement.removeChild(e);
            var i = r();
            i && !LadiPageScript.isEmpty(LadiPageScript.runtime.tmp.bodyScrollY) && window.scrollTo(0, LadiPageScript.runtime.tmp.bodyScrollY),
            i && (LadiPageScript.runtime.tmp.bodyScrollY = null)
        };
        n.addEventListener("click", function(t) {
            t.stopPropagation(),
            l(i)
        }),
        i.style.setProperty("display", "block");
        var s = "body {";
        s += "overflow: hidden !important;",
        s += "position: fixed !important;",
        s += "width: 100% !important;",
        s += "top: -" + o + "px",
        s += "}",
        LadiPageScript.createStyleElement("style_lightbox", s);
        var c = function() {
            var t = i.getElementsByClassName("lightbox-close")[0]
              , e = i.getElementsByClassName("lightbox-item")[0];
            if (!LadiPageScript.isEmpty(t) && !LadiPageScript.isEmpty(e)) {
                var n = LadiPageScript.getElementBoundingClientRect(e)
                  , a = 10
                  , r = 10;
                n.x - 5 - t.clientWidth > r && (r = n.x - 5 - t.clientWidth),
                n.y - 5 - t.clientHeight > a && (a = n.y - 5 - t.clientHeight),
                r += LadiPageScript.runtime.widthScrollBar,
                t.style.setProperty("right", r + "px"),
                t.style.setProperty("top", a + "px")
            }
        };
        LadiPageScript.isEmpty(i.getAttribute("data-event-click")) && (i.setAttribute("data-event-click", !0),
        i.addEventListener("click", function(t) {
            t.stopPropagation(),
            l(t.target)
        }),
        window.addEventListener("resize", c)),
        n.style.setProperty("top", "-100px"),
        n.style.setProperty("right", "-100px"),
        a.onload = c,
        a.src = e
    }
}
  , lightbox_iframe = function(t, e) {
    if (!LadiPageScript.isEmpty(t)) {
        var i = document.getElementById(LadiPageScript.runtime.lightbox_screen_id);
        if (!LadiPageScript.isEmpty(i)) {
            var n = ""
              , a = .9 * document.body.clientWidth
              , r = .5625 * a;
            r > .9 * LadiPageScript.getHeightDevice() && (a = 1.7778 * (r = .9 * LadiPageScript.getHeightDevice()));
            var o = "margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; width: " + a + "px; height: " + r + "px; max-width: 90%; max-height: 90%;";
            n = '<iframe id="lightbox_iframe" class="lightbox-item" style="' + o + '" frameborder="0" allowfullscreen></iframe>';
            var l = t
              , s = LadiPageScript.createTmpElement("iframe", l, null, !0);
            LadiPageScript.isEmpty(s) || "IFRAME" != s.tagName || (l = s.src,
            s.removeAttribute("src"),
            s.setAttribute("style", o),
            s.removeAttribute("width"),
            s.removeAttribute("height"),
            s.classList.add("lightbox-item"),
            e || s.setAttribute("id", "lightbox_iframe"),
            n = s.outerHTML),
            lightbox_run(n, l)
        }
    }
}
  , lightbox_image = function(t) {
    if (!LadiPageScript.isEmpty(t)) {
        var e = document.getElementById(LadiPageScript.runtime.lightbox_screen_id);
        if (!LadiPageScript.isEmpty(e)) {
            lightbox_run('<img class="lightbox-item" style="margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; object-fit: scale-down; max-width: 90%; max-height: 90%;" />', t)
        }
    }
}
  , lightbox_video = function(t, e) {
    if (!LadiPageScript.isEmpty(t) && !LadiPageScript.isEmpty(e) && e == LadiPageScript.const.VIDEO_TYPE.youtube) {
        LadiPageScript.stopAllVideo();
        var i = LadiPageScript.getVideoId(e, t);
        lightbox_iframe('<iframe id="lightbox_player" src="https://www.youtube.com/embed/' + i + '?autoplay=1&rel=0" class="iframe-video-play" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>', !0)
    }
}
  , LadiPageLibraryV2 = LadiPageLibraryV2 || function() {}
;
LadiPageLibraryV2.prototype.open_url = function(t, e) {
    if (!LadiPageScript.isEmpty(this.id)) {
        var i = this.id
          , n = "";
        if (LadiPageScript.isObject(e))
            Object.keys(e).forEach(function(t) {
                LadiPageScript.isEmpty(n) || (n += "&"),
                n += t + "=" + encodeURIComponent(e[t])
            });
        if (!LadiPageScript.isEmpty(n)) {
            var a = LadiPageScript.createTmpElement("a", "", {
                href: i
            });
            a.search = a.search + (LadiPageScript.isEmpty(a.search) ? "?" : "&") + n,
            i = a.href
        }
        i = LadiPageScript.getLinkUTMRedirect(i, window.location.search),
        window.open(i, t)
    }
}
,
LadiPageLibraryV2.prototype.submit = function() {
    var t = document.getElementById(this.id);
    if (!LadiPageScript.isEmpty(t)) {
        var e = t.querySelector('.ladi-form button[type="submit"]');
        LadiPageScript.isEmpty(e) || e.click()
    }
}
,
LadiPageLibraryV2.prototype.scroll = function() {
    var t = document.getElementById(this.id);
    if (!LadiPageScript.isEmpty(t)) {
        for (var e = document.querySelectorAll("#" + LadiPageScript.runtime.builder_section_popup_id + " .ladi-container > .ladi-element"), i = 0; i < e.length; i++)
            e[i].id != this.id && e[i].hasAttribute("data-popup-backdrop") && "block" == e[i].style.getPropertyValue("display") && LadiPageScript.runRemovePopup(e[i].id, !0);
        var n = function() {
            t.scrollIntoView({
                behavior: "smooth"
            })
        };
        try {
            if (!LadiPageScript.isNull(window.jQuery))
                return void $("html, body").animate({
                    scrollTop: $(t).offset().top
                });
            n()
        } catch (t) {
            n()
        }
    }
}
,
LadiPageLibraryV2.prototype.value = function(t) {
    if (!LadiPageScript.isEmpty(this.id)) {
        var e = document.querySelectorAll("#" + this.id + " > ." + ["ladi-button .ladi-headline", "ladi-headline", "ladi-paragraph", "ladi-list-paragraph"].join(", #" + this.id + " > ."))
          , i = document.querySelectorAll("#" + this.id + " > ." + ["ladi-form-item-container .ladi-form-item > input", "ladi-form-item-container .ladi-form-item > textarea", "ladi-form-item-container .ladi-form-item > select"].join(", #" + this.id + " > ."))
          , n = document.querySelectorAll("#" + this.id + " > ." + ["ladi-form-item-container .ladi-form-checkbox-item > input"].join(", #" + this.id + " > ."))
          , a = 0;
        for (a = 0; a < e.length; a++)
            e[a].innerHTML = t;
        for (a = 0; a < i.length; a++)
            i[a].value = t;
        var r = LadiPageScript.isArray(t) ? t : [t];
        for (a = 0; a < n.length; a++) {
            var o = !1;
            "checkbox" == n[a].getAttribute("type").toLowerCase() && -1 != r.indexOf(n[a].value) && (o = !0),
            "radio" == n[a].getAttribute("type").toLowerCase() && r.length > 0 && r[0] == n[a].value && (o = !0),
            o ? n[a].checked || n[a].click() : n[a].checked && n[a].click()
        }
    }
}
,
LadiPageLibraryV2.prototype.top = function() {
    var t = document.getElementById(this.id);
    if (!LadiPageScript.isEmpty(t) && t.classList.contains("ladi-section"))
        try {
            t.parentElement.insertBefore(t, t.parentElement.firstChild),
            LadiPageScript.reloadLazyload()
        } catch (t) {}
}
,
LadiPageLibraryV2.prototype.hide = function() {
    var t = document.getElementById(this.id);
    LadiPageScript.isEmpty(t) || (0 == t.getElementsByClassName("ladi-popup").length ? (t.style.setProperty("display", "none", "important"),
    LadiPageScript.reloadLazyload()) : LadiPageScript.runRemovePopup(this.id, !0, function() {
        for (var t = document.querySelectorAll("#" + LadiPageScript.runtime.builder_section_popup_id + " .ladi-container > .ladi-element"), e = 0; e < t.length; e++)
            t[e].id != this.id && t[e].hasAttribute("data-popup-backdrop") && "block" == t[e].style.getPropertyValue("display") && LadiPageScript.runRemovePopup(t[e].id, !0)
    }))
}
,
LadiPageLibraryV2.prototype.show = function() {
    var t = document.getElementById(this.id);
    if (!LadiPageScript.isEmpty(t))
        if (0 == t.getElementsByClassName("ladi-popup").length) {
            t.style.setProperty("display", "block", "important");
            var e = LadiPageScript.runtime.eventData[this.id];
            LadiPageScript.isEmpty(e) || LadiPageScript.startAutoScroll(this.id, e.type, e[LadiPageScript.const.DESKTOP + ".option.auto_scroll"], e[LadiPageScript.const.MOBILE + ".option.auto_scroll"]),
            LadiPageScript.reloadLazyload()
        } else
            LadiPageScript.runShowPopup(this.id, null, null, !0, function() {
                for (var t = document.querySelectorAll("#" + LadiPageScript.runtime.builder_section_popup_id + " .ladi-container > .ladi-element"), e = 0; e < t.length; e++)
                    t[e].id != this.id && t[e].hasAttribute("data-popup-backdrop") && "block" == t[e].style.getPropertyValue("display") && LadiPageScript.runRemovePopup(t[e].id, !0)
            }),
            LadiPageScript.removeLazyloadPopup(this.id)
}
,
["start", "add_turn"].forEach(function(t) {
    LadiPageLibraryV2.prototype[t] = function() {
        var e = LadiPageScript.runtime.eventData[this.id];
        if (!LadiPageScript.isEmpty(e)) {
            var i = LadiPageApp[e.type + LadiPageScript.const.APP_RUNTIME_PREFIX];
            if (!LadiPageScript.isEmpty(i)) {
                var n = i(e, !0);
                LadiPageScript.isFunction(n[t]) && n[t](this.id)
            }
        }
    }
});
var LadiPageAppV2, ladi = ladi || function(t) {
    if (!LadiPageScript.isEmpty(t)) {
        var e = new LadiPageLibraryV2;
        return e.id = t,
        e
    }
}
;
LadiPageScript.const.API_CHECK_VERIFY = "https://la.ladipage.com/2.0/domain/check",
LadiPageScript.const.API_CHECKING_FORM = "https://la.ladipage.com/2.0/elastic/tracking-form",
LadiPageScript.const.API_FORM_DATA = "https://api.forms.ladipage.com/2.0/send-form",
LadiPageScript.const.CDN_LIBRARY_JS_URL = "https://w.ladicdn.com/v2/source/",
LadiPageScript.const.CDN_LIBRARY_CSS_URL = "https://w.ladicdn.com/v2/source/",
(LadiPageAppV2 = LadiPageAppV2 || function() {}
).prototype.notify_runtime = function(t, e) {
    var i = function() {}
      , n = null
      , a = "top_left"
      , r = "top_center"
      , o = "top_right"
      , l = "bottom_left"
      , s = "bottom_center"
      , c = "bottom_right";
    return i.prototype.run = function(e, i) {
        var d = t["option.sheet_id"];
        if (!LadiPageScript.isEmpty(d)) {
            var u = document.getElementById(e)
              , p = i ? LadiPageScript.const.DESKTOP : LadiPageScript.const.MOBILE
              , m = t[p + ".option.position"]
              , g = 1e3 * (parseFloat(t["option.time_show"]) || 5)
              , y = 1e3 * (parseFloat(t["option.time_delay"]) || 10)
              , h = "https://static.ladipage.net/source/notify.svg"
              , f = [{
                key: "gsx$title",
                className: ".ladi-notify-title"
            }, {
                key: "gsx$content",
                className: ".ladi-notify-content"
            }, {
                key: "gsx$time",
                className: ".ladi-notify-time"
            }, {
                key: "gsx$image",
                className: ".ladi-notify-image img"
            }]
              , _ = function() {
                u.style.setProperty("opacity", 0),
                m != a && m != r && m != o || u.style.setProperty("top", -u.clientHeight - 100 + "px"),
                m != l && m != s && m != c || u.style.setProperty("bottom", -u.clientHeight - 100 + "px")
            };
            _(),
            f.forEach(function(t) {
                "gsx$image" == t.key ? u.querySelectorAll(t.className)[0].src = h : u.querySelectorAll(t.className)[0].textContent = ""
            });
            var v = function(t) {
                t = t.sort(function() {
                    return .5 - Math.random()
                });
                var e = -1
                  , i = function() {
                    if (e + 1 < t.length) {
                        var d = t[++e]
                          , p = Object.keys(d);
                        u.style.removeProperty("opacity"),
                        m != a && m != r && m != o || u.style.removeProperty("top"),
                        m != l && m != s && m != c || u.style.removeProperty("bottom"),
                        f.forEach(function(t) {
                            -1 != p.indexOf(t.key) && ("gsx$image" == t.key ? u.querySelectorAll(t.className)[0].src = LadiPageScript.isEmpty(d[t.key].$t) ? h : d[t.key].$t : u.querySelectorAll(t.className)[0].textContent = d[t.key].$t)
                        }),
                        n = LadiPageScript.runTimeout(function() {
                            _(),
                            n = LadiPageScript.runTimeout(i, y)
                        }, g)
                    } else
                        v(t)
                };
                n = LadiPageScript.runTimeout(i, y)
            };
            LadiPageScript.sendRequest("GET", "https://spreadsheets.google.com/feeds/list/" + d + "/1/public/values?alt=json", null, !0, null, function(t, e, i) {
                if (i.readyState == XMLHttpRequest.DONE && 200 == e) {
                    u.querySelector(".ladi-notify").classList.remove("ladi-hidden");
                    var n = JSON.parse(t).feed.entry;
                    u.classList.add("ladi-notify-transition"),
                    v(n)
                }
            })
        }
    }
    ,
    i.prototype.stop = function(t, e) {
        LadiPageScript.removeTimeout(n),
        n = null
    }
    ,
    new i
}
,
(LadiPageAppV2 = LadiPageAppV2 || function() {}
).prototype.spinlucky_runtime = function(t, e) {
    var i = function() {}
      , n = function(t) {
        return parseFloat(LadiPageScript.getCookie("_total_turn_" + t)) || 0
    };
    return i.prototype.getEventTrackingCategory = function() {
        return "LadiPageFinish"
    }
    ,
    i.prototype.run = function(e, i) {
        var a = t["option.spinlucky_setting.list_value"]
          , r = t["option.spinlucky_setting.result_popup_id"]
          , o = t["option.spinlucky_setting.result_message"]
          , l = t["option.spinlucky_setting.max_turn"]
          , s = n(e);
        if (!LadiPageScript.isEmpty(a)) {
            LadiPageScript.setDataReplaceStr("spin_turn_left", l - s);
            var c = document.getElementById(e)
              , d = c.getElementsByClassName("ladi-spin-lucky-start")[0]
              , u = c.getElementsByClassName("ladi-spin-lucky-screen")[0]
              , p = !1;
            d.addEventListener("click", function(t) {
                if (t.stopPropagation(),
                !p)
                    if ((s = n(e)) >= l)
                        LadiPageScript.showMessage(LadiPageScript.const.LANG.GAME_MAX_TURN_MESSAGE.format(l));
                    else {
                        p = !0;
                        var i = []
                          , c = 0
                          , d = 1;
                        a.forEach(function(t, e) {
                            var n = Base64.decode(t).split("|")
                              , a = n[0].trim()
                              , r = n[1].trim()
                              , o = parseFloat(n[2].trim()) || 0;
                            i.push({
                                min: d,
                                max: d + o - 1,
                                value: a,
                                text: r,
                                percent: o,
                                index: e
                            }),
                            d += o,
                            c += o
                        });
                        for (var m = LadiPageScript.randomInt(1, c), g = null, y = 0; y < i.length; y++)
                            if (i[y].min <= m && i[y].max >= m) {
                                g = i[y];
                                break
                            }
                        if (LadiPageScript.isEmpty(g))
                            p = !1;
                        else {
                            var h = parseFloat(u.getAttribute("data-rotate")) || 0
                              , f = g.index * (360 / i.length) + 360 * (4 + Math.ceil(h / 360)) + 180 / i.length
                              , _ = "rotate(" + f + "deg)";
                            u.setAttribute("data-rotate", f),
                            u.style.setProperty("transform", _),
                            u.style.setProperty("-webkit-transform", _),
                            "NEXT_TURN" != g.value.toUpperCase() && (s++,
                            LadiPageScript.setCookie(null, "_total_turn_" + e, s, 1, !1, window.location.pathname));
                            LadiPageScript.runTimeout(function() {
                                "NEXT_TURN" == g.value.toUpperCase() ? LadiPageScript.isEmpty(g.text) || LadiPageScript.showMessage(g.text) : (LadiPageScript.setDataReplaceStr("coupon", g.value),
                                LadiPageScript.setDataReplaceStr("coupon_code", g.value),
                                LadiPageScript.setDataReplaceStr("coupon_text", g.text),
                                LadiPageScript.setDataReplaceStr("spin_turn_left", l - s),
                                LadiPageScript.setDataReplaceElement(!0, !1, null, null, ["coupon"]),
                                r == LadiPageScript.const.GAME_RESULT_TYPE.default ? LadiPageScript.isEmpty(o) || LadiPageScript.showMessage(o) : window.ladi(r).show(),
                                LadiPageScript.runEventTracking(e, !1)),
                                p = !1
                            }, 1e3 * parseFloat(getComputedStyle(u).transitionDuration) + 1e3)
                        }
                    }
            })
        }
    }
    ,
    i.prototype.stop = function(t, e) {}
    ,
    i.prototype.start = function(t) {
        var e = document.getElementById(t);
        if (!LadiPageScript.isEmpty(e) && e.getElementsByClassName("ladi-spin-lucky").length > 0) {
            var i = e.getElementsByClassName("ladi-spin-lucky-start")[0];
            LadiPageScript.isEmpty(i) || i.click()
        }
    }
    ,
    i.prototype.add_turn = function(e) {
        var i = t["option.spinlucky_setting.max_turn"]
          , a = n(e);
        a > 0 && a--,
        LadiPageScript.setCookie(null, "_total_turn_" + e, a, 1, !1, window.location.pathname),
        LadiPageScript.setDataReplaceStr("spin_turn_left", i - a),
        LadiPageScript.setDataReplaceElement(!1)
    }
    ,
    new i
}
;
