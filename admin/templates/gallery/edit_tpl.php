﻿<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<style>
    .form-group .control-label span{
        color: red;
    }
</style>

<h3>Thông tin</h3>

<?php $flash->display(); ?>

<form name="frm" method="post" action="<?=url(null, 'edit')?>" enctype="multipart/form-data" class="nhaplieu form-horizontal">

    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">Loại <span>*</span></label>
        <div class="col-sm-9">
            <select name="type" id="type" class="form-control">
                <option value="video" <?php if (@$item['type'] == 'video') echo 'selected'; ?> >Youtube video</option>
                <option value="photo" <?php if (@$item['type'] == 'photo') echo 'selected'; ?>>Photo</option>
            </select>
        </div>
    </div>

    <div class="form-group" id="photo" style="<?php if(empty($_GET['id']) or @$item['type'] == 'video') echo 'display: none'; ?>;">
        <label class="col-sm-3 control-label">Ảnh <span>*</span></label>
        <div class="col-sm-9">
            <div id="preview1" class="_preview">
                <?php if(@$item['type'] == 'photo'): ?>
                    <img src="<?=@$item['resource']?>" alt="Có lỗi xảy ra">
                <?php endif; ?>
            </div>
            <input type="file" name="photo" onchange="preview(this, '#preview1')">
        </div>
    </div>

    <div class="form-group" id="video" style="<?php if(@$item['type'] == 'photo') echo 'display: none'; ?>;">
        <label class="col-sm-3 control-label">Video <span>*</span></label>
        <div class="col-sm-9">
            <input type="text" name="video" value="<?php if (@$item['type'] == 'video') echo @$item['resource'];?>">
            <span class="help-block">Đặt link mở video youtube</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Hiển thị</label>
        <div class="col-sm-9">
            <input type="checkbox" name="is_show" <?php if (@$item['is_show'] == 1) echo 'checked'; ?>>
            <span class="help-block">Tick vào đây, sẽ hiển thị trên trang chủ</span>
        </div>
    </div>

    <input type="hidden" name="id" id="id" value="<?= @$item['id'] ?>" />
    <input type="submit" value="Lưu" class="btn" />
    <input type="button" value="Thoát" onclick="javascript:window.location = '<?=router('admin', ['com' => 'gallery'])?>'" class="btn" />
</form>

<script>

    $('#type').change(function () {
        $('#video').toggle();
        $('#photo').toggle();
    });

    function preview(el, target) {
        var files = el.files;
        // console.log(el);
        if (files.length) {
            var fileName = files[0].name;
            // $(this).next('.custom-file-label').html(fileName);

            var reader = new FileReader();
            reader.onload = function (e) {
                $(target).html('<img src="' + e.target.result + '">');

                var img = new Image();
                img.onload = function () {// image is loaded; sizes are available
                    if (img.width > img.height) {
                        // previewLandscape();
                    } else {
                        // previewPortrait();
                    }
                };
                img.src = reader.result;
            };
            reader.readAsDataURL(files[0]);
        }
    }
</script>