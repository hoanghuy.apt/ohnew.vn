﻿<h3>Thêm mới Landing page</h3>

<form name="frm" method="post" action="index.php?com=landing-page&act=do_add" enctype="multipart/form-data" class="nhaplieu">
	<b><u>Cấu hình chung</u></b><br/><br/>
    <b>Tên:</b> 
    <input type="text" required="required" name="name" value="" class="input" required/><br /><br /><br />
	<br />
    <b>Slide chính:</b><input type="file" name="top_slide" required/><br /><br />
    <b>Slide phụ:</b><input type="file" name="middle_slide" required/><br /><br />
    <b>Ảnh khuyến mãi:</b> <input type="file" name="contact_image" required/><br /><br />
    <b>Mô tả:</b> <br/>
	<textarea class="ckeditor" name="content" id="nd" rows="100" cols="40"></textarea><br /><br />
	<br />
    <b>Nội dung feedback:</b> <br/>
	<textarea class="ckeditor" name="feedback_content" id="nd" rows="100" cols="40"></textarea><br /><br />
	<br /><br />
    <b>Nội dung footer:</b> <br/>
	<textarea class="ckeditor" name="footer_content" id="footer_content" rows="100" cols="40"></textarea><br /><br />
	<br /><br />
    <b>Đếm ngược:</b>
    <input type="number" required="required" name="count_down_sec" value="100000" class="input" required /> (giây)<br /><br /><br />
	<b><u>Sản phẩm:</u></b><br/><br/>
	<input type="hidden" value="<?=$setting_id?>" name="setting_id"/>
	<table class="table" id="table_product">
		<tr>
			<th>Hình ảnh</th>
			<th>Tên sản phẩm</th>
			<th>Loại</th>
			<th>Thứ tự</th>
			<th><p onclick="add_new_product()" id="add-image">+ Thêm sản phẩm</p></th>
		</tr>
	</table>
	<br/><br/><br/>
	<b><u>Hình ảnh liên quan:</u></b><br/><br/>
	
	<table class="table" id="table_images">
		<tr>
			<th>Hình ảnh</th>
			<th>Tên</th>
			<th>Loại</th>
			<th>Thứ tự</th>
			<th><p onclick="add_new_image()" id="add-image">+ Thêm hình ảnh</p></th>
		</tr>
	</table>
	
	
	
    <br />
    <input type="hidden" name="id" id="id" value="<?= @$item['id'] ?>" />
    <input type="submit" value="Lưu" class="btn" />
    <a href="index.php?com=landing-page&act=list"><input type="button" value="Quay lại" class="btn" /></a>
</form>
<script>
	function delete_new_row(key_new){
		$('.product_row_'+key_new).remove();
	}
	function add_new_product(){
		var id = 'pro_'+Math.floor(Math.random() * 1000) + 1;
		var html = '<tr class="product_row_'+id+'" style="background-color: #c6f3cd;">'
			+'<td>'
				+'<img src="" class="product-image">'
				+'<input type="file" name="image_url_'+id+'" placeholder="Thay ảnh" />'
			+'</td>'
			+'<td>'
				+'<input type="text" required="required" name="products['+id+'][name]" value="Sản phẩm mới" class="name-input" />'
			+'</td>'
			+'<td>'
				+'<select name="products['+id+'][type]">'
					+'<option value="TENDENCY" >SP Xu hướng</option>'
					+'<option value="PRODUCT" >Sản phẩm</option>'
				+'</select>'
			+'</td>'
			+'<td>'
				+'<input type="text" required="required" class="sort_index" name="products['+id+'][sort_index]" value="0" />'
			+'</td>'
			+'<td>'
				+'<input type="hidden" name="products['+id+'][is_add]" value="1"/>'
				+'<p onclick="delete_new_row(\''+id+'\')" id="delete-image">Xóa</p>'
			+'</td>'
		+'</tr>';
		$('#table_product').append(html);
	}
	function add_new_image(){
		var id = 'img_'+Math.floor(Math.random() * 1000) + 1;
		var html = '<tr class="product_row_'+id+'" style="background-color: #c6f3cd;">'
			+'<td>'
				+'<img src="" class="product-image">'
				+'<input type="file" name="image_url_'+id+'" placeholder="Thay ảnh" />'
			+'</td>'
			+'<td>'
				+'<input type="text" required="required" name="images['+id+'][name]" value="Hình ảnh mới" class="name-input" />'
			+'</td>'
			+'<td>'
				+'<select name="images['+id+'][type]">'
					+'<option value="OTHER" selected>Hình ảnh khác</option>'
				+'</select>'
			+'</td>'
			+'<td>'
				+'<input type="text" required="required" class="sort_index" name="images['+id+'][sort_index]" value="0" />'
			+'</td>'
			+'<td>'
				+'<input type="hidden" name="images['+id+'][is_add]" value="1"/>'
				+'<p onclick="delete_new_row(\''+id+'\')" id="delete-image">Xóa</p>'
			+'</td>'
		+'</tr>';
		$('#table_images').append(html);
	}
</script>
<style>
.table{
	width: 800px;
	border: thin solid #ccc;
}
.table tr td,th {
	border: thin solid #ccc;
	padding:5px;
}
th{
	background-color: #bde5f8;
	height: 30px;
}
.product-image {
	width: 80px;
}
#delete-image{
	color: red;
	cursor:pointer;
}
#add-image{
	color: blue;
	cursor:pointer;
}
.sort_index{
	width:50px;
	text-align:center;
}
.name-input {
	width:150px;
}

</style>
