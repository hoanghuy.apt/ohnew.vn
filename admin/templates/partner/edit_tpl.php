﻿<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<h3>Thông tin đối tác</h3>

<?php $flash->display(); ?>

<form name="frm" method="post" action="<?=url(null, 'edit')?>" enctype="multipart/form-data" class="nhaplieu">

    <b>Tên: </b><input type="text" name="name" value="<?=@$item['name']?>" class="input" /><br />

    <?php if (isset($item['id'])) { ?>
        <b>Hình hiện tại:</b><img src="<?=_upload_partner . $item['logo']?>" alt="NO PHOTO"  width="150"/><br />
    <?php } ?>

    <b>Logo<span style="color: red;">*</span> : </b> <input type="file" name="logo" /> <br /><br />

    <b>Liên kết đích: </b><input type="text" name="target_link" value="<?=@$item['target_link']?>" class="input" /><br />
	
    <input type="hidden" name="id" id="id" value="<?= @$item['id'] ?>" />
    <input type="submit" value="Lưu" class="btn" />
    <input type="button" value="Thoát" onclick="javascript:window.location = '<?=router('admin', ['com' => 'partner'])?>'" class="btn" />
</form>