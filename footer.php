
<footer>
  <div class="footer-top">
    <div class="container container_index">
      <div class="info-footer">
        <div class="des-info-footer"><?php echo $gioithieu_lienhe; ?></div>
      </div>
    </div>
  </div>
  <div class="footer-content">
    <div class="container container_index text-center" > <?php echo $timkiem_lienhe; ?> </div>
  </div>
  <div class="footer-bottom">
    <div class="container container_index">
      <div class="row">
        <div class="footercol1 col-sm-4 col-xs-12">
           <!-- 
		  <div class="footerbox1">
            <h2>MỘT PHÚT HIỂU THÊM VỀ <?php echo $tieude_lienhe; ?></h2>
            <div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" style="max-width:100%;" src="<?php echo $you_lienhe; ?>"></iframe>
			</div>
          </div>
		   -->
          <div class="footerbox4">
            <div class="aboutUs clearfix">
              <h3>THEO DÕI <?php echo $tieude_lienhe; ?></h3>
              <p></p>
              <ul>
                <li class="fb-click-modal"><a href="javascript:;" data-toggle="modal" data-target="#fbModal"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?php echo $instagram_lienhe; ?>"><i class="fa fa-instagram"></i></a></li>
                <li><a href="<?php echo $google_lienhe; ?>"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footercol2 col-sm-4 col-xs-12">
          <?php 
		$sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and footer=1 order by uutien limit 0,2");
		if(mysql_num_rows($sql_1)>0){
			$res_1 = mysql_fetch_object($sql_1);
			do{
				$sql = mysql_query('select * from table_nhomtin where parentid='.$res_1->id.' order by uutien limit 0,6');
?>
          <div class="footerbox2">
            <h3 class="quick-link-title"><?php echo $res_1->loaitin; ?></h3>
            <?php if(mysql_num_rows($sql)>0){ ?>
            <ul class="list-quick-link">
              <?php $res=mysql_fetch_object($sql); do{ ?>
              <li> <a href="<?php echo $res->url; ?>/"><?php echo $res->loaitin; ?></a> </li>
              <?php }while($res=mysql_fetch_object($sql)); ?>
            </ul>
            <?php } ?>
          </div>
          <?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>
        </div>
        <div class="footercol3 col-sm-4 col-xs-12">
          <div class="footerbox3">
            <ul class="footer-support hidden-xs">
              <li class="support"> <a href="#baogia_popup" class="open-popup-link"> <span class="quote sp">Báo giá</span> </a> <a href="pages/lien-he.html"> <span class="call sp">Liên hệ</span> </a> </li>
            </ul>
            <ul class="footer-contact">
              <li class="hotline"><a href="tel:<?php echo $hotline_lienhe; ?>"><?php echo $hotline_lienhe; ?></a></li>
              <li class="mail"><a href="mailto:<?php echo $email_lienhe; ?>"><?php echo $email_lienhe; ?></a></li>
            </ul>
            <a href="index.html"><img class="img-responsive" src="images/bct.png" /></a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyRight">
    <div class="container">
		<div class="col-sm-4 col-xs-12"><a href="index.html"><img alt="<?php echo $title_s; ?>" class="imglogo" src="media/upload/slide/<?php echo $hinh_logo_6; ?>"></a></div>
		<div class="col-sm-8 col-xs-12" style="text-align:left;">
			<?php echo $tomtat_lienhe; ?>
		</div>
	</div>
  </div>
</footer>
<div class="modal fade" id="fbModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Theo dõi <?php echo $tieude_lienhe; ?></h4>
      </div>
      <div class="modal-body"> <?php echo $baivietface_lienhe; ?> </div>
    </div>
  </div>
</div>
<nav id="my-menu" class="">
  <ul>
    <li> <a href="dich-vu.html">Dịch vụ</a>
      <ul>
        <?php
	$sql = mysql_query('select * from table_tbmang order by uutien');
	if(mysql_num_rows($sql)>0){ $res=mysql_fetch_object($sql);  do{
 ?>
        <li><a href="dich-vu/<?php echo $res->url; ?>"><?php echo $res->ten; ?></a></li>
        <?php }while($res=mysql_fetch_object($sql)); } ?>
      </ul>
    </li>
    <li> <a href="product.html">Sản phẩm</a>
      <ul>
        <?php 
		$sql_1 = mysql_query('select * from table_nhommuc where parentid=0');
		if(mysql_num_rows($sql_1)>0){ $res_1=mysql_fetch_object($sql_1);
			do{	
				$sql = mysql_query('select * from table_sanpham where id_cap_mot='.$res_1->id.' and spnoibat=1 and publish = 1 order by id desc limit 0,10');
?>
        <li><a href="product/<?php echo $res_1->url; ?>"><?php echo $res_1->loaitin; ?></a>
          <?php if(mysql_num_rows($sql)>0){ ?>
          <ul>
            <?php $res = mysql_fetch_object($sql); do{ ?>
            <li><a href="product/<?php echo $res->url ;?>.html"><?php echo $res->tensp;?></a></li>
            <?php }while($res = mysql_fetch_object($sql)); ?>
          </ul>
          <?php } ?>
        </li>
        <?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>
      </ul>
    </li>
    <li> <a href="thiet-ke-goi-y.html">Thiết kế gợi ý</a>
      <ul>
        <?php 
		$sql_1 = mysql_query('select * from table_hinhdaidien order by id limit 0,2');
		if(mysql_num_rows($sql_1)>0){
			$res_1=mysql_fetch_object($sql_1);
			do{				
?>
        <li><a href="thiet-ke-goi-y/<?php echo $res_1->url; ?>"><?php echo $res_1->ten; ?></a> </li>
        <?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>
      </ul>
    </li>
    <?php 
		$sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and menutren=1 order by uutien limit 0,1");
		if(mysql_num_rows($sql_1)>0){
			$res_1 = mysql_fetch_object($sql_1);
				$sql = mysql_query('select * from table_nhomtin where parentid='.$res_1->id.' order by uutien limit 0,4');
?>
    <li> <a href="<?php echo $res_1->url; ?>/"><?php echo $res_1->loaitin; ?></a>
      <?php if(mysql_num_rows($sql)>0){ ?>
      <ul>
        <?php $res=mysql_fetch_object($sql); do{ ?>
        <li><a href="<?php echo $res->url; ?>/"><?php echo $res->loaitin; ?></a> </li>
        <?php }while($res=mysql_fetch_object($sql)); ?>
      </ul>
      <?php } ?>
    </li>
    <?php } ?>
    <li> <a href="shop-online.html">Shop online</a>
      <ul>
        <?php 
		$sql_1 = mysql_query('select * from table_nhommuc where parentid=0');
		if(mysql_num_rows($sql_1)>0){ $res_1=mysql_fetch_object($sql_1);
			do{	
?>
        <li> <a href="product/<?php echo $res_1->url; ?>"><?php echo $res_1->loaitin; ?></a> </li>
        <?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>
      </ul>
    </li>
    <?php 
		$sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and menutren=1 order by uutien limit 1,10");
		if(mysql_num_rows($sql_1)>0){
			$res_1 = mysql_fetch_object($sql_1);
			do{
				$sql = mysql_query('select * from table_nhomtin where parentid='.$res_1->id.' order by uutien limit 0,4');
?>
    <li> <a href="<?php echo $res_1->url; ?>/"><?php echo $res_1->loaitin; ?></a>
      <?php if(mysql_num_rows($sql)>0){ ?>
      <ul>
        <?php $res=mysql_fetch_object($sql); do{ ?>
        <li><a href="<?php echo $res->url; ?>/"><?php echo $res->loaitin; ?></a> </li>
        <?php }while($res=mysql_fetch_object($sql)); ?>
      </ul>
      <?php } ?>
    </li>
    <?php }while($res_1 = mysql_fetch_object($sql_1)); } ?>
  </ul>
</nav>
<div class="mobi-bar visible-xs">
  <div class="col-xs-6"> <a href="#baogia_popup" class="mb-open-popup-link btn" style="border-right: 1px solid #ccc">Báo giá</a> </div>
  <div class="col-xs-6"> <a href="lien-he.html" class="btn">Liên hệ</a> </div>
</div>
</div>
<div id="baogia_popup" class="white-popup mfp-hide">
  <div class="col-sm-6 hidden-xs"> <img src="media/upload/slide/<?php echo $hinh_logo_5; ?>" alt="<?php echo $title_s; ?>"> </div>
  <div class="col-sm-6 col-xs-12">
    <h3>Bạn có thể liên hệ với <?php echo $tieude_lienhe; ?> theo 2 cách:</h3>
    <br/>
    <p><strong>Cách 1:</strong> Gọi cho chúng tôi để được tư vấn ngay: <?php echo $hotline_lienhe; ?></p>
    <a class="callbackfree-button" href="tel:<?php echo $hotline_lienhe; ?>">Gọi Ngay!</a> <br/>
    <hr/>
    <p><strong>Cách 2:</strong> Điền số điện thoại, chúng tôi sẽ gọi lại tư vấn cho bạn ngay sau ít phút.</p>
    <div id="getfly-optin-form-iframe-1541746999055">
      <form id="getfly-form" class="getfly-form">
        <div class="getfly-row">
          <label class="getfly-label getfly-label-c">Tên công ty / Tên khách hàng<span class="getfly-span getfly-span-c">*</span></label>
          <input type="input" id="txtTenkhachhangBaogia" class="getfly-input " name="account_name" placeholder="Tên *">
        </div>
        <div class="getfly-row">
          <label class="getfly-label getfly-label-c">Điện thoại<span class="getfly-span getfly-span-c">*</span></label>
          <input type="text" name="account_phone" id="txtDienthoaikhachhangBaogia" class="getfly-input " placeholder="Số điện thoại *">
        </div>
        <div class="getfly-mt10 getfly-btn"> <a id="btnkhachhangBaogia" class="getfly-button getfly-button-bg ">Gọi Lại Cho Tôi Ngay!</a> </div>
      </form>	  
      <style>
		.getfly-form {
    max-width: 450px!important;
    margin: 0 auto;
    font-size: 12px;
    line-height: 30px!important;
    color: #777;
    font-family: sans-serif;
    background: #fff !important;
}.getfly-row {
    margin-bottom: 5px;
}.getfly-label-c {
    color: #777 !important;
}.getfly-input {
    width: 94%!important;
    border: 1px solid #CCC;
    background: #FFF;
    outline: 0;
    height: 34px;
    border-radius: 10px;
    padding: 0 10px 0 10px;
}.getfly-form input, select, textarea {
    border: 1px solid #ccc !important;
}.getfly-mt10 {
    margin-top: 10px;
}.getfly-button {
    cursor: pointer;
    border: none;
    background: #f58632;
    color: #FFF;
    padding: 10px 20px;
    font-size: 18px;
    outline: 0;
    font-family: sans-serif;
}.getfly-button {
    background: #04AFEF !important;
    font-size: 15px;
    width: 100%;
    display: inline-block;
    padding: 3px 0 3px 0;
    text-align: center;
    border-radius: 10px;
}.getfly-label {
    display: none!important;
}.getfly-button:hover {
    background: #39c3f7 !important;
}
		</style>
    </div>
  </div>
</div>
<div id="hoptacdaily_popup" class="white-popup mfp-hide">
  <div>
    <h4>NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ</h4>
    <div class="form-group">
      <input required type="text" id="txtTenDaily"  class="form-control" name="name-guest"  placeholder="Tên" value="">
    </div>
    <span class="error-message hidden">Vui lòng nhập số điện thoại hợp lệ gồm 10 hoặc 11 chữ số!</span>
    <div class="form-group">
      <input required type="text" id="txtDienthoaiDaily"  class="form-control" name="num-phone" placeholder="Số điện thoại" value="">
    </div>
    <div class="form-group">
      <input required type="email" id="txtEmailDaily"  class="form-control" name="mail" placeholder="Nhập địa chỉ email" value="">
    </div>
    <a id="btnDaily" class="daily-send btn orange_btn">Gửi !</a> </div>
</div>
<script src='919/1000032919/1000192486/jquery.magnific-popup.mindcde.js?v=468' type='text/javascript'></script>
<script src='919/1000032919/1000192486/jquery.fancyboxdcde.js?v=468' type='text/javascript'></script>
<script src='0/0/global/design/plugins/fancybox/jquery.mousewheel-3.0.6.js' type='text/javascript'></script>
<script>
$(document).ready(function(){
	if($(window).width() > 960){
		$(window).scroll(function(){
			if($(this).scrollTop() > $('header').height()){
				$( "#menuheaderfix" ).addClass( "topmainmenu" );
				$( "#alo-fixed" ).addClass( "show" );

			}else{
				$( "#menuheaderfix" ).removeClass( "topmainmenu" );
				$( "#alo-fixed" ).removeClass( "show" );
			}})
	}
	var owl = $(".customer-slider"); 
	owl.owlCarousel({
		itemsCustom : [
			[0, 1],
			[480, 1],
			[600, 1]
		],
		lazyLoad: true,
		pagination:	true
	});

<?php 
		$sql_1 = mysql_query("select * from table_nhomtin where parentid=0 and duoi=1 order by uutien");
		if(mysql_num_rows($sql_1)>0){
			$res_1 = mysql_fetch_object($sql_1);
			do{
?>
	var feedback_index_<?php echo $res_1->id; ?> = $('.feedback_index_<?php echo $res_1->id; ?>');
	feedback_index_<?php echo $res_1->id; ?>.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 2],
			[768, 3]
		],
		lazyLoad: true,
		navigation:true,
		navigationText: [	"",	""],
		pagination:	true
	});
<?php }while($res_1 = mysql_fetch_object($sql_1)); } ?> 

	var owl_contactus_block = $('.contactus_block');
	owl_contactus_block.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 1],
			[768, 1]
		],
		autoPlay: 3000,
		lazyLoad: true,
		autoHeight : true,
		navigation:true,
		navigationText: [	"",	""],
		pagination:	true
	});

	var owl_img_feedback = $('.list_img_feedback');
	owl_img_feedback.owlCarousel({
		itemsCustom : [
			[0, 1],
			[500, 1],
			[768, 1]
		],
		autoPlay: 3000,
		stopOnHover: false,
		lazyLoad: true,
		navigation:true,
		navigationText: ["",""],
		pagination: true
	});


	/*
$(".slides").owlCarousel({
	navigation : true, 
	video:true,
	merge:true,
	slideSpeed : 300,
	paginationSpeed : 400,
	singleItem: true,
	pagination: false,
	rewindSpeed: 500
});
*/

	$(".fancybox").fancybox({
		padding:0,
		margin:15,
		scrolling : 'yes'
	});
	$('.open-popup-link, .mb-open-popup-link').magnificPopup({
		type:'inline',
		midClick: true
	});

	$('#alo-fixed .open-popup-link').hover(function(){
		$(this).trigger('click');
	});



});


$(document).ready(function(){
	$('.articlelistOther h3, .articlelistCate h3').append('<div class="openc">+</div>');
	$('.articlelistOther ul li a.current, .articlelistCate ul li a.current').parents().eq(1).css("display","block");
	$('.articlelistOther ul li a.current, .articlelistCate ul li a.current').parents().eq(2).find(".openc").css("display","none");
	$('.articlelistOther h3 .openc, .articlelistCate h3 .openc').click(function(){  
		var text = $(this).text()
		if(text == '+'){
			$(this).parents().eq(1).find('ul').slideToggle(500);
			$(this).text('-');
		}else{
			$(this).parents().eq(1).find('ul').slideToggle(500);
			$(this).text('+');  
		}
	});
});

</script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<a href="tel:<?php echo $hotline_lienhe; ?>" class="visible-xs phone-ring">
<div class="laziweb-alo-phone">
  <div class="animated infinite zoomIn laziweb-alo-ph-circle"></div>
  <div class="animated infinite pulse laziweb-alo-ph-circle-fill"></div>
  <div class="animated infinite tada laziweb-alo-ph-img-circle"></div>
</div>
</a>
<div class="hidden-xs quickcontact phone-ring"> <a href='tel:<?php echo $hotline_lienhe; ?>'>
  <div class="laziweb-alo-phone">
    <div class="animated infinite zoomIn laziweb-alo-ph-circle"></div>
    <div class="animated infinite pulse laziweb-alo-ph-circle-fill"></div>
    <div class="animated infinite tada laziweb-alo-ph-img-circle"></div>
  </div>
 <!-- 
  <div class="hot-line-wrap">
    <strong class="hidden-xs"><?php echo $hotline_lienhe; ?></strong>
  </div>
  -->
  </a> </div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#btnkhachhangBaogia').click(function(e){
			e.preventDefault();
			
			var txtFullname = jQuery('#txtTenkhachhangBaogia');
			var txtTitle = jQuery('#txtDienthoaikhachhangBaogia');
			var ajax_loading = jQuery('#ajax_loading');
			
			if(txtFullname.val()==""){alert("Bạn chưa nhập tên"); txtFullname.focus();}
						else if(txtTitle.val()==""){alert("Bạn chưa nhập số điện thoại"); txtTitle.focus();}
						
			else{
			
					var UrlToPass = 'action=gui_baogia&lienhe_hoten='+txtFullname.val()+'&lienhe_tieude='+txtTitle.val();
							//console.log(UrlToPass);
							ajax_loading.removeClass('hidden');
							jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
								type : 'POST',
								data : UrlToPass,
								url  : 'lienhe_xuly.php',
								success: function(responseText){ // Get the result and asign to each cases
									//alert(responseText);
									if(responseText == 1){ ajax_loading.addClass('hidden');alert("Liên hệ Báo giá thành công, cảm ơn bạn!");
										window.location = "index.html";}
									else if(responseText == 4){ ajax_loading.addClass('hidden');alert("có lỗi trong quá trình liên hệ báo giá, hãy thử lại");}
								}
							});
							
						}				
			
		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#btnDaily').click(function(e){
			e.preventDefault();
			
			var txtFullname = jQuery('#txtTenDaily');
			var txtEmailInput = jQuery('#txtEmailDaily');
			var txtTitle = jQuery('#txtDienthoaiDaily');
			var ajax_loading = jQuery('#ajax_loading');
			
			if(txtFullname.val()==""){alert("Bạn chưa nhập tên"); txtFullname.focus();}
						else if(txtEmailInput.val()==""){alert("Bạn chưa nhập email"); txtEmailInput.focus();}
						
			else{
			
					var UrlToPass = 'action=gui_daily&lienhe_hoten='+txtFullname.val()+'&lienhe_email='+txtEmailInput.val()+'&lienhe_tieude='+txtTitle.val();
							//console.log(UrlToPass);
							ajax_loading.removeClass('hidden');
							jQuery.ajax({ // Send the credential values to another checker.php using Ajax in POST menthod
								type : 'POST',
								data : UrlToPass,
								url  : 'lienhe_xuly.php',
								success: function(responseText){ // Get the result and asign to each cases
									//alert(responseText);
									if(responseText == 2){
										ajax_loading.addClass('hidden');
										alert("Email không hợp lệ, vui lòng nhập lại");
										txtEmailInput.focus();
									}
									else if(responseText == 1){ ajax_loading.addClass('hidden');alert("Liên hệ NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ thành công, cảm ơn bạn!");
										window.location = "index.html";}
									else if(responseText == 4){ ajax_loading.addClass('hidden');alert("có lỗi trong quá trình liên hệ NHẬN CHÍNH SÁCH VÀ BẢNG GIÁ SỈ, hãy thử lại");}
								}
							});
							
						}				
			
		});
	});
</script>
<style type="text/css">
	.content img{height:inherit!important;}
</style>
<noscript id="deferred-styles">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="0/0/global/design/css/bootstrap.3.3.1.css">
<link rel="stylesheet" type="text/css" href="0/global/design/member/fonts/opensans.css">
<link href="919/1000032919/1000192486/font-aothundcde.css?v=468" rel="stylesheet" type="text/css">
<link href='0/0/global/design/plugins/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/owl.carouseldcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/flat-uidcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/collectiondcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/stylesdcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/jquery.fancyboxdcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/responsivedcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/magnific-popupdcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
<link href='919/1000032919/1000192486/sweetalertdcde.css?v=468' rel='stylesheet' type='text/css'  media='all'  />
</noscript>
<script>
      var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
      };
      var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
          window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
      if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
      else window.addEventListener('load', loadDeferredStyles);
    </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d9ea1c0f82523213dc6889d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->	