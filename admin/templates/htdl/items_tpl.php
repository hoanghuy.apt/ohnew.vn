<h3><a href="index.php?com=htdl&act=add">Thêm mới</a></h3>
<!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- DataTables -->
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	
<table id="example1" class="table table-bordered table-striped">
	<thead>
    <tr>
       <th style="width:1%;">Stt</th>
        <th style="width:6%;">Tên</th>
        <th style="width:6%;">chức vụ</th>
		<th style="width:6%;">Hình</th>
        <th style="width:6%;">Sửa</th>
        <th style="width:6%;">Xóa</th>
    </tr>
		</thead>
	<tbody>	
    <?php for ($i = 0, $count = count($items); $i < $count; $i++) { ?>
        <tr>
            <td style="width:1%;" align="center"><?= $items[$i]['id'] ?></td>
            <td style="width:6%;"><?= $items[$i]['ten'] ?></td>
            <td style="width:6%;"><?= $items[$i]['sdt'] ?></td>
			<td style="width:12%;" align="center"><img src="<?= _upload_gioithieu . $items[$i]['hinhanh'] ?>"  width="200px" title="Ảnh minh họa" alt="Chưa cập nhật hình"> </td>
            <td style="width:1%;"><a href="index.php?com=htdl&act=edit&id=<?= $items[$i]['id'] ?>"><img src="media/images/edit.png"  border="0"/></a></td>
            <td style="width:1%;"><a href="index.php?com=htdl&act=delete&id=<?= $items[$i]['id'] ?>" onClick="if (!confirm('Xác nhận xóa'))
                        return false;"><img src="media/images/delete.png" border="0" /></a></td>
        </tr>
<?php } ?>
	</tbody>
</table>

	<script>
      $('#example1').DataTable( {
        	"order": [[ 0, "desc" ]]
   		 } );
    </script>