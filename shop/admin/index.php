<?php
session_start();
@define('_template', './templates/');
@define('_source', './sources/');
@define('_lib', './lib/');

include_once _lib . "config.php";
include_once _lib . "constant.php";
include_once _lib . "functions.php";
include_once _lib . "library.php";
include_once _lib . "class.database.php";

$com = (isset($_REQUEST['com'])) ? addslashes($_REQUEST['com']) : "";
$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";
$login_name = 'AMTECOL';

$d = new database($config['database']);
/* Link den cac muc trong phan admin, com bien */
switch ($com) {
	
	case 'tailieu':
        $source = "tailieu";
        break;	
	
	case 'tbmang':
        $source = "tbmang";
        break;
		
	case 'popup':
        $source = "popup";
        break;
		
	case 'gamenet':
        $source = "gamenet";
        break;	
		
	case 'chuchay':
        $source = "chuchay";
        $template = "chuchay";
        break;
	
	case 'bannertren':
        $source = "bannertren";
        $template = "bannertren";
        break;
		
	case 'albumkh':
        $source = "albumkh";
        break;
		
	case 'baivietlienhe':
        $source = "baivietlienhe";
        break;	
	
    case 'an':
        $source = "anhnhieu";
        $template = "anhnhieu";
        break;
    case 'menu_doc':
        $source = "menu_doc";
        $template = "menu_doc";
        break;
		
    case 'user':
        $source = "user";
        break;
		
	case 'traodoinoibo':
        $source = "traodoinoibo";
        break;	
		
	 case 'tuvan':
        $source = "tuvan";
        break;	
		
    case 'thanhtich':
        $source = "thanhtich";
        break;
	
    case 'htkd':
        $source = "htkd";
        break;
    
	case 'hinhdaidien':
        $source = "hinhdaidien";
        break;
	
    case 'htkt':
        $source = "htkt";
        break;
		
	case 'htdl':
        $source = "htdl";
        break;
	case 'htbh':
        $source = "htbh";
        break;
	case 'bh':
        $source = "bh";
        break;
	case 'htpg':
        $source = "htpg";
        break;
	
	case 'donhang':
        $source = "donhang";
        break;
		
    case 'hotline':
        $source = "hotline";
        break;
    case 'fter':
        $source = "fter";
        break;
    
    case 'nhomtin':
        $source = "nhomtin";
        break;
    case 'sp':
        $source = "sanpham";
        break;
    case 'hang':
        $source = "hangsx";
        break;
	case 'noidunghangsx':
        $source = "noidunghangsx";
        break;
	
    case 'kg':
        $source = "khoanggia";
        break;
    case 'hdh':
        $source = "hedh";
        break;
    case 'sl':
        $source = "sl";
        break;
    case 'lk':
        $source = 'linhkien';
        break;
    case 'nlk':
        $source = "nhomlk";
        break;
    case 'khachhang':
        $source = "khachhang";
        break;
    case 'dattour':
        $source = "dattour";
        break;
    case 'lienhe':
        $source = "lienhe";
        break;
    case 'loaitour':
        $source = "loaitour";
        break;
    case 'chitiet_dattour':
        $source = "chitiet_dattour";
        break;
    case 'dsachkh_dat_tour':
        $source = "dsachkh_dat_tour";
        break;
    case 'bannerqc':
        $source = "bannerqc";
        break;
    case 'tintuc':
        $source = "tintuc";
        break;
    case 'chuong_trinh_tour':
        $source = "chuong_trinh_tour";
        break;
    case 'doitac':
        $source = "doitac";
        break;
    case 'gioithieu':
        $source = "gioithieu";
        break;
	case 'dichvu':
        $source = "dichvu";
        break;
	case 'congtrinhtieubieu':
        $source = "congtrinhtieubieu";
        break;
	case 'congtrinhhoanthien':
        $source = "congtrinhhoanthien";
        break;
	
    case 'tuyendung':
        $source = "tuyendung";
        break;
	case 'doitac':
        $source = "doitac";
        break;	
	case 'khuyenmai':
        $source = "khuyenmai";
        break;
	case 'quydinhkhuyenmai':
        $source = "quydinhkhuyenmai";
        break;	
		
	case 'chitiet_menu':
        $source = "chitiet_menu";
        break;	
	
	
    case 'khachsan':
        $source = "khachsan";
        break;
    case 'footer':
        $source = "footer";
        break;
    default:
        $source = "";
        $template = "index";
        break;
}

if ((!isset($_SESSION[$login_name]) || $_SESSION[$login_name] == false) && $act != "login") {
    redirect("index.php?com=user&act=login");
}

if ($source != "")
    include _source . $source . ".php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/DTD/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Website Administration</title>
        <link href="media/css/style.css" rel="stylesheet" type="text/css" />
    </head>
	
	
	<script language="JavaScript" type="text/javascript" src="./ckeditor/ckeditor.js"></script>
	<script language="JavaScript" type="text/javascript" src="./ckeditor/sample.js"></script>
	
	

	<!----quoc---->
    <link rel="stylesheet" href="muti/uploadifyit/uploadify.css" type="text/css" />
	<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="muti/uploadifyit/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="muti/uploadifyit/swfobject.js"></script>
    <script type="text/javascript" src="muti/uploadifyit/jquery.uploadify.v2.1.4.min.js"></script>
	
    <body>

            <?php if (isset($_SESSION[$login_name]) && ($_SESSION[$login_name] == true)) { ?>  
            <div id="wrapper">
    <?php include _template . "header_tpl.php" ?>

                <div id="main"> 

                    <!-- Right col -->
                    <div id="contentwrapper">
                        <div id="content">
    <?php include _template . $template . "_tpl.php" ?>
                        </div>
                    </div>
                    <!-- End right col -->

                    <!-- Left col -->
                    <div id="leftcol">
                        <div class="innertube">
    <?php include _template . "menu_tpl.php"; ?>
                        </div>
                    </div>
                    <!-- End Left col -->

                    <div class="clr"></div>
                </div>
                <div id="footer">
    <?php include _template . "footer_tpl.php" ?>
                </div>
            </div>
        <?php } else { ?>   
            <?php include _template . $template . "_tpl.php" ?>
<?php } ?>
    </body>
</html>
