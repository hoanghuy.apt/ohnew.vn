<?php

if (!defined('_source'))
    die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "create":
		get_item();
        $template = "ad/index";
        break;
    case "save":
        save_item();
        break;
    default:
        $template = "index";
}
function get_item() {
    global $d, $item;
    $sql = "select * from table_ad where id =1";
    $d->query($sql);
    if ($d->num_rows() == 0){
        transfer("Dữ liệu không có thực", "index.php");
	}
    $item = $d->fetch_array();
}

function fns_Rand_digit($min, $max, $num) {
    $result = '';
    for ($i = 0; $i < $num; $i++) {
        $result.=rand($min, $max);
    }
    return $result;
}

function save_item() {
    global $d;
    if (empty($_POST))
        transfer("Không nhận được dữ liệu", "index.php?com=bh&act=man");
    $id = isset($_POST['id']) ? themdau($_POST['id']) : "";
    if ($id) {//cap nhat
        $data['top_ad_link'] = $_POST['top_ad_link'];
        $data['middle_ad_link'] = $_POST['middle_ad_link'];
        $data['middle_ad_link2'] = $_POST['middle_ad_link2'];
		
		$d->setTable('ad');
        $d->setWhere('id', $id);
		$d->select();
        if ($d->num_rows() > 0) {
			$row = $d->fetch_array();
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("top_ad_image", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_ad, $file_name)) {
				$data['top_ad_image'] = $photo;
				delete_file(_upload_ad . $row['top_ad_image']);
			}
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("middle_ad_image", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_ad, $file_name)) {
				$data['middle_ad_image'] = $photo;
				delete_file(_upload_ad . $row['middle_ad_image']);
			}
			
			$file_name = fns_Rand_digit(0, 9, 8);
			if ($photo = upload_image("middle_ad_image2", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG', _upload_ad, $file_name)) {
				$data['middle_ad_image2'] = $photo;
				delete_file(_upload_ad . $row['middle_ad_image2']);
			}
		}
        if ($d->update($data))
            redirect("index.php?com=ad&act=create");
        else
            transfer("Cập nhật dữ liệu bị lỗi", "index.php?com=bh&act=man");
    }
}

function delete_item() {
    global $d;

    if (isset($_GET['id'])) {
        $id = themdau($_GET['id']);


        // xoa item
        $sql = "delete from #_bh where id='" . $id . "'";
        if ($d->query($sql))
            header("Location:index.php?com=bh&act=man");
        else
            transfer("Xóa dữ liệu bị lỗi", "index.php?com=bh&act=man");
    } else
        transfer("Không nhận được dữ liệu", "index.php?com=bh&act=man");
}
?>



