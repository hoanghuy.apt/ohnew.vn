
<div class="overlay-cart" ao="media/upload/slide/<?php echo $hinh_logo_1; ?>"></div>
<div class="load-cart-index">
  <div class="container">
    <form action="cart" method="post" id="cartform">
      <div id="table-cart">
        <div class="content-cart">
          <table class="table table-bordered">
            <tbody>
            </tbody>
          </table>
        </div>
        <div id="table-action" class="clearfix">
          <div class="col-md-3 col-sm-3 col-xs-6"> <a href="javascript:;" class="continue_shopping btn btn-lg">Tiếp tục mua sắm</a> </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="total-cart"> <a href="cart.html" class="btn-checkout">Thanh toán</a>
              <h4> <span>0₫</span> <small> Chưa bao gồm phí giao hàng </small> </h4>
            </div>
          </div>
        </div>
        <!-- #under-table -->
        <div class="clearfix"></div>
      </div>
    </form>
  </div>
</div>
<script>
function flyToElement(flyer, flyingTo) {
	var $func = $(this);
	var divider = 3;
	var flyerClone = $(flyer).clone();
	$(flyerClone).css({position: 'absolute', top: $(flyer).offset().top + "px", left: $(flyer).offset().left + "px", opacity: 1, 'z-index': 10000});
	$('body').append($(flyerClone));
	var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
	var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;

	$(flyerClone).animate({
		opacity: 0.4,
		left: gotoX,
		top: gotoY,
		width: $(flyer).width()/divider,
		height: $(flyer).height()/divider
	}, 1500,function () {
		$(flyingTo).fadeOut('fast', function () {
			$(flyingTo).fadeIn('fast', function () {
				$(flyerClone).fadeOut('fast', function () {
					$(flyerClone).remove();
				});
			});
		});
	});
}

function getCartView(){
	jQuery.getJSON('cart.json', function(cart, textStatus) {
		//$('.cart-count').html(cart.item_count);
		$('#shopping-bag-qty').html(cart.item_count);
		//$('.cart-price').html(Haravan.formatMoney(cart.total_price,"{{amount}}₫")+"đ");
		if($('.load-cart-index').length > 0){
			if(cart.item_count > 0){
				$('.load-cart-index #cartform').remove();
				var total_money = cart.total_price.toString();
				//var mon = parseFloat(total_money.substring(0,total_money.length -2));
				var mon = parseInt(cart.item_count);
				var cart_html = '';
				cart_html += '<div class="container">';
				cart_html += '<form action="/cart" method="post" id="cartform"> ';
				cart_html += '<div id="table-cart">';
				cart_html += '<div class="content-cart">';
				cart_html += '<table class="table table-bordered">';
				cart_html += '<tbody>';
				cart_html += '</tbody>';
				cart_html += '</table>';
				cart_html += '</div>';
				cart_html += '</div>';

				cart_html += '<div id="table-action" class="clearfix">';
				cart_html += '<div class="col-md-3 col-sm-3 col-xs-6">';
				cart_html += '<a href="javascript:;" class="continue_shopping btn btn-lg">Tiếp tục mua sắm';
				cart_html += '</a>';
				cart_html += '</div>';
				cart_html += '<div class="col-md-3 col-sm-3 col-xs-6 more1">';
				cart_html += '<div class="add-more1">';

				cart_html += '</div>';
				cart_html += '</div>';
				cart_html += '<div class="col-md-6 col-sm-6 col-xs-12">';
				cart_html += '<div class="total-cart clearfix">';
				cart_html += '<a href="/checkout" class="btn-checkout">Thanh toán';
				cart_html += '</a>';
				cart_html += '<h4><span>'+Haravan.formatMoney(cart.total_price,"{{amount}}₫")+'đ</span><small></small></h4>'
				cart_html += '</div>';
				cart_html += '</div>';
				cart_html += '</form>';
				cart_html += '</div>';
				$('.load-cart-index').html(cart_html);
				$.each(cart.items,function(i,item){
					clone_item(item);
				});
				
			} 
			else{
				$('.load-cart-index').html('<p class="text-center no-item">Giỏ hàng của bạn đang trống!</p>');
			}
		}
	});
}
function clone_item(item){
	var html = '';
	var variant = '';
	var img = item.image;
	if (img == null){
		img = '0/0/global/noDefaultImage6_thumb.gif';
	} else {
		img = Haravan.resizeImage(item.image,'small');
	}
	var title_variant = item.variant_options[0];
	var title_img = '';
	if(title_variant.indexOf('Xanh 676') != -1){
		title_img = 'xanh676';
	}
	if(title_variant.indexOf('Đen') != -1){
		title_img = 'den';
	}
	if(title_variant.indexOf('Xám đậm') != -1){
		title_img = 'xamdam';
	}
	if(title_variant.indexOf('Nâu đất') != -1){
		title_img = 'naudat';
	}
	if(title_variant.indexOf('Xanh biển') != -1){
		title_img = 'xanhbien';
	}
	if(title_variant.indexOf('Tím') != -1){
		title_img = 'tim';
	}
	if(title_variant.indexOf('Xanh Navy') != -1){
		title_img = 'xanhnavy';
	}
	if(title_variant.indexOf('Vàng kem') != -1){
		title_img = 'vangkem';
	}
	if(title_variant.indexOf('Vàng đậm') != -1){
		title_img = 'vangdam';
	}
	if(title_variant.indexOf('Rêu') != -1){
		title_img = 'reu';
	}
	if(title_variant.indexOf('Xanh bơ') != -1){
		title_img = 'xanhbo';
	}
	if(title_variant.indexOf('Xanh bích') != -1){
		title_img = 'xanhbich';
	}
	if(title_variant.indexOf('Xanh vịt đậm') != -1){
		title_img = 'xanhvitdam';
	}
	if(title_variant.indexOf('Xanh nhạt') != -1){
		title_img = 'xanhnhat';
	}
	if(title_variant.indexOf('xám nhạt') != -1){
		title_img = 'xamnhat';
	}
	if(title_variant.indexOf('Xanh ya') != -1){
		title_img = 'xanhya';
	}
	if(title_variant.indexOf('Cam') != -1){
		title_img = 'cam';
	}
	if(title_variant.indexOf('Xám CM30') != -1){
		title_img = 'xamcm30';
	}
	if(title_variant.indexOf('Xanh lá') != -1){
		title_img = 'xanhla';
	}
	if(title_variant.indexOf('Trắng') != -1){
		title_img = 'trang';
	}
	if(title_variant.indexOf('Huyết dụ') != -1){
		title_img = 'huyetdu';
	}
	if(title_variant.indexOf('Đỏ đô') != -1){
		title_img = 'dodo';
	}
	if(title_variant.indexOf('Đỏ') != -1){
		title_img = 'do';
	}
	if(title_variant.indexOf('Nâu CM30') != -1){
		title_img = 'naucm30';
	}
	if(title_variant.indexOf('Xanh dương') != -1){
		title_img = 'xanhduong';
	}
	if(title_variant.indexOf('Hồng') != -1){
		title_img = 'hong';
	}
	//console.log(img);
	
	
			 
			 img = Haravan.resizeImage(item.image,'small');
				


				html += '<tr id-data="' + item.variant_id + '">';
				 html += '<td class="cir-cancel">';
				 html += '<a class="remove-cart" data-id="' + item.variant_id + '" href="javascript:void(0);"><i class="fa fa-times-circle-o"></i></a>';
				 html += '</td>';

				 html += '<td class="product-name">';
				 html += '<ul>';
				 html += '<li>';
				 html += '<img class="img-cart" src="' + img + '" />';
				 html += '</li>';
				 html += '<li class="des">';
				 html += '<p>';
				 html += '<a href="'+ item.url +'">'+ item.title +'</a>';
				 html += '<span class="variant_title"> ('+item.variant_options+')</span>';
				 html += '</p>';
				 html += '</li>';
				 html += '</ul>';
				 html += '</td>';

				 html += '<td class="product-price hidden-xs">';
				 html += Haravan.formatMoney(item.price,'{{amount}}₫') +'đ';
				 html += '</td>';

				 html += '<td class="product-quality">';
				 html += '<input type="hidden" id="quality_available_'+ item.variant_id +'" value="10000">';
				 html += '<input type="hidden" id="quality_current_'+ item.variant_id +'" value="1">';

				 html += '<div class="js-qty">';
				 html += '<input type="text" class="js-qty__num" size="4" name="updates[]" id="updates_'+ item.variant_id +'" data-id="' + item.variant_id + '" value="'+item.quantity+'" onfocus="this.select();" class="qty" min="1"/>';
				 html += '<button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--minus" data-id="' + item.variant_id + '" data-qty="1">−</button>';
				 html += '<button type="button" class="btn btn-outline js-qty__adjust js-qty__adjust--plus" data-id="' + item.variant_id + '" data-qty="1000">+</button>';
				 html += '</div>';

				 html += '<div class="clearfix"></div>';
				 html += '</td>';

				 html += '<td class="total-product">';
				 html += '<p>'+ Haravan.formatMoney(item.line_price,'{{amount}}₫') +'đ</p>';
				 html += '</td>';
				 html += '</tr>';

				 $('.load-cart-index #cartform table tbody').append(html);
				}
				$(document).on("click",".remove-cart",function(){
					var index_view_cart = $(this).parents('tr').index();
					$(this).parents('tr').remove();  
					var variant_id = $(this).attr('data-id');
					var params = {
						type: 'POST',
						url: '/cart/change.js',
						data:  'quantity=0&id='+variant_id,
						dataType: 'json',
						success: function(cart) { 	
							if ( cart.item_count > 0 ) {
								var total_money = cart.total_price.toString();
								//var mon = parseFloat(total_money.substring(0,total_money.length -2));
								var mon = parseInt(cart.item_count);
								if(mon < 5){
									$('.load-cart-index .more').html('<div class="add-more"><span>Mua thêm để được giao hàng miễn phí</span></div>');
									$('.total-cart small').text('Chưa bao gồm phí giao hàng');
								}else{
									$('.load-cart-index .more').html('<div class="add-more"><span>Miễn phí vận chuyển</span></div>');
									$('.total-cart small').text('Miễn phí giao hàng');
								}
								$('#shopping-bag-qty').html(cart.item_count);
								$('.icon_bag-light').html(cart.item_count);
								$('.load-cart-index .total-cart span').html(Haravan.formatMoney(cart.total_price,'{{amount}}₫')+"đ");
								if ( window.location.pathname == '/cart' ){		
									$('tbody > tr').eq(index_view_cart).remove();
									$('#order-infor').find('b').html(Haravan.formatMoney(cart.total_price, "{{amount}}₫"));
								}
							}else {
								if ( window.location.pathname == '/cart' ){
									$('#cart .span12 .row').remove();
									$('#cart .span12').append($('.cart-null').clone());
								}
								$('#table-cart').html('');
								$('#shopping-bag-qty').html(cart.item_count);
								$('.icon_bag-light').html(cart.item_count);
								$('.load-cart-index').hide();
							}
						},
						error: function(XMLHttpRequest, textStatus) {
							Haravan.onError(XMLHttpRequest, textStatus);
						}
					};
					jQuery.ajax(params);
				});
				function Update_to_checkout(v,q){
					var total_monney = 0;
					var qty = 0;
					//debugger
					$('.load-cart-index table tr').each(function(){
						var inty = $(this).find('.total-product p').text().replace (',','').replace ('đ','');
						qty = qty + parseInt($(this).find('input.js-qty__num').val());
						//console.log(inty + '----2' );
						if(inty.indexOf(',' != -1)){
							inty = inty.replace (',','');
						}
						//console.log(inty + '----2' );
						total_monney+=parseFloat(inty);
					})
					
					var params = {
						type: 'POST',
						url: '/cart/change.js',
						data:  'quantity='+q+'&id='+v,
						dataType: 'json',
						success: function(cart) { 
							if ( cart.item_count > 0 ) {	
								$('.load-cart-index .total-cart span').html(Haravan.formatMoney(cart.total_price,'{{amount}}₫')+"đ");
							}
							$('#shopping-bag-qty').text(qty);
							$('.icon_bag-light').html(cart.item_count);
						},
						error: function(XMLHttpRequest, textStatus) {
							Haravan.onError(XMLHttpRequest, textStatus);
						}
					};
					jQuery.ajax(params);
				}
				/*$(document).on("click",".btn-checkout",function(){
		$('.load-cart-index table tr').each(function(){
			var variant = $(this).attr('id-data');
			var qty = $(this).find('.js-qty input').val();
			Update_to_checkout(variant,qty);
			window.location = '/checkout';
		})
	})*/
				$(document).on("click",".js-qty__adjust",function(){
					var $this = $(this)
					var gia = parseFloat($this.parents('tr').find('.product-price').text().replace (',',''));
					setTimeout(function(){
						var variant = $this.parents('tr').attr('id-data');
						var qty = $this.parent().find('input').val();
						var kq = (gia * qty) + '00';
						var t = Haravan.formatMoney(kq,"{{amount}}₫" + "đ");
						$this.parents('.product-quality').next().find('p').text(t);
						Update_to_checkout(variant,qty); 
					}, 500);
				})
				$(document).on("click",".continue_shopping, .overlay-cart",function(){
					$('.load-cart-index').hide();
					$('.overlay-cart').hide();
				})
				$(document).on("click","#shoppingBagBlock a",function(){
					$('.load-cart-index').show();
					$('.overlay-cart').show();
					jQuery('html, body').animate({
						scrollTop: 0
					}, 800);
				})

				</script>
<section id="off-canvas-nav" class="apollo-megamenu">
  <nav class="offcanvas-mainnav">
    <div id="off-canvas-button"> <span class="off-canvas-nav"><i class="fa fa-close"></i></span> Close </div>
    <ul class="nav navbar-nav megamenu">
      <li class="parent dropdown "> <a class="dropdown-toggle has-category" data-toggle="dropdown" href="product.html" title="Thể loại" target="_self"> <span class="">Thể loại</span><b class="caret"></b> </a>
        <div class="dropdown-menu level1">
          <div class="dropdown-menu-inner">
            <div class="mega-col-inner">
              <ul>
<?php 
		$sql_1 = mysql_query('select * from table_nhommuc where menuchinh=1');
		if(mysql_num_rows($sql_1)>0){ $res_1=mysql_fetch_object($sql_1);
			do{	
?> 			  
                <li class=""> <a class="" href="product/<?php echo $res_1->url; ?>" title="<?php echo $res_1->loaitin; ?>"> <span class=""><?php echo $res_1->loaitin; ?></span> </a> </li>
 <?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>				
              </ul>
            </div>
          </div>
        </div>
      </li>
<?php 
		$sql_1 = mysql_query('select * from table_htbh order by id');
		if(mysql_num_rows($sql_1)>0){ $res_1=mysql_fetch_object($sql_1);
			do{	
?>	  
      <li class=""> <a class="" href="<?php echo $res_1->sdt; ?>" title="<?php echo $res_1->ten; ?>" target="_self"> <span class=""><?php echo $res_1->ten; ?></span> </a> </li>
<?php	
			}while($res_1=mysql_fetch_object($sql_1));
		}
?>      
	  <li class=""> <a class="" href="lien-he.html" title="Liên hệ" target="_self"> <span class="">Liên hệ</span> </a> </li>
	</ul>
  </nav>
</section>
<script>
		$(document).ready(function(){
			$('.flypanels-container').flyPanels({
				treeMenu: {
					init: true
				},
			});
			FastClick.attach(document.body);

			var max_height = 0;
			$('.product-block img').each(function(){
				if($(this).outerHeight()> max_height){
					max_height = $(this).outerHeight();
				}
			});
			$('.product-block img').css('height',max_height);
		});
		</script>
<script>
		(function() {
			if(!$('iframe[src*=orderstrackingphone]').length) {
				return;
			}
			if(sessionStorage.getItem('tracking_phone')) {
				return;
			}
			var is_safari = navigator.userAgent.indexOf("Safari") > -1;
			if(!is_safari) {
				return;
			}
			sessionStorage.setItem('tracking_phone', 'fixsafari');
			var src = $('iframe[src*=orderstrackingphone]').attr('src');
			window.location.replace(src + '?fixsafari');

		}());
		</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>	