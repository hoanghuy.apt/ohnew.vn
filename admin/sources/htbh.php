<?php

if (!defined('_source'))
    die("Error");

$act = (isset($_REQUEST['act'])) ? addslashes($_REQUEST['act']) : "";

switch ($act) {
    case "man":
        get_items();
        $template = "htbh/items";
        break;
    case "add":
       	get_hinhdaidien();
        $template = "htbh/item_add";
        break;
    case "edit":
        get_item();
        get_hinhdaidien();
        $template = "htbh/item_add";
        break;
    case "save":
        save_item();
       
        break;
    case "delete":
        delete_item();
        break;

    default:
        $template = "index";
}

//doc cac muc menu cha va con
/* lay du lieu cho hai bang dat_tour va bang khach_hang */

function get_hinhdaidien() {
    global $d, $tbms;

    $sql = "select * from #_hinhdaidien order by id DESC";
    $d->query($sql);
    $tbms = $d->result_array();
}
function fns_Rand_digit($min,$max,$num)
{
    $result='';
    for($i=0;$i<$num;$i++){
        $result.=rand($min,$max);
    }
    return $result;
}
function get_items() // hien tat ca cac menu
{
   
    global $d, $items, $paging;

    $sql = "select * from #_htbh order by id DESC";
    $d->query($sql);
    $items = $d->result_array();

    $curPage = isset($_GET['curPage']) ? $_GET['curPage'] : 1;

    $url = "index.php?com=htbh&act=man";
    $maxR = 10;
    $maxP = 4;
    $paging = paging($items, $url, $curPage, $maxR, $maxP);
    $items = $paging['source'];
}

function get_item() {
    global $d, $item;
    $id = isset($_GET['id']) ? themdau($_GET['id']) : "";
    if (!$id)
        transfer("Không nhận được dữ liệu", "index.php?com=htbh&act=man");

    $sql = "select * from #_htbh where id ='" . $id . "'";
    $d->query($sql);
    if ($d->num_rows() == 0)
        transfer("Dữ liệu không có thực", "index.php?com=htbh&act=man");
    $item = $d->fetch_array();
}

function save_item() {
    global $d;
	$file_name=fns_Rand_digit(0,9,8);
    if (empty($_POST))
        transfer("Không nhận được dữ liệu", "index.php?com=htbh&act=man");
    $id = isset($_POST['id']) ? themdau($_POST['id']) : "";
    if ($id) {//cap nhat
		
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
            $d->setTable('htbh');
            $d->setWhere('id', $id);
            $d->select();
            if($d->num_rows()>0){
                $row = $d->fetch_array();
                delete_file(_upload_gioithieu.$row['hinhanh']);
            }
        }
		
		$file_name=fns_Rand_digit(0,9,8);
		if($hinhanh1 = upload_image("file1", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh1'] = $hinhanh1;
            $d->setTable('htbh');
            $d->setWhere('id', $id);
            $d->select();
            if($d->num_rows()>0){
                $row = $d->fetch_array();
                delete_file(_upload_gioithieu.$row['hinhanh1']);
            }
        }
		
        $data['ten'] = $_POST['ten'];
		$data['id_menu'] = $_POST['id_menu'];
        //$data['sdt'] = $_POST['sdt'];
		
		
        $d->setTable('htbh');
        $d->setWhere('id', $id);
        if ($d->update($data))
            redirect("index.php?com=htbh&act=man");
        else
            transfer("Cập nhật dữ liệu bị lỗi", "index.php?com=htbh&act=man");
    }else {//them moi
		if($hinhanh = upload_image("file", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh'] = $hinhanh;
        }
		
		$file_name=fns_Rand_digit(0,9,8);
		if($hinhanh1 = upload_image("file1", 'jpg|png|gif|jpeg|JPEG|PNG|GIF|JPG',_upload_gioithieu,$file_name)){
            $data['hinhanh1'] = $hinhanh1;
        }
	
        $data['ten'] = $_POST['ten'];
		$data['id_menu'] = $_POST['id_menu'];
		
        $d->setTable('htbh');
        if ($d->insert($data))
            redirect("index.php?com=htbh&act=man");
        else
            transfer("Lưu dữ liệu bị lỗi", "index.php?com=htbh&act=man");
    }
}

function delete_item() {
    global $d;

    if (isset($_GET['id'])) {
        $id = themdau($_GET['id']);
		$d->reset();
		$sql = "select * from #_htbh where id='" . $id . "'";
		
        $d->query($sql);
        if ($d->num_rows() > 0) {
            while ($row = $d->fetch_array()) {
                delete_file(_upload_gioithieu . $row['hinhanh']);
				delete_file(_upload_gioithieu . $row['hinhanh1']);
            }
            $sql = "delete from #_htbh where id='" . $id . "'";
            $d->query($sql);
        }

        // xoa item
        
        if ($d->query($sql))
            header("Location:index.php?com=htbh&act=man");
        else
            transfer("Xóa dữ liệu bị lỗi", "index.php?com=htbh&act=man");
    } else
        transfer("Không nhận được dữ liệu", "index.php?com=htbh&act=man");
}
?>



